import { SellablePart, SellablePartName } from '../types/SellableParts';
import { priceToString } from '../services/helperFunctions';
import { backendURL } from '../services/base';

export interface SummaryItemItemInput {
  name: SellablePartName;
  part: SellablePart;
}
export function SummaryItem({ name, part }: SummaryItemItemInput) {
  return (
    <div className="build__part">
      <h2 className="part__title">{name}</h2>
      <div className="part__name">
        <span className="part__info">Name:</span>
        <span>{`${part.name}`}</span>
      </div>
      <div className="part__manufacturer">
        <span className="part__info">Manufacturer:</span>
        <span>{`${part.manufacturer.name}`}</span>
      </div>
      <div className="part__price">
        <span className="part__info">Price:</span>
        <span>{`${priceToString(part.price)}`}</span>
      </div>
      <img
        src={`${backendURL}/upload/${part.image}`}
        alt="processor"
        className="part__img"
      />
    </div>
  );
}
