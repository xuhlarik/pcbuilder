import { Button } from '@mui/material';
import { Link } from 'react-router-dom';
import { NavItem } from './NavItemView.tsx';

export function LinkButton({ url, name }: NavItem) {
  return (
    <Button variant="contained" component={Link} to={url}>
      {name}
    </Button>
  );
}
