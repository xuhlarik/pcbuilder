import {
  TableContainer,
  Table,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Stack,
  Button,
  Avatar,
  IconButton,
} from '@mui/material';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { useNavigate } from 'react-router-dom';
import { useSetRecoilState, useRecoilState, useRecoilValue } from 'recoil';
import { RotatingLines } from 'react-loader-spinner';
import { SellablePartName, SellablePart } from '../../types/SellableParts';
import { priceToString } from '../../services/helperFunctions';
import { VideoCardFilters } from '../Filters/VideoCardFilters.tsx';
import './PartTable.css';
import { backendURL } from '../../services/base';
import { ProcessorFilters } from '../Filters/ProcessorFilters.tsx';
import { MotherboardFilters } from '../Filters/MotherboardFilters.tsx';
import { MemoryFilters } from '../Filters/MemoryFilters.tsx';
import {
  auth,
  sortingAtom,
  sortNameAtom,
  sortPriceAtom,
} from '../../state/atoms';
import { ApiError } from '../../types/Error';
import { ErrorView } from '../ErrorView.tsx';

export type PartTableInput<T> = {
  name: SellablePartName;
  data: T[] | undefined;
  addPart: (arg0: T) => void;
  deleteFn: (id: string) => void;
  deleteLoading: boolean;
  errorDelete: ApiError | null;
  isErrorDelete: boolean;
  isError: boolean;
  error: ApiError | null;
  isLoading: boolean;
  functionality: 'delete' | 'all';
};

export function PartTable<T extends SellablePart>({
  name,
  data,
  addPart,
  deleteFn: deletePart,
  deleteLoading,
  errorDelete,
  isErrorDelete,
  isError: isErrorGet,
  error: loadError,
  isLoading: loadingGet,
  functionality = 'all',
}: PartTableInput<T>) {
  const navigate = useNavigate();
  const setOrdering = useSetRecoilState(sortingAtom);
  const [nameOrder, setNameOrder] = useRecoilState(sortNameAtom);
  const [priceOrder, setPriceOrder] = useRecoilState(sortPriceAtom);
  const admin = useRecoilValue(auth);

  let filters = <ProcessorFilters />;
  switch (name) {
    case 'Processor': {
      filters = <ProcessorFilters />;
      break;
    }
    case 'Motherboard': {
      filters = <MotherboardFilters />;
      break;
    }
    case 'Memory': {
      filters = <MemoryFilters />;
      break;
    }
    default: {
      filters = <VideoCardFilters />;
      break;
    }
  }

  const handleSortName = () => {
    setNameOrder((prev) => (prev === 'asc' ? 'desc' : 'asc'));
    setOrdering('name');
  };

  const handleSortPrice = () => {
    setPriceOrder((prev) => (prev === 'asc' ? 'desc' : 'asc'));
    setOrdering('price');
  };

  return (
    <div className="main__parts">
      {isErrorDelete && errorDelete !== null ? (
        <ErrorView dataType={name} error={errorDelete} />
      ) : (
        ''
      )}
      {isErrorGet && loadError !== null ? (
        <ErrorView dataType={name} error={loadError} />
      ) : (
        ''
      )}

      <h1 className="parts__title">Choose {name}</h1>
      {loadingGet || data === undefined ? (
        <RotatingLines />
      ) : (
        <TableContainer className="parts__container">
          {functionality !== 'delete' ? filters : ''}
          <Table className="container__table">
            <TableHead>
              <TableRow>
                <TableCell>
                  <div className="table__cell">
                    <span>Name</span>
                    {functionality !== 'delete' && (
                      <IconButton onClick={handleSortName}>
                        {nameOrder === 'desc' ? (
                          <KeyboardArrowUpIcon />
                        ) : (
                          <KeyboardArrowDownIcon />
                        )}
                      </IconButton>
                    )}
                  </div>
                </TableCell>

                <TableCell>
                  <div className="table__cell">
                    <span>Price</span>
                    {functionality !== 'delete' && (
                      <IconButton onClick={handleSortPrice}>
                        {priceOrder === 'desc' ? (
                          <KeyboardArrowUpIcon />
                        ) : (
                          <KeyboardArrowDownIcon />
                        )}
                      </IconButton>
                    )}
                  </div>
                </TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((oneRow) => (
                <TableRow key={oneRow.id}>
                  <TableCell>
                    <Stack spacing={3} direction="row" alignItems="center">
                      <Avatar
                        variant="rounded"
                        alt={oneRow.name}
                        src={`${backendURL}/upload/${oneRow.image}`}
                      />
                      <div>{oneRow.name}</div>
                    </Stack>
                  </TableCell>
                  <TableCell>{priceToString(oneRow.price)}</TableCell>
                  <TableCell>
                    {functionality !== 'delete' ? (
                      <Button
                        variant="contained"
                        onClick={() => {
                          addPart(oneRow);
                          navigate('/config');
                        }}
                      >
                        Add
                      </Button>
                    ) : (
                      ''
                    )}

                    {admin ? (
                      <Button
                        variant="contained"
                        disabled={deleteLoading}
                        onClick={() => {
                          deletePart(oneRow.id);
                        }}
                      >
                        Delete
                      </Button>
                    ) : (
                      ''
                    )}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </div>
  );
}
