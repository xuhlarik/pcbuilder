import './AdminComponent.css';
import { Button, IconButton } from '@mui/material';
import { Link } from 'react-router-dom';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import AddIcon from '@mui/icons-material/Add';

export interface AdminComponentInput {
  name: string;
  image: string;
}
export function AdminComponent({ name, image }: AdminComponentInput) {
  return (
    <div className="manage">
      <h2 className="manage__title">{`${name}`}</h2>
      <img src={`../assets/${image}`} alt={name} className="manage__img" />
      <div className="manage__buttons">
        <Button
          variant="contained"
          component={Link}
          to={name === 'Part' ? `/admin/add/part` : `/admin/add/config`}
          className="manage__buttons--item"
        >
          <IconButton>
            <AddIcon />
          </IconButton>
        </Button>
        {name === 'Part' ? (
          ''
        ) : (
          <Button
            variant="contained"
            component={Link}
            to={`/presets`}
            className="manage__buttons--item"
          >
            <IconButton>
              <EditIcon />
            </IconButton>
          </Button>
        )}

        <Button
          variant="contained"
          component={Link}
          to={name === 'Part' ? '/admin/delete/part' : `/presets`}
          className="manage__buttons--item"
        >
          <IconButton>
            <DeleteIcon />
          </IconButton>
        </Button>
      </div>
    </div>
  );
}
