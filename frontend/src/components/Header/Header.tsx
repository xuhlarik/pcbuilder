import { Link, useLocation } from 'react-router-dom';
import { useRecoilState } from 'recoil';
import { useMutation } from 'react-query';
import LoginIcon from '@mui/icons-material/Login';
import LogoutIcon from '@mui/icons-material/Logout';
import { Button, Breadcrumbs, Link as MuiLink } from '@mui/material';
import { KeyboardArrowRight, KeyboardArrowLeft } from '@mui/icons-material';
import { useState } from 'react';
import { NavItem, NavItemView } from '../NavItemView.tsx';

import './Header.css';
import { auth } from '../../state/atoms';
import { logoutApi } from '../../services/authApi';
import { makePartPath } from '../../services/helperFunctions';
import { SellablePartName } from '../../types/SellableParts';

export function Header() {
  const navItems: NavItem[] = [
    { url: '/', name: 'Home' },
    { url: '/presets', name: 'Presets' },
    { url: '/config', name: 'Do it yourself' },
  ];
  const partItems: SellablePartName[] = [
    'Processor',
    'Motherboard',
    'Memory',
    'Video Card',
  ];
  const location = useLocation();
  const [authVal, setAuth] = useRecoilState(auth);
  const [more, setMore] = useState(false);

  const { mutate: logout, isLoading } = useMutation({
    mutationKey: ['logout'],
    mutationFn: () => logoutApi(),
    onSuccess: () => setAuth(false),
  });

  const showLoginButton = location.pathname !== '/login';
  return (
    <header className="header">
      <div className="header__row">
        <Link to="/">
          <img src="../../assets/logo.svg" alt="logo" className="row__logo" />
        </Link>
        {showLoginButton && !authVal && (
          <Link to="/login">
            <Button type="button" variant="contained" endIcon={<LoginIcon />}>
              Log in
            </Button>
          </Link>
        )}
        {authVal && (
          <Button
            type="button"
            variant="contained"
            disabled={isLoading}
            onClick={() => {
              logout();
            }}
            endIcon={<LogoutIcon />}
          >
            Log Out
          </Button>
        )}
      </div>

      <Breadcrumbs>
        {navItems.map((item) => (
          <NavItemView key={item.name} {...item} />
        ))}
        <MuiLink onClick={() => setMore(!more)}>
          <div className="parts">
            Parts{' '}
            {more !== true ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
          </div>
        </MuiLink>
        {more === true &&
          partItems.map((part) => (
            <NavItemView key={part} url={makePartPath(part)} name={part} />
          ))}
      </Breadcrumbs>
    </header>
  );
}
