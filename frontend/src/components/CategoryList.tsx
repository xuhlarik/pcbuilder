import { Category } from '@pcbuilder/common/api';
import { useQuery } from 'react-query';
import { RotatingLines } from 'react-loader-spinner';
import { ApiError } from '../types/Error';
import categoryApi from '../services/categoryApi';

import { ErrorView } from './ErrorView.tsx';
import { CategoryView } from './CategoryView/CategoryView.tsx';

export function CategoryList({ view }: { view: 'view' | 'add' }) {
  const {
    isLoading,
    data: categories,
    isError,
    error,
  } = useQuery<Category[], ApiError>({
    queryKey: ['categories'],
    queryFn: () => categoryApi.readMany(),
  });

  if (isError) {
    return <ErrorView dataType="Category" error={error} />;
  }
  return (
    <>
      <h1 className="main__title">Choose category</h1>
      <ul className="main__types">
        {categories === undefined || isLoading ? (
          <RotatingLines />
        ) : (
          categories.map((category) => (
            <CategoryView key={category.id} view={view} {...category} />
          ))
        )}
      </ul>
    </>
  );
}
