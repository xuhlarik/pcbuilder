import { Link } from 'react-router-dom';
import './Footer.css';

export function Footer() {
  return (
    <footer className="footer">
      <div className="footer__moto">
        <h2 className="moto__text">Pick parts.</h2>
        <h2 className="moto__text">Build your PC.</h2>
      </div>
      <img src="../../assets/logoBW.svg" alt="logo" className="footer__logo" />
      <div className="footer__contact">
        <h2 className="contact__title">Contact us:</h2>
        <Link to="mailto:stepandvorsky@mail.muni.cz" className="contact__mail">
          stepandvorsky@mail.muni.cz
        </Link>
      </div>
    </footer>
  );
}
