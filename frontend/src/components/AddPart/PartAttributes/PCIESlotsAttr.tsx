import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
} from '@mui/material';
import { MotherboardPcieSlot, PcieGeneration } from '@pcbuilder/common/api';
import { ChangeEvent, useState } from 'react';
import { useQuery } from 'react-query';
import { readManyGenerations } from '../../../services/pcieGenerationApi';

export interface PCIESlot {
  generation: PcieGeneration;
  size: number;
  count: number;
}

interface PCIESlotsProps {
  PCIESlots: MotherboardPcieSlot;
  setPCIESlots: (data: MotherboardPcieSlot) => void;
}

export function PCIESlotsAttr({ PCIESlots, setPCIESlots }: PCIESlotsProps) {
  const { data: generations } = useQuery({
    queryKey: ['generations'],
    queryFn: async () => readManyGenerations(),
  });
  const [generation, setGeneration] = useState<PcieGeneration>({});
  const handleGenerationChange = (event: SelectChangeEvent<PcieGeneration>) => {
    setGeneration(event.target.value as PcieGeneration);
  };

  const sizes = [1, 4, 8, 16];
  const [size, setSize] = useState<number>(1);
  const handleSizeChange = (event: SelectChangeEvent<number>) => {
    setSize(event.target.value as number);
  };

  const [count, setCount] = useState<number>(1);
  const handleCountChange = (event: ChangeEvent<HTMLInputElement>) => {
    setCount(Number(event.target.value));
  };

  const handlePCIESlotAdd = () => {
    setPCIESlots((prev: PCIESlot[]) => [
      ...prev,
      {
        generation,
        size,
        count,
      },
    ]);
  };

  return (
    <div className="formAdd__item--slots">
      <div className="slots__container">
        <label className="formAdd__item--label">PCIE Slots:</label>
        <FormControl>
          <InputLabel id="generation">Generation</InputLabel>
          <Select
            value={generation}
            autoWidth
            onChange={handleGenerationChange}
            labelId="generation"
            label="Generation"
          >
            {generations?.map((g) => (
              <MenuItem key={g.id} value={g}>
                {g.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl>
          <InputLabel id="size">Size</InputLabel>
          <Select
            value={size}
            autoWidth
            onChange={handleSizeChange}
            labelId="size"
            label="Size"
          >
            {sizes?.map((s) => (
              <MenuItem key={s} value={s}>
                {s}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <FormControl>
          <TextField
            id="outlined-number"
            label="Count"
            type="number"
            InputLabelProps={{
              shrink: true,
            }}
            inputProps={{
              style: { width: '3rem' },
              min: 1,
            }}
            value={count}
            onChange={handleCountChange}
          />
        </FormControl>
        <Button onClick={handlePCIESlotAdd}>Add</Button>
      </div>
      <div className="slots__selected">
        <span>Added: </span>
        {PCIESlots.map((s: MotherboardPcieSlot) => (
          <span key={s.id}>
            {s.generation.name}, {s.size}, {s.count};
          </span>
        ))}
      </div>
    </div>
  );
}
