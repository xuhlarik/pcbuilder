import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { FormFactor } from '@pcbuilder/common/api';
import { useQuery } from 'react-query';
import { readManyFormFactors } from '../../../services/formFactorApi';

interface FormFactorProps {
  formFactor: FormFactor;
  setFormFactor: (data: FormFactor) => void;
}

export function FormFactorAttr({ formFactor, setFormFactor }: FormFactorProps) {
  const { data: formFactors } = useQuery({
    queryKey: ['formFactors'],
    queryFn: async () => readManyFormFactors(),
  });
  const handleFormFactorChange = (event: SelectChangeEvent<FormFactor>) => {
    setFormFactor(event.target.value as FormFactor);
  };
  return (
    <div className="formAdd__item">
      <label className="formAdd__item--label">Form Factor:</label>
      <FormControl>
        <Select value={formFactor} autoWidth onChange={handleFormFactorChange}>
          {formFactors?.map((f) => (
            <MenuItem key={f.id} value={f}>
              {f.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
