import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { MotherboardChipset } from '@pcbuilder/common/api';
import { useQuery } from 'react-query';
import { readManyChipsets } from '../../../services/motherboardChipsetApi';

interface MotherboardChipsetProps {
  motherboardChipset: MotherboardChipset;
  setMotherboardChipset: (data: MotherboardChipset) => void;
}

export function MotherboardChipsetAttr({
  motherboardChipset,
  setMotherboardChipset,
}: MotherboardChipsetProps) {
  const { data: motherboardChipsets } = useQuery({
    queryKey: ['motherboardChipsets'],
    queryFn: async () => readManyChipsets(),
  });
  const handleMotherboardChipsetChange = (
    event: SelectChangeEvent<MotherboardChipset>
  ) => {
    setMotherboardChipset(event.target.value as MotherboardChipset);
  };
  return (
    <div className="formAdd__item">
      <label className="formAdd__item--label">Motherboard Chipset:</label>
      <FormControl>
        <Select
          value={motherboardChipset}
          autoWidth
          onChange={handleMotherboardChipsetChange}
        >
          {motherboardChipsets?.map((m) => (
            <MenuItem key={m.id} value={m}>
              {m.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
