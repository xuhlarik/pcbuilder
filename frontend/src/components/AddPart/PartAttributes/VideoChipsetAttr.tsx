import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { VideoChipset } from '@pcbuilder/common/api';
import { useQuery } from 'react-query';
import { readManyChipsets } from '../../../services/videoChipsetApi';

interface VideoChipsetProps {
  videoChipset: VideoChipset;
  setVideoChipset: (data: VideoChipset) => void;
}

export function VideoChipsetAttr({
  videoChipset,
  setVideoChipset,
}: VideoChipsetProps) {
  const { data: videoChipsets } = useQuery({
    queryKey: ['videoChipset'],
    queryFn: async () => readManyChipsets(),
  });
  const handleVideoChipsetChange = (event: SelectChangeEvent<VideoChipset>) => {
    setVideoChipset(event.target.value as VideoChipset);
  };
  return (
    <div className="formAdd__item">
      <label className="formAdd__item--label">Video Chipset:</label>
      <FormControl>
        <Select
          value={videoChipset}
          autoWidth
          onChange={handleVideoChipsetChange}
        >
          {videoChipsets?.map((v) => (
            <MenuItem key={v.id} value={v}>
              {v.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
