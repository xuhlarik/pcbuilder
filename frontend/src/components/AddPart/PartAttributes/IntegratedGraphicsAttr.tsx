import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { IntegratedGraphics } from '@pcbuilder/common/api';
import { useQuery } from 'react-query';
import { readManyIntegratedGraphics } from '../../../services/integratedGraphicsApi';

interface IntegratedGraphicsProps {
  integratedGraphic: IntegratedGraphics | null;
  setIntegratedGraphic: (data: IntegratedGraphics | null) => void;
}

export function IntegratedGraphicsAttr({
  integratedGraphic,
  setIntegratedGraphic,
}: IntegratedGraphicsProps) {
  const { data: integratedGraphics } = useQuery({
    queryKey: ['integratedGraphics'],
    queryFn: async () => readManyIntegratedGraphics(),
  });
  const handleIntegratedGraphicsChange = (
    event: SelectChangeEvent<IntegratedGraphics | string>
  ) => {
    if (event.target.value === 'None') {
      setIntegratedGraphic(null);
    } else {
      setIntegratedGraphic(event.target.value as IntegratedGraphics);
    }
  };
  return (
    <div className="formAdd__item">
      <label className="formAdd__item--label">Integrated Graphics:</label>
      <FormControl>
        <Select
          value={integratedGraphic}
          autoWidth
          onChange={handleIntegratedGraphicsChange}
        >
          <MenuItem value={'None'}>None</MenuItem>
          {integratedGraphics?.map((i) => (
            <MenuItem key={i.id} value={i}>
              {i.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
