import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { Socket } from '@pcbuilder/common/api';
import { useQuery } from 'react-query';
import { readManySockets } from '../../../services/socketApi';

interface SocketProps {
  socket: Socket;
  setSocket: (data: Socket) => void;
}

export function SocketAttr({ socket, setSocket }: SocketProps) {
  const { data: sockets } = useQuery({
    queryKey: ['sockets'],
    queryFn: async () => readManySockets(),
  });
  const handleSocketChange = (event: SelectChangeEvent<Socket>) => {
    setSocket(event.target.value as Socket);
  };
  return (
    <div className="formAdd__item">
      <label className="formAdd__item--label">Socket:</label>
      <FormControl>
        <Select value={socket} autoWidth onChange={handleSocketChange}>
          {sockets?.map((s) => (
            <MenuItem key={s.id} value={s}>
              {s.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
