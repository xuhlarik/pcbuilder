import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { MemoryGeneration } from '@pcbuilder/common/api';
import { useQuery } from 'react-query';
import { readManyGenerations } from '../../../services/memoryGenerationApi';

interface MemoryGenerationProps {
  memoryGeneration: MemoryGeneration;
  setMemoryGeneration: (data: MemoryGeneration) => void;
}

export function MemoryGenerationAttr({
  memoryGeneration,
  setMemoryGeneration,
}: MemoryGenerationProps) {
  const { data: memoryGenerations } = useQuery({
    queryKey: ['memoryGenerations'],
    queryFn: async () => readManyGenerations(),
  });
  const handleMemoryGenerationChange = (
    event: SelectChangeEvent<MemoryGeneration>
  ) => {
    setMemoryGeneration(event.target.value as MemoryGeneration);
  };
  return (
    <div className="formAdd__item">
      <label className="formAdd__item--label">Memory Generation:</label>
      <FormControl>
        <Select
          value={memoryGeneration}
          autoWidth
          onChange={handleMemoryGenerationChange}
        >
          {memoryGenerations?.map((m) => (
            <MenuItem key={m.id} value={m}>
              {m.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
