import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { Manufacturer } from '@pcbuilder/common/api';
import { useQuery } from 'react-query';
import { readManyManufacturers } from '../../../services/manufacturerApi';

interface ManufacturerProps {
  manufacturer: Manufacturer;
  setManufacturer: (data: Manufacturer) => void;
}

export function ManufacturerAttr({
  manufacturer,
  setManufacturer,
}: ManufacturerProps) {
  const { data: manufacturers } = useQuery({
    queryKey: ['manufacturers'],
    queryFn: async () => readManyManufacturers(),
  });
  const handleManufacturerChange = (event: SelectChangeEvent<Manufacturer>) => {
    setManufacturer(event.target.value as Manufacturer);
  };
  return (
    <div className="formAdd__item">
      <label className="formAdd__item--label">Manufacturer:</label>
      <FormControl>
        <Select
          value={manufacturer}
          autoWidth
          onChange={handleManufacturerChange}
        >
          {manufacturers?.map((m) => (
            <MenuItem key={m.id} value={m}>
              {m.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
