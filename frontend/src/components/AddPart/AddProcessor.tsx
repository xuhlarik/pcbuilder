import { Button, OutlinedInput } from '@mui/material';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import schema, {
  Manufacturer,
  Socket,
  IntegratedGraphics,
} from '@pcbuilder/common/api';
import './AddPart.css';
import { useState } from 'react';
import { z } from 'zod';
import { useMutation } from 'react-query';
import { ManufacturerAttr } from './PartAttributes/ManufacturerAttr.tsx';
import { SocketAttr } from './PartAttributes/SocketAttr.tsx';
import { IntegratedGraphicsAttr } from './PartAttributes/IntegratedGraphicsAttr.tsx';
import { createProcessor } from '../../services/processorApi';
import { ErrorView } from '../ErrorView.tsx';
import { uploadImage } from '../../services/uploadApi';

export function AddProcessor() {
  const navigate = useNavigate();

  const [manufacturer, setManufacturer] = useState<Manufacturer>();
  const [socket, setSocket] = useState<Socket>();
  const [integratedGraphics, setIntegratedGraphics] =
    useState<IntegratedGraphics | null>();

  const { register, handleSubmit, getValues } = useForm<Processor>();
  const {
    mutate,
    isLoading,
    error: responseError,
    isError,
  } = useMutation(
    'createProcessor',
    (processor: Processor) => createProcessor(processor),
    {
      onSuccess: () => {
        navigate('/');
      },
    }
  );

  type Processor = z.infer<typeof schema.processor.create>;

  async function add() {
    const stored = await uploadImage(getValues('image'));
    const processor = {
      name: getValues('name'),
      price: getValues('price'),
      manufacturerId: manufacturer.id,
      socketId: socket.id,
      integratedGraphicsId: integratedGraphics?.id ?? null,
      image: stored[0].filename,
    };
    mutate(processor);
  }

  return (
    <>
      {isError === true && (
        <ErrorView dataType="Processor" error={responseError} />
      )}
      <form onSubmit={handleSubmit(add)} className="processor__formAdd">
        <h1 className="formAdd__title">Add Processor</h1>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Name:</label>
          <OutlinedInput
            required
            type="text"
            {...register('name')}
            className="formAdd__item--input"
          />
        </div>
        <ManufacturerAttr
          manufacturer={manufacturer}
          setManufacturer={setManufacturer}
        />
        <SocketAttr socket={socket} setSocket={setSocket} />
        <IntegratedGraphicsAttr
          integratedGraphic={integratedGraphics}
          setIntegratedGraphic={setIntegratedGraphics}
        />
        <div className="formAdd__item">
          <label className="formAdd__item--label">Price:</label>
          <OutlinedInput
            required
            type="text"
            {...register('price')}
            className="formAdd__item--input"
          />
        </div>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Image:</label>
          <input
            required
            type="file"
            {...register('image')}
            className="formAdd__item--inputFile"
          />
        </div>
        <Button
          disabled={isLoading}
          type="submit"
          color="info"
          variant="contained"
          className="form__button"
        >
          Proceed
        </Button>
      </form>
    </>
  );
}
