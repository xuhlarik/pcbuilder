import {
  Button,
  FormControl,
  MenuItem,
  OutlinedInput,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import schema, {
  Manufacturer,
  VideoChipset,
  PcieGeneration,
} from '@pcbuilder/common/api/api';
import './AddPart.css';
import { z } from 'zod';
import { useMutation, useQuery } from 'react-query';
import { useState } from 'react';
import { ManufacturerAttr } from './PartAttributes/ManufacturerAttr.tsx';
import { readManyGenerations } from '../../services/pcieGenerationApi';
import { VideoChipsetAttr } from './PartAttributes/VideoChipsetAttr.tsx';
import { createVideoCard } from '../../services/videoCardApi';
import { ErrorView } from '../ErrorView.tsx';
import { uploadImage } from '../../services/uploadApi';

export function AddVideoCard() {
  const navigate = useNavigate();

  const [manufacturer, setManufacturer] = useState<Manufacturer>();
  const [videoChipset, setVideoChipset] = useState<VideoChipset>();
  const { data: generations } = useQuery({
    queryKey: ['generations'],
    queryFn: async () => readManyGenerations(),
  });
  const [generation, setGeneration] = useState<PcieGeneration>();
  const handleGenerationChange = (event: SelectChangeEvent<PcieGeneration>) => {
    setGeneration(event.target.value as PcieGeneration);
  };

  const { register, handleSubmit, getValues } = useForm<VideoCard>();
  const {
    mutate,
    isLoading,
    error: responseError,
    isError,
  } = useMutation(
    'createVideoCard',
    (videoCard: VideoCard) => createVideoCard(videoCard),
    {
      onSuccess: () => {
        navigate('/');
      },
    }
  );

  type VideoCard = z.infer<typeof schema.videoCard.create>;

  async function add() {
    const stored = await uploadImage(getValues('image'));
    const processor = {
      name: getValues('name'),
      price: getValues('price'),
      manufacturerId: manufacturer.id,
      pcieGenerationId: generation.id,
      chipsetId: videoChipset.id,
      slotSize: getValues('slotSize'),
      image: stored[0].filename,
    };
    mutate(processor);
  }

  return (
    <>
      {isError === true && (
        <ErrorView dataType="Processor" error={responseError} />
      )}
      <form onSubmit={handleSubmit(add)} className="processor__formAdd">
        <h1 className="formAdd__title">Add Video Card</h1>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Name:</label>
          <OutlinedInput
            required
            type="text"
            {...register('name')}
            className="formAdd__item--input"
          />
        </div>
        <ManufacturerAttr
          manufacturer={manufacturer}
          setManufacturer={setManufacturer}
        />
        <div className="formAdd__item">
          <label className="formAdd__item--label">PCIE Generation:</label>
          <FormControl>
            <Select
              value={generation}
              autoWidth
              onChange={handleGenerationChange}
            >
              {generations?.map((g) => (
                <MenuItem key={g.id} value={g}>
                  {g.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
        <VideoChipsetAttr
          videoChipset={videoChipset}
          setVideoChipset={setVideoChipset}
        />
        <div className="formAdd__item">
          <label className="formAdd__item--label">Slot Size:</label>
          <OutlinedInput
            required
            type="text"
            {...register('slotSize')}
            className="formAdd__item--input"
          />
        </div>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Price:</label>
          <OutlinedInput
            required
            type="text"
            {...register('price')}
            className="formAdd__item--input"
          />
        </div>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Image:</label>
          <input
            required
            type="file"
            {...register('image')}
            className="formAdd__item--inputFile"
          />
        </div>
        <Button
          disabled={isLoading}
          type="submit"
          color="info"
          variant="contained"
          className="form__button"
        >
          Proceed
        </Button>
      </form>
    </>
  );
}
