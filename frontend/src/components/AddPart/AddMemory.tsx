import { Button, OutlinedInput } from '@mui/material';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { useMutation } from 'react-query';
import schema, { Manufacturer, MemoryGeneration } from '@pcbuilder/common/api';
import './AddPart.css';
import { z } from 'zod';
import { useState } from 'react';
import { ManufacturerAttr } from './PartAttributes/ManufacturerAttr.tsx';
import { MemoryGenerationAttr } from './PartAttributes/MemoryGenerationAttr.tsx';
import { createMemory } from '../../services/memoryApi';
import { ErrorView } from '../ErrorView.tsx';
import { uploadImage } from '../../services/uploadApi';

export function AddMemory() {
  const navigate = useNavigate();

  const [manufacturer, setManufacturer] = useState<Manufacturer>();
  const [memoryGeneration, setMemoryGeneration] = useState<MemoryGeneration>();

  const { register, handleSubmit, getValues } = useForm<Memory>();
  const {
    mutate,
    isLoading,
    error: responseError,
    isError,
  } = useMutation('createMemory', (memory: Memory) => createMemory(memory), {
    onSuccess: () => {
      navigate('/');
    },
  });

  type Memory = z.infer<typeof schema.memory.create>;

  async function add() {
    const stored = await uploadImage(getValues('image'));
    const memory = {
      name: getValues('name'),
      price: getValues('price'),
      manufacturerId: manufacturer.id,
      memoryGenerationId: memoryGeneration.id,
      image: stored[0].filename,
    };
    mutate(memory);
  }

  return (
    <>
      {isError === true && (
        <ErrorView dataType="Processor" error={responseError} />
      )}
      <form onSubmit={handleSubmit(add)} className="processor__formAdd">
        <h1 className="formAdd__title">Add Memory</h1>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Name:</label>
          <OutlinedInput
            required
            type="text"
            {...register('name')}
            className="formAdd__item--input"
          />
        </div>
        <ManufacturerAttr
          manufacturer={manufacturer}
          setManufacturer={setManufacturer}
        />
        <MemoryGenerationAttr
          memoryGeneration={memoryGeneration}
          setMemoryGeneration={setMemoryGeneration}
        />
        <div className="formAdd__item">
          <label className="formAdd__item--label">Price:</label>
          <OutlinedInput
            required
            type="text"
            {...register('price')}
            className="formAdd__item--input"
          />
        </div>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Image:</label>
          <input
            required
            type="file"
            {...register('image')}
            className="formAdd__item--inputFile"
          />
        </div>
        <Button
          disabled={isLoading}
          type="submit"
          color="info"
          variant="contained"
          className="form__button"
        >
          Proceed
        </Button>
      </form>
    </>
  );
}
