import { Button, OutlinedInput } from '@mui/material';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import schema, {
  Manufacturer,
  Socket,
  FormFactor,
  MotherboardChipset,
} from '@pcbuilder/common/api';
import './AddPart.css';
import { useState } from 'react';
import { z } from 'zod';
import { pick } from '@pcbuilder/common/utils';
import { useMutation } from 'react-query';
import { ManufacturerAttr } from './PartAttributes/ManufacturerAttr.tsx';
import { SocketAttr } from './PartAttributes/SocketAttr.tsx';
import { FormFactorAttr } from './PartAttributes/FormFactorAttr.tsx';
import { MotherboardChipsetAttr } from './PartAttributes/MotherboardChipsetAttr.tsx';
import { PCIESlotsAttr, PCIESlot } from './PartAttributes/PCIESlotsAttr.tsx';
import { createMotherboard } from '../../services/motherboardApi';
import { ErrorView } from '../ErrorView.tsx';
import { uploadImage } from '../../services/uploadApi';

export function AddMotherboard() {
  const navigate = useNavigate();

  const [manufacturer, setManufacturer] = useState<Manufacturer>();
  const [socket, setSocket] = useState<Socket>();
  const [formFactor, setFormFactor] = useState<FormFactor>();
  const [chipset, setChipset] = useState<MotherboardChipset>();
  const [pcieSlots, setPcieSlots] = useState<PCIESlot[]>([]);

  const { register, handleSubmit, getValues } = useForm<Motherboard>();

  const {
    mutate,
    isLoading,
    error: responseError,
    isError,
  } = useMutation(
    'createMotherboard',
    (motherboard: Motherboard) => createMotherboard(motherboard),
    {
      onSuccess: () => {
        navigate('/');
      },
    }
  );

  type Motherboard = z.infer<typeof schema.motherboard.create>;

  async function add() {
    const stored = await uploadImage(getValues('image'));
    const motherboard = {
      name: getValues('name'),
      price: getValues('price'),
      manufacturerId: manufacturer.id,
      socketId: socket.id,
      formFactorId: formFactor.id,
      chipsetId: chipset.id,
      memorySlotCount: getValues('memorySlotCount'),
      pcieSlots: pcieSlots.map((p) => ({
        ...pick(p, 'size', 'count'),
        pcieGenerationId: p.generation.id,
      })),
      image: stored[0].filename,
    };
    mutate(motherboard);
  }

  return (
    <>
      {isError === true && (
        <ErrorView dataType="Processor" error={responseError} />
      )}
      <form onSubmit={handleSubmit(add)} className="processor__formAdd">
        <h1 className="formAdd__title">Add Motherboard</h1>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Name:</label>
          <OutlinedInput
            required
            type="text"
            {...register('name')}
            className="formAdd__item--input"
          />
        </div>
        <ManufacturerAttr
          manufacturer={manufacturer}
          setManufacturer={setManufacturer}
        />
        <SocketAttr socket={socket} setSocket={setSocket} />
        <FormFactorAttr formFactor={formFactor} setFormFactor={setFormFactor} />
        <MotherboardChipsetAttr
          motherboardChipset={chipset}
          setMotherboardChipset={setChipset}
        />
        <PCIESlotsAttr PCIESlots={pcieSlots} setPCIESlots={setPcieSlots} />
        <div className="formAdd__item">
          <label className="formAdd__item--label">Slot Count:</label>
          <OutlinedInput
            required
            type="text"
            {...register('memorySlotCount')}
            className="formAdd__item--input"
          />
        </div>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Price:</label>
          <OutlinedInput
            required
            type="text"
            {...register('price')}
            className="formAdd__item--input"
          />
        </div>
        <div className="formAdd__item">
          <label className="formAdd__item--label">Image:</label>
          <input
            required
            type="file"
            {...register('image')}
            className="formAdd__item--inputFile"
          />
        </div>
        <Button
          disabled={isLoading}
          type="submit"
          color="info"
          variant="contained"
          className="form__button"
        >
          Proceed
        </Button>
      </form>
    </>
  );
}
