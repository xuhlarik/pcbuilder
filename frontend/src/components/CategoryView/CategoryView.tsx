import { Category } from '@pcbuilder/common/api';
import { Button } from '@mui/material';
import './CategoryView.css';
import { LinkButton } from '../LinkButton.tsx';

import { useConfiguration } from '../../hooks/configurationHook';

export type CategoryInputType = Category & { view: 'view' | 'add' };
// view true for viewing false for editing
export function CategoryView({ id, name, view }: CategoryInputType) {
  const { addCategory, configuration } = useConfiguration();
  return (
    <li className="main__types--item">
      <h2 className="item__title">{name}</h2>
      <img
        src={`/assets/${name.toLowerCase()}.jpg`}
        alt={name}
        className="item__img"
      />
      {view === 'view' ? (
        <LinkButton name="Build" url={`category/${id}`} />
      ) : (
        <Button variant="contained" onClick={() => addCategory(id)}>
          {id === configuration.preset.categoryId ? 'Chosen' : 'Choose'}
        </Button>
      )}
    </li>
  );
}
