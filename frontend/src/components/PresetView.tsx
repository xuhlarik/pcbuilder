import { Preset } from '@pcbuilder/common/api';
import { useNavigate } from 'react-router-dom';
import { Button } from '@mui/material';
import { useRecoilValue } from 'recoil';
import { auth } from '../state/atoms';
import { LinkButton } from './LinkButton.tsx';
import { useConfiguration } from '../hooks/configurationHook';
import presetApi from '../services/presetApi';
import { ErrorView } from './ErrorView.tsx';
import { useDelete } from '../hooks/deleteHook';

export function PresetView({
  buildId,
  name,
  description,
  id,
  categoryId,
}: Preset) {
  const admin = useRecoilValue(auth);
  const { addPresetAtributes } = useConfiguration();
  const navigate = useNavigate();
  function setEdit() {
    addPresetAtributes({ id, name, description, categoryId });
    navigate(`/presets/${buildId}`);
  }
  const {
    deleteFn: deletePreset,
    isErrorDelete,
    errorDelete,
    deleteLoading,
  } = useDelete(['presets'], presetApi.delete, [[`category${categoryId}`]]);
  function deleteFn() {
    deletePreset(id);
  }

  return (
    <article className="preset">
      <h3 className="preset__name">{name}</h3>
      <span className="preset__description">{description}</span>
      {isErrorDelete ? <ErrorView dataType="preset" error={errorDelete} /> : ''}
      {admin ? (
        <>
          <Button variant="contained" onClick={setEdit}>
            View or Edit
          </Button>
          <Button
            variant="contained"
            disabled={deleteLoading}
            onClick={deleteFn}
          >
            Delete
          </Button>
        </>
      ) : (
        <LinkButton url={`/presets/${buildId}`} name={`view`} />
      )}
    </article>
  );
}
