import { Alert } from '@mui/material';
import { ApiError } from '../types/Error';
import { message } from '../services/helperFunctions';

export interface ErrorViewInput {
  dataType: string;
  error: ApiError;
}
export function ErrorView({ dataType, error }: ErrorViewInput) {
  return (
    <Alert variant="filled" severity="error">
      {message(error, dataType)}
    </Alert>
  );
}
