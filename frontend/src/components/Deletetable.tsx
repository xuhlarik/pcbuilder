import { useDelete } from '../hooks/deleteHook';
import { useGetParts } from '../hooks/getPartsHooks';
import { SellablePart, SellablePartName } from '../types/SellableParts';
import { PartTable } from './PartTable/PartTable.tsx';

export function DeleteTable<T extends SellablePart>({
  name,
  getFn,
  deleteFn,
}: {
  name: SellablePartName;
  getFn: () => Promise<T[]>;
  deleteFn: (arg0: string) => Promise<void>;
}) {
  const getHook = useGetParts<T>([name], getFn);
  const deleteHook = useDelete([name], deleteFn);
  return (
    <PartTable<T>
      addPart={() => {
        console.log('Function not implemented.');
      }}
      functionality="delete"
      name={name}
      {...getHook}
      {...deleteHook}
    />
  );
}
