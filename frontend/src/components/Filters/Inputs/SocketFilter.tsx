import { FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import { Socket } from '@pcbuilder/common/api';
import { FilterProps } from '../FilterProps';
import { IMotherboardFilters, IProcessorFilters } from '../../../state/atoms';

export function SocketFilter<
  T extends IMotherboardFilters | IProcessorFilters
>({ setFilterData, sockets }: FilterProps<T>) {
  const handleSocketChange =
    (socket: Socket) => (event: React.ChangeEvent<HTMLInputElement>) => {
      setFilterData((prev) => {
        if (event.target.checked) {
          return {
            ...prev,
            sockets: [...prev.sockets, socket],
          };
        }
        return {
          ...prev,
          sockets: prev.sockets.filter((s) => s !== socket),
        };
      });
    };

  return (
    <div className="filters__checkbox">
      <h2 className="checkbox__title">Socket</h2>
      <FormGroup>
        {[...(sockets ?? [])].map((socket) => (
          <FormControlLabel
            key={socket.id}
            control={<Checkbox onChange={handleSocketChange(socket)} />}
            label={socket.name}
          />
        ))}
      </FormGroup>
    </div>
  );
}
