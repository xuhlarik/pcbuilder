import { FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import { MotherboardChipset } from '@pcbuilder/common/api';
import { FilterProps } from '../FilterProps';
import { IMotherboardFilters } from '../../../state/atoms';

export function ChipsetFilter({
  setFilterData,
  chipsets,
}: FilterProps<IMotherboardFilters>) {
  const handleChipsetChange =
    (chipset: MotherboardChipset) =>
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setFilterData((prev) => {
        if (event.target.checked) {
          return {
            ...prev,
            chipsets: [...prev.chipsets, chipset],
          };
        }
        return {
          ...prev,
          chipsets: prev.chipsets.filter((c) => c !== chipset),
        };
      });
    };

  return (
    <div className="filters__checkbox">
      <h2 className="checkbox__title">Chipset</h2>
      <FormGroup>
        {[...(chipsets ?? [])].map((chipset) => (
          <FormControlLabel
            key={chipset.id}
            control={<Checkbox onChange={handleChipsetChange(chipset)} />}
            label={chipset.name}
          />
        ))}
      </FormGroup>
    </div>
  );
}
