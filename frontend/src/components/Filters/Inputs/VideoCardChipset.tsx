import { FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import { VideoCardChipset } from '@pcbuilder/common/api';
import { FilterProps } from '../FilterProps';
import { IVideoCardFilters } from '../../../state/atoms';

export function ChipsetFilter({
  setFilterData,
  chipsets,
}: FilterProps<IVideoCardFilters>) {
  const handleChipsetChange =
    (chipset: VideoCardChipset) =>
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setFilterData((prev) => {
        if (event.target.checked) {
          return {
            ...prev,
            chipsets: [...prev.chipsets, chipset],
          };
        }
        return {
          ...prev,
          chipsets: prev.chipsets.filter((c) => c !== chipset),
        };
      });
    };

  return (
    <div className="filters__checkbox">
      <h2 className="checkbox__title">Chipset</h2>
      <FormGroup>
        {[...(chipsets ?? [])].map((chipset) => (
          <FormControlLabel
            key={chipset.id}
            control={<Checkbox onChange={handleChipsetChange(chipset)} />}
            label={chipset.name}
          />
        ))}
      </FormGroup>
    </div>
  );
}
