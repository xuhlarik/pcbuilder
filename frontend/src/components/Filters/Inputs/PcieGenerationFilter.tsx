import { FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import { PcieGeneration } from '@pcbuilder/common/api';
import { FilterProps } from '../FilterProps';
import { IVideoCardFilters } from '../../../state/atoms';

export function PcieGenerationFilter({
  setFilterData,
  generations,
}: FilterProps<IVideoCardFilters>) {
  const handlePcieGenerationChange =
    (generation: PcieGeneration) =>
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setFilterData((prev) => {
        if (event.target.checked) {
          return {
            ...prev,
            generations: [...prev.generations, generation],
          };
        }
        return {
          ...prev,
          generations: prev.generations.filter((g) => g !== generation),
        };
      });
    };

  return (
    <div className="filters__checkbox">
      <h2 className="checkbox__title">PCIE Generation</h2>
      <FormGroup>
        {[...(generations ?? [])].map((generation) => (
          <FormControlLabel
            key={generation.id}
            control={
              <Checkbox onChange={handlePcieGenerationChange(generation)} />
            }
            label={generation.name}
          />
        ))}
      </FormGroup>
    </div>
  );
}
