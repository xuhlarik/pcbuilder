import { FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import { Manufacturer } from '@pcbuilder/common/api';
import { FilterProps, IFilters } from '../FilterProps';

export function ManufacturerFilter<T extends IFilters>({
  setFilterData,
  manufacturers,
}: FilterProps<T>) {
  const handleManufacturerChange =
    (manufacturer: Manufacturer) =>
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setFilterData((prev) => {
        if (event.target.checked) {
          return {
            ...prev,
            manufacturers: [...prev.manufacturers, manufacturer],
          };
        }
        return {
          ...prev,
          manufacturers: prev.manufacturers.filter((m) => m !== manufacturer),
        };
      });
    };

  return (
    <div className="filters__checkbox">
      <h2 className="checkbox__title">Manufacturer</h2>
      <FormGroup>
        {[...(manufacturers ?? [])].map((manufacturer) => (
          <FormControlLabel
            key={manufacturer.id}
            control={
              <Checkbox onChange={handleManufacturerChange(manufacturer)} />
            }
            label={manufacturer.name}
          />
        ))}
      </FormGroup>
    </div>
  );
}
