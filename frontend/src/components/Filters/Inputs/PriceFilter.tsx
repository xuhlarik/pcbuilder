import { Slider } from '@mui/material';
import { FilterProps, IFilters } from '../FilterProps';

export function PriceFilter<T extends IFilters>({
  filterData,
  setFilterData,
  minSize,
  maxSize,
}: FilterProps<T>) {
  function valuetext(value: number) {
    return `${value}$`;
  }

  const minDistance = 10;

  const handleChange = (
    _event: Event,
    newValue: number | number[],
    activeThumb: number
  ) => {
    if (!Array.isArray(newValue)) {
      return;
    }

    if (newValue[1] - newValue[0] < minDistance) {
      if (activeThumb === 0) {
        const clamped = Math.min(newValue[0], 100 - minDistance);
        setFilterData((prev) => ({
          ...prev,
          priceRange: [clamped, clamped + minDistance] as number[],
        }));
      } else {
        const clamped = Math.max(newValue[1], minDistance);
        setFilterData((prev) => ({
          ...prev,
          priceRange: [clamped - minDistance, clamped] as number[],
        }));
      }
    } else {
      setFilterData((prev) => ({
        ...prev,
        priceRange: newValue as number[],
      }));
    }
  };

  return (
    <div className="filters__slider">
      <h2 className="slider__title">Price</h2>
      <Slider
        max={maxSize}
        min={minSize}
        getAriaLabel={() => 'Minimum distance shift'}
        value={filterData.priceRange}
        onChange={handleChange}
        valueLabelDisplay="on"
        getAriaValueText={valuetext}
        disableSwap
      />
    </div>
  );
}
