import { OutlinedInput } from '@mui/material';
import { FilterProps, IFilters } from '../FilterProps';

export function NameFilter<T extends IFilters>({
  setFilterData,
}: FilterProps<T>) {
  return (
    <div className="filters__text">
      <h2 className="text__title">Name</h2>
      <OutlinedInput
        type="text"
        className="text__input"
        onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
          setFilterData((prev) => ({
            ...prev,
            name: event.target.value as string,
          }));
        }}
      />
    </div>
  );
}
