import { FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import { IntegratedGraphics } from '@pcbuilder/common/api';
import { FilterProps } from '../FilterProps';
import { IProcessorFilters } from '../../../state/atoms';

export function IntegratedGraphicsFilter({
  setFilterData,
  integratedGraphics,
}: FilterProps<IProcessorFilters>) {
  const handleIntegratedGraphicsChange =
    (integratedGraphic: IntegratedGraphics) =>
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setFilterData((prev) => {
        if (event.target.checked) {
          return {
            ...prev,
            integratedGraphics: [...prev.integratedGraphics, integratedGraphic],
          };
        }
        return {
          ...prev,
          integratedGraphics: prev.integratedGraphics.filter(
            (i) => i !== integratedGraphic
          ),
        };
      });
    };

  return (
    <div className="filters__checkbox">
      <h2 className="checkbox__title">Integrated Graphics</h2>
      <FormGroup>
        {[...(integratedGraphics ?? [])].map((integratedGraphic) => (
          <FormControlLabel
            key={integratedGraphic?.id ?? 'None'}
            control={
              <Checkbox
                onChange={handleIntegratedGraphicsChange(integratedGraphic)}
              />
            }
            label={integratedGraphic?.name ?? 'None'}
          />
        ))}
      </FormGroup>
    </div>
  );
}
