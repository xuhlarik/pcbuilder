import { Slider } from '@mui/material';
import { FilterProps } from '../FilterProps';
import { IVideoCardFilters } from '../../../state/atoms';

export function SlotsFilter({
  filterData,
  setFilterData,
  minSize,
  maxSize,
}: FilterProps<IVideoCardFilters>) {
  function valuetext(value: number) {
    return `${value}$`;
  }

  const minDistance = 1;

  const handleChange = (
    _event: Event,
    newValue: number | number[],
    activeThumb: number
  ) => {
    if (!Array.isArray(newValue)) {
      return;
    }

    if (newValue[1] - newValue[0] < minDistance) {
      if (activeThumb === 0) {
        const clamped = Math.min(newValue[0], 100 - minDistance);
        setFilterData((prev) => ({
          ...prev,
          slotSizeRange: [clamped, clamped + minDistance] as number[],
        }));
      } else {
        const clamped = Math.max(newValue[1], minDistance);
        setFilterData((prev) => ({
          ...prev,
          slotSizeRange: [clamped - minDistance, clamped] as number[],
        }));
      }
    } else {
      setFilterData((prev) => ({
        ...prev,
        slotSizeRange: newValue as number[],
      }));
    }
  };

  return (
    <div className="filters__slider">
      <h2 className="slider__title">Memory Slots</h2>
      <Slider
        max={maxSize}
        min={minSize}
        getAriaLabel={() => 'Minimum distance shift'}
        value={filterData.slotSizeRange}
        onChange={handleChange}
        valueLabelDisplay="on"
        getAriaValueText={valuetext}
        disableSwap
      />
    </div>
  );
}
