import { FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import { FormFactor } from '@pcbuilder/common/api';
import { IMotherboardFilters } from '../../../state/atoms';
import { FilterProps } from '../FilterProps';

export function FormFactorFilter({
  setFilterData,
  formFactors,
}: FilterProps<IMotherboardFilters>) {
  const handleFormFactorChange =
    (formFactor: FormFactor) =>
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setFilterData((prev) => {
        if (event.target.checked) {
          return {
            ...prev,
            formFactors: [...prev.formFactors, formFactor],
          };
        }
        return {
          ...prev,
          formFactors: prev.formFactors.filter((f) => f !== formFactor),
        };
      });
    };

  return (
    <div className="filters__checkbox">
      <h2 className="checkbox__title">Form Factor</h2>
      <FormGroup>
        {[...(formFactors ?? [])].map((formFactor) => (
          <FormControlLabel
            key={formFactor.id}
            control={<Checkbox onChange={handleFormFactorChange(formFactor)} />}
            label={formFactor.name}
          />
        ))}
      </FormGroup>
    </div>
  );
}
