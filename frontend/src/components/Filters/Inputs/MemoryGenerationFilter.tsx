import { FormGroup, FormControlLabel, Checkbox } from '@mui/material';
import { MemoryGeneration } from '@pcbuilder/common/api';
import { FilterProps } from '../FilterProps';
import { IMemoryFilters } from '../../../state/atoms';

export function GenerationFilter({
  setFilterData,
  generations,
}: FilterProps<IMemoryFilters>) {
  const handleGenerationChange =
    (generation: MemoryGeneration) =>
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setFilterData((prev) => {
        if (event.target.checked) {
          return {
            ...prev,
            generations: [...prev.generations, generation],
          };
        }
        return {
          ...prev,
          generations: prev.generations.filter((g) => g !== generation),
        };
      });
    };

  return (
    <div className="filters__checkbox">
      <h2 className="checkbox__title">Generation</h2>
      <FormGroup>
        {[...(generations ?? [])].map((generation) => (
          <FormControlLabel
            key={generation.id}
            control={<Checkbox onChange={handleGenerationChange(generation)} />}
            label={generation.name}
          />
        ))}
      </FormGroup>
    </div>
  );
}
