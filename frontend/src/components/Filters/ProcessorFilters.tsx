import { Button } from '@mui/material';
import './Filters.css';
import React from 'react';
import { useRecoilState, useRecoilValue } from 'recoil';
import {
  Manufacturer,
  Socket,
  IntegratedGraphics,
} from '@pcbuilder/common/api';
import { PriceFilter } from './Inputs/PriceFilter.tsx';
import {
  processorFiltersAtom,
  IProcessorFilters,
  processorsAtom,
} from '../../state/atoms';
import { ManufacturerFilter } from './Inputs/ManufacturerFilter.tsx';
import { SocketFilter } from './Inputs/SocketFilter.tsx';
import { IntegratedGraphicsFilter } from './Inputs/IntegratedGraphicsFilter.tsx';
import { NameFilter } from './Inputs/NameFilter.tsx';

export function ProcessorFilters() {
  const [globalFilterData, setGlobalFilterData] =
    useRecoilState(processorFiltersAtom);

  const [localFilterData, setLocalFilterData] =
    React.useState<IProcessorFilters>({
      ...globalFilterData,
    });

  const [filtering, setFiltering] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (filtering) {
      setFiltering(false);
    } else {
      setLocalFilterData({ ...globalFilterData });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalFilterData]);

  React.useEffect(() => {
    if (filtering) {
      setGlobalFilterData({ ...localFilterData });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filtering]);

  const handleFilter = () => {
    setFiltering(true);
  };

  const processors = useRecoilValue(processorsAtom);
  const [minPrice, maxPrice] = processors.reduce<[number, number]>(
    ([min, max], curr) => [
      Math.floor(Math.min(min, curr.price)),
      Math.round(Math.max(max, curr.price)),
    ],
    [Infinity, 0]
  );

  const manufacturers = processors.reduce<[Set<string>, Manufacturer[]]>(
    ([seen, acc], { manufacturer }) => {
      if (!seen.has(manufacturer.id)) {
        acc.push(manufacturer);
        seen.add(manufacturer.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const sockets = processors.reduce<[Set<string>, Socket[]]>(
    ([seen, acc], { socket }) => {
      if (!seen.has(socket.id)) {
        acc.push(socket);
        seen.add(socket.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const integratedGraphicsList = processors.reduce<
    [Set<string>, IntegratedGraphics[]]
  >(
    ([seen, acc], { integratedGraphics }) => {
      if (integratedGraphics === null) {
        if (!seen.has(integratedGraphics)) {
          acc.push(integratedGraphics);
          seen.add(integratedGraphics);
        }
        return [seen, acc];
      }
      if (!seen.has(integratedGraphics.id)) {
        acc.push(integratedGraphics);
        seen.add(integratedGraphics.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  return (
    <div className="filters">
      <h1 className="filters__title">Filters</h1>
      <NameFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
      />
      <PriceFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        minSize={minPrice}
        maxSize={maxPrice}
      />
      <ManufacturerFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        manufacturers={manufacturers}
      />
      <SocketFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        sockets={sockets}
      />
      <IntegratedGraphicsFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        integratedGraphics={integratedGraphicsList}
      />
      <Button
        variant="contained"
        className="filters__button"
        onClick={handleFilter}
      >
        Filter
      </Button>
    </div>
  );
}
