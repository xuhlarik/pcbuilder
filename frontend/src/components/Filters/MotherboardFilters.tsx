import { Button } from '@mui/material';
import './Filters.css';
import React from 'react';
import { useRecoilState, useRecoilValue } from 'recoil';
import {
  Manufacturer,
  Socket,
  FormFactor,
  MotherboardChipset,
} from '@pcbuilder/common/api';
import { NameFilter } from './Inputs/NameFilter.tsx';
import { PriceFilter } from './Inputs/PriceFilter.tsx';
import { ManufacturerFilter } from './Inputs/ManufacturerFilter.tsx';
import {
  IMotherboardFilters,
  motherboardFiltersAtom,
  motherboardsAtom,
} from '../../state/atoms';
import { SocketFilter } from './Inputs/SocketFilter.tsx';
import { FormFactorFilter } from './Inputs/FormFactorFilter.tsx';
import { ChipsetFilter } from './Inputs/MotherboardChipsetFilter.tsx';
import { MemorySlotsFilter } from './Inputs/MemorySlotsFilter.tsx';

export function MotherboardFilters() {
  const [globalFilterData, setGlobalFilterData] = useRecoilState(
    motherboardFiltersAtom
  );

  const [localFilterData, setLocalFilterData] =
    React.useState<IMotherboardFilters>({
      ...globalFilterData,
    });

  const [filtering, setFiltering] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (filtering) {
      setFiltering(false);
    } else {
      setLocalFilterData({ ...globalFilterData });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalFilterData]);

  React.useEffect(() => {
    if (filtering) {
      setGlobalFilterData({ ...localFilterData });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filtering]);

  const handleFilter = () => {
    setFiltering(true);
  };

  const motherboards = useRecoilValue(motherboardsAtom);
  const [minPrice, maxPrice] = motherboards.reduce<[number, number]>(
    ([min, max], curr) => [
      Math.floor(Math.min(min, curr.price)),
      Math.round(Math.max(max, curr.price)),
    ],
    [Infinity, 0]
  );

  const manufacturers = motherboards.reduce<[Set<string>, Manufacturer[]]>(
    ([seen, acc], { manufacturer }) => {
      if (!seen.has(manufacturer.id)) {
        acc.push(manufacturer);
        seen.add(manufacturer.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const sockets = motherboards.reduce<[Set<string>, Socket[]]>(
    ([seen, acc], { socket }) => {
      if (!seen.has(socket.id)) {
        acc.push(socket);
        seen.add(socket.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const formFactors = motherboards.reduce<[Set<string>, FormFactor[]]>(
    ([seen, acc], { formFactor }) => {
      if (!seen.has(formFactor.id)) {
        acc.push(formFactor);
        seen.add(formFactor.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const chipsets = motherboards.reduce<[Set<string>, MotherboardChipset[]]>(
    ([seen, acc], { chipset }) => {
      if (!seen.has(chipset.id)) {
        acc.push(chipset);
        seen.add(chipset.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const [minSlots, maxSlots] = motherboards.reduce<[number, number]>(
    ([min, max], curr) => [
      Math.floor(Math.min(min, curr.memorySlotCount)),
      Math.round(Math.max(max, curr.memorySlotCount)),
    ],
    [Infinity, 0]
  );

  return (
    <div className="filters">
      <h1 className="filters__title">Filters</h1>
      <NameFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
      />
      <PriceFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        minSize={minPrice}
        maxSize={maxPrice}
      />
      <ManufacturerFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        manufacturers={manufacturers}
      />
      <SocketFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        sockets={sockets}
      />
      <FormFactorFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        formFactors={formFactors}
      />
      <ChipsetFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        chipsets={chipsets}
      />
      <MemorySlotsFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        minSize={minSlots}
        maxSize={maxSlots}
      />
      <Button
        variant="contained"
        className="filters__button"
        onClick={handleFilter}
      >
        Filter
      </Button>
    </div>
  );
}
