import {
  Manufacturer,
  Socket,
  IntegratedGraphics,
  MemoryGeneration,
  VideoChipset,
  PcieGeneration,
  MotherboardChipset,
  MotherboardPcieSlot,
  FormFactor,
} from '@pcbuilder/common/api';
import {
  IMemoryFilters,
  IMotherboardFilters,
  IProcessorFilters,
  IVideoCardFilters,
} from '../../state/atoms';

export type IFilters =
  | IProcessorFilters
  | IMotherboardFilters
  | IMemoryFilters
  | IVideoCardFilters;

export interface FilterProps<T extends IFilters> {
  filterData: T;
  setFilterData: React.Dispatch<React.SetStateAction<T>>;
  minSize?: number;
  maxSize?: number;
  manufacturers?: Manufacturer[];
  sockets?: Socket[];
  integratedGraphics?: IntegratedGraphics[];
  generations?: MemoryGeneration[] | PcieGeneration[];
  chipsets?: VideoChipset[] | MotherboardChipset[];
  pcieSlots?: MotherboardPcieSlot[];
  formFactors?: FormFactor[];
}
