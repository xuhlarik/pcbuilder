import { Button } from '@mui/material';
import './Filters.css';
import React from 'react';
import { useRecoilState, useRecoilValue } from 'recoil';
import { Manufacturer, MemoryGeneration } from '@pcbuilder/common/api';
import {
  IMemoryFilters,
  memoriesAtom,
  memoryFiltersAtom,
} from '../../state/atoms';
import { NameFilter } from './Inputs/NameFilter.tsx';
import { PriceFilter } from './Inputs/PriceFilter.tsx';
import { ManufacturerFilter } from './Inputs/ManufacturerFilter.tsx';
import { GenerationFilter } from './Inputs/MemoryGenerationFilter.tsx';

export function MemoryFilters() {
  const [globalFilterData, setGlobalFilterData] =
    useRecoilState(memoryFiltersAtom);

  const [localFilterData, setLocalFilterData] = React.useState<IMemoryFilters>({
    ...globalFilterData,
  });

  const [filtering, setFiltering] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (filtering) {
      setFiltering(false);
    } else {
      setLocalFilterData({ ...globalFilterData });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalFilterData]);

  React.useEffect(() => {
    if (filtering) {
      setGlobalFilterData({ ...localFilterData });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filtering]);

  const handleFilter = () => {
    setFiltering(true);
  };

  const memories = useRecoilValue(memoriesAtom);
  const [minPrice, maxPrice] = memories.reduce<[number, number]>(
    ([min, max], curr) => [
      Math.floor(Math.min(min, curr.price)),
      Math.round(Math.max(max, curr.price)),
    ],
    [Infinity, 0]
  );

  const manufacturers = memories.reduce<[Set<string>, Manufacturer[]]>(
    ([seen, acc], { manufacturer }) => {
      if (!seen.has(manufacturer.id)) {
        acc.push(manufacturer);
        seen.add(manufacturer.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const generations = memories.reduce<[Set<string>, MemoryGeneration[]]>(
    ([seen, acc], { memoryGeneration }) => {
      if (!seen.has(memoryGeneration.id)) {
        acc.push(memoryGeneration);
        seen.add(memoryGeneration.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  return (
    <div className="filters">
      <h1 className="filters__title">Filters</h1>
      <NameFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
      />
      <PriceFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        minSize={minPrice}
        maxSize={maxPrice}
      />
      <ManufacturerFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        manufacturers={manufacturers}
      />
      <GenerationFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        generations={generations}
      />
      <Button
        variant="contained"
        className="filters__button"
        onClick={handleFilter}
      >
        Filter
      </Button>
    </div>
  );
}
