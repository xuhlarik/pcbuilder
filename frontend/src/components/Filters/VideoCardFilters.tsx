import { Button } from '@mui/material';
import './Filters.css';
import React from 'react';
import { useRecoilState, useRecoilValue } from 'recoil';
import {
  Manufacturer,
  PcieGeneration,
  VideoChipset,
} from '@pcbuilder/common/api';
import {
  IVideoCardFilters,
  videoCardFiltersAtom,
  videoCardsAtom,
} from '../../state/atoms';
import { NameFilter } from './Inputs/NameFilter.tsx';
import { PriceFilter } from './Inputs/PriceFilter.tsx';
import { ManufacturerFilter } from './Inputs/ManufacturerFilter.tsx';
import { ChipsetFilter } from './Inputs/VideoCardChipset.tsx';
import { SlotsFilter } from './Inputs/VideoCardSlotsFilter.tsx';
import { PcieGenerationFilter } from './Inputs/PcieGenerationFilter.tsx';

export function VideoCardFilters() {
  const [globalFilterData, setGlobalFilterData] =
    useRecoilState(videoCardFiltersAtom);

  const [localFilterData, setLocalFilterData] =
    React.useState<IVideoCardFilters>({
      ...globalFilterData,
    });

  const [filtering, setFiltering] = React.useState<boolean>(false);

  React.useEffect(() => {
    if (filtering) {
      setFiltering(false);
    } else {
      setLocalFilterData({ ...globalFilterData });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalFilterData]);

  React.useEffect(() => {
    if (filtering) {
      setGlobalFilterData({ ...localFilterData });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filtering]);

  const handleFilter = () => {
    setFiltering(true);
  };

  const videoCards = useRecoilValue(videoCardsAtom);
  const [minPrice, maxPrice] = videoCards.reduce<[number, number]>(
    ([min, max], curr) => [
      Math.floor(Math.min(min, curr.price)),
      Math.round(Math.max(max, curr.price)),
    ],
    [Infinity, 0]
  );

  const manufacturers = videoCards.reduce<[Set<string>, Manufacturer[]]>(
    ([seen, acc], { manufacturer }) => {
      if (!seen.has(manufacturer.id)) {
        acc.push(manufacturer);
        seen.add(manufacturer.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const generations = videoCards.reduce<[Set<string>, PcieGeneration[]]>(
    ([seen, acc], { pcieGeneration }) => {
      if (!seen.has(pcieGeneration.id)) {
        acc.push(pcieGeneration);
        seen.add(pcieGeneration.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const chipsets = videoCards.reduce<[Set<string>, VideoChipset[]]>(
    ([seen, acc], { chipset }) => {
      if (!seen.has(chipset.id)) {
        acc.push(chipset);
        seen.add(chipset.id);
      }
      return [seen, acc];
    },
    [new Set(), []]
  )[1];

  const [minSlots, maxSlots] = videoCards.reduce<[number, number]>(
    ([min, max], curr) => [
      Math.floor(Math.min(min, curr.slotSize)),
      Math.round(Math.max(max, curr.slotSize)),
    ],
    [Infinity, 0]
  );

  return (
    <div className="filters">
      <h1 className="filters__title">Filters</h1>
      <NameFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
      />
      <PriceFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        minSize={minPrice}
        maxSize={maxPrice}
      />
      <ManufacturerFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        manufacturers={manufacturers}
      />
      <PcieGenerationFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        generations={generations}
      />
      <ChipsetFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        chipsets={chipsets}
      />
      <SlotsFilter
        filterData={localFilterData}
        setFilterData={setLocalFilterData}
        minSize={minSlots}
        maxSize={maxSlots}
      />
      <Button
        variant="contained"
        className="filters__button"
        onClick={handleFilter}
      >
        Filter
      </Button>
    </div>
  );
}
