import { IconButton } from '@mui/material';
import ClearIcon from '@mui/icons-material/Clear';
import { LinkButton } from './LinkButton.tsx';
import { makePartPath, priceToString } from '../services/helperFunctions';
import { SellablePart, SellablePartName } from '../types/SellableParts';

export interface ConfigurationItemInput<T> {
  name: SellablePartName;
  part: T | null;
  removePart: (arg0: T) => void;
}
export function ConfigurationItem<T extends SellablePart>({
  part,
  name,
  removePart,
}: ConfigurationItemInput<T>) {
  return (
    <div className="config__part">
      <h2 className="part__title">{`${name}`}:</h2>
      {part === undefined || part === null ? (
        <LinkButton name="Add" url={makePartPath(name)} />
      ) : (
        <div className="part__selected">
          <h3 className="selected__name">{part.name}</h3>
          <span className="selected__manufacturer">
            {part.manufacturer.name}
          </span>
          <span className="selected__price">{priceToString(part.price)}</span>
          <IconButton onClick={() => removePart(part)}>
            <ClearIcon />
          </IconButton>
        </div>
      )}
    </div>
  );
}
