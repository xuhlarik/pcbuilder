import { Link as RouterLink } from 'react-router-dom';
import { Link } from '@mui/material';

export interface NavItem {
  url: string;
  name: string;
}
export function NavItemView({ url, name }: NavItem) {
  return (
    <Link component={RouterLink} to={url}>
      <div>{name}</div>
    </Link>
  );
}
