import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  components: {
    MuiTableCell: {
      styleOverrides: {
        root: {
          color: 'white',
          fontSize: '0.7rem',
        },
      },
    },
    MuiBreadcrumbs: {
      styleOverrides: {
        root: {
          color: 'white',
          '& a': {
            color: 'white',
          },
          textDecoration: 'underline',
          textDecorationColor: 'var(--black)',
          margin: '1rem 4rem',
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          backgroundColor: 'rgb(1, 197, 33)',
          '&:hover': {
            backgroundColor: 'green',
          },
          color: 'var(--white)',
          margin: '0.5rem',
          minWidth: '1.5rem',
        },
      },
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          color: 'var(--white)',
          padding: '0',
        },
      },
    },
    MuiLink: {
      styleOverrides: {
        root: {
          '&:hover': {
            cursor: 'pointer',
          },
        },
      },
    },
    MuiSlider: {
      styleOverrides: {
        root: {
          color: 'var(--green)',
        },
        valueLabel: {
          fontSize: '0.8rem',
        },
      },
    },
    MuiCheckbox: {
      styleOverrides: {
        root: {
          color: 'var(--green)',
          '&.Mui-checked': {
            color: 'var(--green)',
          },
        },
      },
    },
    MuiFormControlLabel: {
      styleOverrides: {
        label: {
          fontSize: '0.8rem',
        },
      },
    },
    MuiFormControl: {
      styleOverrides: {
        root: {
          display: 'flex',
          flexDirection: 'column',
          margin: '0.3rem',
        },
      },
    },
    MuiSelect: {
      styleOverrides: {
        select: {
          backgroundColor: 'var(--white)',
          minWidth: '3rem',
        },
      },
    },
    MuiTable: {
      styleOverrides: {
        root: {
          height: 'fit-content',
          width: '97%',
        },
      },
    },
  },
});
