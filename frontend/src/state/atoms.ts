import { atom } from 'recoil';
import {
  FormFactor,
  IntegratedGraphics,
  Manufacturer,
  Memory,
  MemoryGeneration,
  Motherboard,
  MotherboardChipset,
  MotherboardPcieSlot,
  PcieGeneration,
  Processor,
  Socket,
  VideoCard,
  VideoChipset,
} from '@pcbuilder/common/api';
import { ConfigurationType } from '../types/Configuration';

export const auth = atom<boolean>({
  key: 'auth',
  default: false,
});

export const configurationAtom = atom<ConfigurationType>({
  key: 'configuration',
  default: {
    processor: null,
    memory: null,
    motherboard: null,
    videoCard: null,
    preset: {},
  },
});

export const processorsAtom = atom<Processor[]>({
  key: 'processors',
  default: [],
});

export interface IProcessorFilters {
  name: string;
  manufacturers: Manufacturer[];
  sockets: Socket[];
  priceRange: number[];
  integratedGraphics: IntegratedGraphics[];
}

export const processorFiltersAtom = atom<IProcessorFilters>({
  key: 'processorFilters',
  default: {
    name: '',
    manufacturers: [],
    sockets: [],
    priceRange: [0, 10],
    integratedGraphics: [],
  },
});

export const motherboardsAtom = atom<Motherboard[]>({
  key: 'motherboards',
  default: [],
});

export interface IMotherboardFilters {
  name: string;
  manufacturers: Manufacturer[];
  sockets: Socket[];
  priceRange: number[];
  formFactors: FormFactor[];
  chipsets: MotherboardChipset[];
  pcieSlots: MotherboardPcieSlot[];
  memorySlotsRange: number[];
}

export const motherboardFiltersAtom = atom<IMotherboardFilters>({
  key: 'motherboardFilters',
  default: {
    name: '',
    manufacturers: [],
    sockets: [],
    priceRange: [0, 10],
    formFactors: [],
    chipsets: [],
    pcieSlots: [],
    memorySlotsRange: [0, 10],
  },
});

export const memoriesAtom = atom<Memory[]>({
  key: 'memories',
  default: [],
});

export interface IMemoryFilters {
  name: string;
  manufacturers: Manufacturer[];
  priceRange: number[];
  generations: MemoryGeneration[];
}

export const memoryFiltersAtom = atom<IMemoryFilters>({
  key: 'memoryFilters',
  default: {
    name: '',
    manufacturers: [],
    priceRange: [0, 10],
    generations: [],
  },
});

export const videoCardsAtom = atom<VideoCard[]>({
  key: 'videoCards',
  default: [],
});

export interface IVideoCardFilters {
  name: string;
  manufacturers: Manufacturer[];
  priceRange: number[];
  generations: PcieGeneration[];
  chipsets: VideoChipset[];
  slotSizeRange: number[];
}

export const videoCardFiltersAtom = atom<IVideoCardFilters>({
  key: 'videoCardFilters',
  default: {
    name: '',
    manufacturers: [],
    priceRange: [0, 10],
    generations: [],
    chipsets: [],
    slotSizeRange: [0, 10],
  },
});

export type Order = 'desc' | 'asc';

export const sortNameAtom = atom<Order>({
  key: 'sortName',
  default: 'asc',
});

export const sortPriceAtom = atom<Order>({
  key: 'sortPrice',
  default: 'asc',
});

export const sortingAtom = atom<'name' | 'price'>({
  key: 'sorting',
  default: 'name',
});
