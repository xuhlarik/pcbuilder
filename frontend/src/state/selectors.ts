import { selector } from 'recoil';
import {
  Processor,
  Motherboard,
  Memory,
  VideoCard,
} from '@pcbuilder/common/api';
import {
  processorsAtom,
  processorFiltersAtom,
  motherboardsAtom,
  motherboardFiltersAtom,
  memoriesAtom,
  memoryFiltersAtom,
  videoCardsAtom,
  videoCardFiltersAtom,
  sortNameAtom,
  sortingAtom,
  sortPriceAtom,
} from './atoms';

const isEmptyOrHas = <T extends { id: string }>(set: T[], value: T) =>
  set.length === 0 || set.findIndex((x) => x?.id === value?.id) !== -1;

export const filteredProcessorsSelector = selector({
  key: 'filteredProcessorsSelector',
  get({ get }) {
    const processors = get<Processor[]>(processorsAtom);
    const processorFilters = get(processorFiltersAtom);
    const sorting = get(sortingAtom);
    const sortName = get(sortNameAtom);
    const sortPrice = get(sortPriceAtom);

    console.log(processorFilters);
    return processors
      .filter((processor) => {
        if (
          !processor.name
            .toLowerCase()
            .includes(processorFilters.name.toLowerCase())
        ) {
          return false;
        }
        if (
          !isEmptyOrHas(processorFilters.manufacturers, processor.manufacturer)
        ) {
          return false;
        }
        if (!isEmptyOrHas(processorFilters.sockets, processor.socket)) {
          return false;
        }
        if (
          !isEmptyOrHas(
            processorFilters.integratedGraphics,
            processor.integratedGraphics
          )
        ) {
          return false;
        }
        if (!isEmptyOrHas(processorFilters.sockets, processor.socket)) {
          return false;
        }
        if (
          processorFilters.priceRange[0] > processor.price ||
          processorFilters.priceRange[1] < processor.price
        ) {
          return false;
        }
        return true;
      })
      .sort((p1, p2) => {
        if (sorting === 'name') {
          if (p1.name.toLowerCase() > p2.name.toLowerCase()) {
            return sortName === 'asc' ? 1 : -1;
          }
          if (p1.name.toLowerCase() < p2.name.toLowerCase()) {
            return sortName === 'asc' ? -1 : 1;
          }
          return 0;
        }
        if (p1.price > p2.price) {
          return sortPrice === 'asc' ? 1 : -1;
        }
        if (p1.price < p2.price) {
          return sortPrice === 'asc' ? -1 : 1;
        }
        return 0;
      });
  },
});

export const filteredMotherboardsSelector = selector({
  key: 'filteredMotherboardsSelector',
  get({ get }) {
    const motherboards = get<Motherboard[]>(motherboardsAtom);
    const motherboardFilters = get(motherboardFiltersAtom);
    const sorting = get(sortingAtom);
    const sortName = get(sortNameAtom);
    const sortPrice = get(sortPriceAtom);

    return motherboards
      .filter((motherboard) => {
        if (
          !motherboard.name
            .toLowerCase()
            .includes(motherboardFilters.name.toLowerCase())
        ) {
          return false;
        }
        if (
          !isEmptyOrHas(
            motherboardFilters.manufacturers,
            motherboard.manufacturer
          )
        ) {
          return false;
        }
        if (!isEmptyOrHas(motherboardFilters.chipsets, motherboard.chipset)) {
          return false;
        }
        if (
          !isEmptyOrHas(motherboardFilters.formFactors, motherboard.formFactor)
        ) {
          return false;
        }
        if (
          !isEmptyOrHas(motherboardFilters.pcieSlots, motherboard.pcieSlots)
        ) {
          return false;
        }
        if (!isEmptyOrHas(motherboardFilters.sockets, motherboard.socket)) {
          return false;
        }
        if (
          motherboardFilters.priceRange[0] > motherboard.price ||
          motherboardFilters.priceRange[1] < motherboard.price
        ) {
          return false;
        }
        if (
          motherboardFilters.memorySlotsRange[0] >
            motherboard.memorySlotCount ||
          motherboardFilters.memorySlotsRange[1] < motherboard.memorySlotCount
        ) {
          return false;
        }
        return true;
      })
      .sort((p1, p2) => {
        if (sorting === 'name') {
          if (p1.name.toLowerCase() > p2.name.toLowerCase()) {
            return sortName === 'asc' ? 1 : -1;
          }
          if (p1.name.toLowerCase() < p2.name.toLowerCase()) {
            return sortName === 'asc' ? -1 : 1;
          }
          return 0;
        }
        if (p1.price > p2.price) {
          return sortPrice === 'asc' ? 1 : -1;
        }
        if (p1.price < p2.price) {
          return sortPrice === 'asc' ? -1 : 1;
        }
        return 0;
      });
  },
});

export const filteredMemoriesSelector = selector({
  key: 'filteredMemoriesSelector',
  get({ get }) {
    const memories = get<Memory[]>(memoriesAtom);
    const memoryFilters = get(memoryFiltersAtom);
    const sorting = get(sortingAtom);
    const sortName = get(sortNameAtom);
    const sortPrice = get(sortPriceAtom);

    return memories
      .filter((memory) => {
        if (
          !memory.name.toLowerCase().includes(memoryFilters.name.toLowerCase())
        ) {
          return false;
        }
        if (!isEmptyOrHas(memoryFilters.manufacturers, memory.manufacturer)) {
          return false;
        }
        if (!isEmptyOrHas(memoryFilters.generations, memory.generation)) {
          return false;
        }
        if (
          memoryFilters.priceRange[0] > memory.price ||
          memoryFilters.priceRange[1] < memory.price
        ) {
          return false;
        }
        return true;
      })
      .sort((p1, p2) => {
        if (sorting === 'name') {
          if (p1.name.toLowerCase() > p2.name.toLowerCase()) {
            return sortName === 'asc' ? 1 : -1;
          }
          if (p1.name.toLowerCase() < p2.name.toLowerCase()) {
            return sortName === 'asc' ? -1 : 1;
          }
          return 0;
        }
        if (p1.price > p2.price) {
          return sortPrice === 'asc' ? 1 : -1;
        }
        if (p1.price < p2.price) {
          return sortPrice === 'asc' ? -1 : 1;
        }
        return 0;
      });
  },
});

export const filteredVideoCardsSelector = selector({
  key: 'filteredVideoCardsSelector',
  get({ get }) {
    const videoCards = get<VideoCard[]>(videoCardsAtom);
    const videoCardFilters = get(videoCardFiltersAtom);
    const sorting = get(sortingAtom);
    const sortName = get(sortNameAtom);
    const sortPrice = get(sortPriceAtom);

    return videoCards
      .filter((videoCard) => {
        if (
          !videoCard.name
            .toLowerCase()
            .includes(videoCardFilters.name.toLowerCase())
        ) {
          return false;
        }
        if (
          !isEmptyOrHas(videoCardFilters.manufacturers, videoCard.manufacturer)
        ) {
          return false;
        }
        if (!isEmptyOrHas(videoCardFilters.chipsets, videoCard.chipset)) {
          return false;
        }
        if (!isEmptyOrHas(videoCardFilters.generations, videoCard.generation)) {
          return false;
        }
        if (
          videoCardFilters.priceRange[0] > videoCard.price ||
          videoCardFilters.priceRange[1] < videoCard.price
        ) {
          return false;
        }
        if (
          videoCardFilters.slotSizeRange[0] > videoCard.slotSize ||
          videoCardFilters.slotSizeRange[1] < videoCard.slotSize
        ) {
          return false;
        }
        return true;
      })
      .sort((p1, p2) => {
        if (sorting === 'name') {
          if (p1.name.toLowerCase() > p2.name.toLowerCase()) {
            return sortName === 'asc' ? 1 : -1;
          }
          if (p1.name.toLowerCase() < p2.name.toLowerCase()) {
            return sortName === 'asc' ? -1 : 1;
          }
          return 0;
        }
        if (p1.price > p2.price) {
          return sortPrice === 'asc' ? 1 : -1;
        }
        if (p1.price < p2.price) {
          return sortPrice === 'asc' ? -1 : 1;
        }
        return 0;
      });
  },
});
