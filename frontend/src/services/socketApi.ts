import { schema, Socket } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const readManySockets = async (
  data: z.infer<typeof schema.Socket.readMany> = {}
) => {
  const response = await axios.get<Socket[]>('/socket', {
    params: data,
  });
  return response.data;
};
