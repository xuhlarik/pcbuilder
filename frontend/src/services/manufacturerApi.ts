import { schema, Manufacturer } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const createManufacturer = async (
  data: z.infer<typeof schema.manufacturer.create>
) => {
  const response = await axios.post<Manufacturer>('/manufacturer', data);
  return response.data;
};

export const deleteManufacturer = async (
  id: string,
  data: z.infer<typeof schema.manufacturer.delete> = {}
) => {
  await axios.delete<void>(`/manufacturer/${id}`, {
    params: data,
  });
};

export const readOneManufacturer = async (
  id: string,
  data: z.infer<typeof schema.manufacturer.readOne> = {}
) => {
  const response = await axios.get<Manufacturer>(`/manufacturer/${id}`, {
    params: data,
  });
  return response.data;
};

export const readManyManufacturers = async (
  data: z.infer<typeof schema.manufacturer.readMany> = {}
) => {
  const response = await axios.get<Manufacturer[]>('/manufacturer', {
    params: data,
  });
  return response.data;
};

export const updateManufacturer = async (
  id: string,
  data: z.infer<typeof schema.manufacturer.update> = {}
) => {
  const response = await axios.patch<Manufacturer>(`/manufacturer/${id}`, data);
  return response.data;
};

export default {
  create: createManufacturer,
  delete: deleteManufacturer,
  readOne: readOneManufacturer,
  readMany: readManyManufacturers,
  update: updateManufacturer,
};
