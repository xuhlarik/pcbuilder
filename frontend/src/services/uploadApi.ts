import { z } from 'zod';
import axios from './base';

export const uploadImage = async (data: z.infer<File> = {}) => {
  const formData = new FormData();
  formData.append('file', data[0]);
  const response = await axios.post('/upload', formData, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  return response.data;
};
