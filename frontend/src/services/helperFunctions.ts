import { ConfigurationType } from '../types/Configuration';
import { ApiError } from '../types/Error';

export function makePartPath(name: string) {
  return `/parts/${name.replace(' ', '').toLowerCase()}`;
}
export function priceToString(price: number) {
  return `${price.toFixed(2)}$`;
}

export function calculatePrice(configuration: ConfigurationType) {
  return (
    (configuration.processor?.price ?? 0) +
    (configuration.motherboard?.price ?? 0) +
    (configuration.memory?.price ?? 0) +
    (configuration.videoCard?.price ?? 0)
  );
}

export function message(error: ApiError, object: string) {
  if (error.response === undefined) {
    return 'Connection error';
  }
  if (error.response.status >= 500) {
    return 'Server Error';
  }
  switch (error.response.status) {
    case 404:
      return `${object} not found`;
    case 401:
      return error.response.data?.message ?? 'Unathorized';
    case 403:
      return error.response.data?.message ?? 'Forbiden';
    case 400:
      return error.response.data?.message ?? 'Input error';
    default:
      return error.response.data?.message ?? 'User Error';
  }
}
