import { schema, FormFactor } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const readManyFormFactors = async (
  data: z.infer<typeof schema.FormFactor.readMany> = {}
) => {
  const response = await axios.get<FormFactor[]>('/form-factor', {
    params: data,
  });
  return response.data;
};
