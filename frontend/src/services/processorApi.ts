import schema, { Processor } from '@pcbuilder/common/api';
import { z } from 'zod';
import axiosInstance from './base';

export const readOneProcessor = async (
  id: string,
  data: z.infer<typeof schema.processor.readOne>
) => {
  const response = await axiosInstance.get<Processor>(`/processor/${id}`, {
    params: data,
  });
  return response.data;
};

export const createProcessor = async (
  data: z.infer<typeof schema.processor.create>
) => {
  const response = await axiosInstance.post<Processor>('/processor', data);
  return response.data;
};
export const deleteProcessor = async (
  id: string,
  data: z.infer<typeof schema.processor.delete> = {}
) => {
  const response = await axiosInstance.delete<void>(`/processor/${id}`, {
    params: data,
  });
  return response.data;
};
export const readManyProcessors = async (
  data: z.infer<typeof schema.processor.readMany> = {}
) => {
  const response = await axiosInstance.get<Processor[]>(`/processor`, {
    params: data,
  });
  return response.data;
};
export const updateProcessor = async (
  id: string,
  data: z.infer<typeof schema.processor.update>
) => {
  const response = await axiosInstance.patch<Processor>(`/processor/${id}`, {
    params: data,
  });
  return response.data;
};
