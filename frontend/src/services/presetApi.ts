import schema, { Preset } from '@pcbuilder/common/api';
import { z } from 'zod';
import axiosInstance from './base';
import { AddPresetType } from '../types/Configuration';

export const readManyPresets = async (
  data: z.infer<typeof schema.preset.readMany> = {}
) => {
  const response = await axiosInstance.get<Preset[]>(`/preset/`, {
    params: data,
  });
  return response.data;
};

export const updatePreset = async (
  id: string,
  data: z.infer<typeof schema.preset.update>
) => {
  const response = await axiosInstance.patch<Preset>(`/preset/${id}`, data);
  return response.data;
};
export const deletePreset = async (
  id: string,
  data: z.infer<typeof schema.preset.delete> = {}
) => {
  const response = await axiosInstance.delete(`/preset/${id}`, data);
  return response.data;
};
export const createPreset = async (data: AddPresetType) => {
  const response = await axiosInstance.post<Preset>(`/preset/`, data);
  return response.data;
};

export default {
  readMany: readManyPresets,
  update: updatePreset,
  create: createPreset,
  delete: deletePreset,
};
