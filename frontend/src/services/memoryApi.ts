import { schema, Memory } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const createMemory = async (
  data: z.infer<typeof schema.memory.create>
) => {
  const response = await axios.post<Memory>('/memory', data);
  return response.data;
};

export const readManyMemories = async (
  data: z.infer<typeof schema.memory.readMany> = {}
) => {
  const response = await axios.get<Memory[]>('/memory', {
    params: data,
  });
  return response.data;
};

export const deleteMemory = async (
  id: string,
  data: z.infer<typeof schema.memory.delete> = {}
) => {
  const response = await axios.delete<void>(`/memory/${id}`, {
    params: data,
  });
  return response.data;
};
