import manufacturer from './manufacturerApi';
import categoryApi from './categoryApi';
import presetApi from './presetApi';

export default {
  manufacturer,
  categoryApi,
  presetApi,
};
