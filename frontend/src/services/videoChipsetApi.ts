import { schema, VideoChipset } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const readManyChipsets = async (
  data: z.infer<typeof schema.VideoChipset.readMany> = {}
) => {
  const response = await axios.get<VideoChipset[]>('/video-chipset', {
    params: data,
  });
  return response.data;
};
