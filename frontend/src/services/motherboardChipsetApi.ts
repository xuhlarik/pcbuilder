import { schema, MotherboardChipset } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const readManyChipsets = async (
  data: z.infer<typeof schema.MotherboardChipset.readMany> = {}
) => {
  const response = await axios.get<MotherboardChipset[]>(
    '/motherboard-chipset',
    {
      params: data,
    }
  );
  return response.data;
};
