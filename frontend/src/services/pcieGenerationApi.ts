import { schema, PcieGeneration } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const readManyGenerations = async (
  data: z.infer<typeof schema.PcieGeneration.readMany> = {}
) => {
  const response = await axios.get<PcieGeneration[]>('/pcie-generation', {
    params: data,
  });
  return response.data;
};
