import schema, {
  Category,
  CategoryWithPresets,
  Preset,
} from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export type OneCategoryType = Category & {
  presets: Preset[];
};
export const readOneCategory = async (
  id: string,
  data: z.infer<typeof schema.category.readOne> = {}
) => {
  const response = await axios.get<CategoryWithPresets>(`/category/${id}`, {
    params: data,
  });
  return response.data;
};

export const readManyCategories = async (
  data: z.infer<typeof schema.category.readMany> = {}
) => {
  const response = await axios.get<Category[]>('/category/', {
    params: data,
  });
  return response.data;
};

export default {
  // create: createBuild,
  //  delete: deleteBuild,
  readOne: readOneCategory,
  readMany: readManyCategories,
  // update: updateBuild,
};
