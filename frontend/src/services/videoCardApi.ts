import { schema, VideoCard } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const createVideoCard = async (
  data: z.infer<typeof schema.videoCard.create>
) => {
  const response = await axios.post<VideoCard>('/video-card', data);
  return response.data;
};

export const readManyVideoCards = async (
  data: z.infer<typeof schema.videoCard.readMany> = {}
) => {
  const response = await axios.get<VideoCard[]>('/video-card', {
    params: data,
  });
  return response.data;
};

export const deleteVideoCards = async (
  id: string,
  data: z.infer<typeof schema.videoCard.delete> = {}
) => {
  const response = await axios.delete<void>(`/video-card/${id}`, {
    params: data,
  });
  return response.data;
};
