import { schema, Motherboard } from '@pcbuilder/common/api';
import { z } from 'zod';
import axiosInstance from './base';

export const createMotherboard = async (
  data: z.infer<typeof schema.motherboard.create>
) => {
  const response = await axiosInstance.post<Motherboard>('/motherboard', data);
  return response.data;
};

export const readManyMotherboards = async (
  data: z.infer<typeof schema.motherboard.readMany> = {}
) => {
  const response = await axiosInstance.get<Motherboard[]>('/motherboard', {
    params: data,
  });
  return response.data;
};

export const deleteMotherboard = async (
  id: string,
  data: z.infer<typeof schema.motherboard.delete> = {}
) => {
  const response = await axiosInstance.delete<void>(`/motherboard/${id}`, {
    params: data,
  });
  return response.data;
};
