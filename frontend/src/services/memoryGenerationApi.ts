import { schema, MemoryGeneration } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const readManyGenerations = async (
  data: z.infer<typeof schema.MemoryGeneration.readMany> = {}
) => {
  const response = await axios.get<MemoryGeneration[]>('/memory-generation', {
    params: data,
  });
  return response.data;
};
