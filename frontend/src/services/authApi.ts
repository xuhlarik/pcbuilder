import { schema, Account } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const loginApi = async (data: z.infer<typeof schema.auth.login>) => {
  const response = await axios.post<Account>('/auth', data);
  return response.data;
};

export const logoutApi = async () => {
  await axios.delete<void>('/auth');
};
