import schema, { Build } from '@pcbuilder/common/api';
import { z } from 'zod';

import axiosInstance from './base';

export const readOneBuild = async (id: string) => {
  const response = await axiosInstance.get<Build>(`/build/${id}`);
  return response.data;
};

export const createBuild = async (
  data: z.infer<typeof schema.build.create>
) => {
  const response = await axiosInstance.post<Build>('/build/', data);
  return response.data;
};
export const deleteBuild = async (
  id: string,
  data: z.infer<typeof schema.build.delete> = {}
) => {
  const response = await axiosInstance.delete<Build>(`/build/${id}`, {
    params: data,
  });
  return response.data;
};
export const readManyBuild = async (
  id: string,
  data: z.infer<typeof schema.build.update> = {}
) => {
  const response = await axiosInstance.get<Build[]>(`/build/${id}`, {
    params: data,
  });
  return response.data;
};
export const updateBuild = async (
  id: string,
  data: z.infer<typeof schema.build.update> = {}
) => {
  const response = await axiosInstance.patch<Build>(`/build/${id}`, data);
  return response.data;
};

export default {
  create: createBuild,
  delete: deleteBuild,
  readOne: readOneBuild,
  readMany: readManyBuild,
  update: updateBuild,
};
