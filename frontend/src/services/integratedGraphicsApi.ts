import { schema, IntegratedGraphics } from '@pcbuilder/common/api';
import { z } from 'zod';
import axios from './base';

export const readManyIntegratedGraphics = async (
  data: z.infer<typeof schema.IntegratedGraphics.readMany> = {}
) => {
  const response = await axios.get<IntegratedGraphics[]>(
    '/integrated-graphics',
    {
      params: data,
    }
  );
  return response.data;
};
