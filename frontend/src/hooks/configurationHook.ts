import {
  Build,
  Memory,
  Motherboard,
  Preset,
  Processor,
  VideoCard,
} from '@pcbuilder/common/api';

import { useMutation } from 'react-query';
import { useRecoilState, useResetRecoilState } from 'recoil';
import { configurationAtom } from '../state/atoms';

import buildApi from '../services/buildApi';
import { ApiError } from '../types/Error';
import {
  AddBuildType,
  AddPresetType,
  ConfigurationType,
  EditBuildType,
  EditConfigType,
  PresetConfig,
} from '../types/Configuration';
import presetApi from '../services/presetApi';

export function useConfiguration() {
  const [configuration, setConfiguration] =
    useRecoilState<ConfigurationType>(configurationAtom);

  const reset = useResetRecoilState(configurationAtom);

  const {
    mutate: addConfigutation,
    isLoading: loadingAddConfig,
    isError: isAddConfigError,
    error: addConfigError,
  } = useMutation<Preset, ApiError, AddPresetType>(
    ['addPreset'],
    (preset: AddPresetType) => presetApi.create(preset)
  );

  const {
    mutate: addBuildServer,
    isLoading: loadingAddBuild,
    isError: isAddBuildError,
    error: addBuildError,
  } = useMutation<Build, ApiError, AddBuildType>(
    ['addBuild'],
    (build: AddBuildType) => buildApi.create(build),
    {
      onSuccess: (b: Build) =>
        addConfigutation({
          buildId: b.id,
          name: configuration.preset.name ?? '',
          description: configuration.preset.description ?? '',
          categoryId: configuration.preset.categoryId ?? '',
        }),
    }
  );

  const {
    mutate: editBuildServer,
    isLoading: loadingEditBuild,
    isError: isEditBuildError,
    error: editBuildError,
  } = useMutation<Build, ApiError, EditBuildType>(
    ['editBuild'],
    (editBuild: EditBuildType) => buildApi.update(editBuild.id, editBuild.data)
  );

  const {
    mutate: editConfiguration,
    isLoading: loadingEditConfig,
    isError: isEditConfigError,
    error: editConfigError,
  } = useMutation<Preset, ApiError, EditConfigType>(
    ['editPreset'],
    (editConfig: EditConfigType) =>
      presetApi.update(editConfig.id, editConfig.data)
  );

  function haveName() {
    return (
      configuration.preset?.name !== undefined &&
      configuration.preset.name.length > 0 &&
      configuration.preset?.description !== undefined &&
      configuration.preset.description.length > 0 &&
      configuration.preset?.categoryId !== undefined
    );
  }

  function isFull() {
    return (
      configuration.processor !== null &&
      configuration.motherboard !== null &&
      configuration.memory !== null &&
      configuration.videoCard !== null &&
      haveName()
    );
  }

  // if not add is edit
  function isAdd() {
    return (
      configuration.preset.id === undefined || configuration.id === undefined
    );
  }

  function getIdsParts() {
    return {
      processorId: configuration.processor?.id ?? null,
      memoryId: configuration.memory?.id ?? null,
      motherboardId: configuration.motherboard?.id ?? null,
      videoCardId: configuration.videoCard?.id ?? null,
    };
  }

  function send() {
    if (!isAdd()) {
      if (haveName()) {
        editConfiguration({
          id: configuration.preset.id,
          data: {
            name: configuration.preset.name,
            description: configuration.preset.description,
            categoryId: configuration.preset.categoryId,
          },
        });
      }
      editBuildServer({ id: configuration.id, data: getIdsParts() });
      reset();
      return;
    }

    if (!isFull()) {
      return;
    }

    addBuildServer(getIdsParts());
    reset();
  }

  function addBuild(build: Build) {
    setConfiguration({ ...configuration, ...build });
  }
  function addNameDesc(nameDesc: Preset) {
    setConfiguration({
      ...configuration,
      preset: {
        ...configuration.preset,
        name: nameDesc.name,
        description: nameDesc.description,
      },
    });
  }

  function addPresetAtributes(preset: PresetConfig) {
    setConfiguration({ ...configuration, preset });
  }

  function addMotherboard(motherboard: Motherboard) {
    setConfiguration({ ...configuration, motherboard });
  }
  function addProcesor(processor: Processor) {
    setConfiguration({ ...configuration, processor });
  }
  function addVideoCard(videoCard: VideoCard) {
    setConfiguration({ ...configuration, videoCard });
  }
  function addMemory(memory: Memory) {
    setConfiguration({ ...configuration, memory });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  function removeProcesor(_: Processor) {
    setConfiguration({ ...configuration, processor: null });
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  function removeMotherBoard(_: Motherboard) {
    setConfiguration({ ...configuration, motherboard: null });
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  function removeMemory(_: Memory) {
    setConfiguration({ ...configuration, memory: null });
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  function removeVideoCard(_: VideoCard) {
    setConfiguration({ ...configuration, videoCard: null });
  }

  function addCategory(categoryId: string) {
    setConfiguration({
      ...configuration,
      preset: { ...configuration.preset, categoryId },
    });
  }

  function isError() {
    return (
      isAddBuildError ||
      isAddConfigError ||
      isEditBuildError ||
      isEditConfigError
    );
  }

  function error() {
    if (!isError()) {
      return undefined;
    }
    if (addBuildError) {
      return addBuildError;
    }

    if (addConfigError) {
      return addConfigError;
    }

    if (editBuildError) {
      return editBuildError;
    }

    if (editConfigError) {
      return editConfigError;
    }
    return undefined;
  }

  return {
    addCategory,
    addMotherboard,
    addProcesor,
    addVideoCard,
    addMemory,
    removeMemory,
    removeMotherBoard,
    removeProcesor,
    removeVideoCard,
    addBuild,
    addNameDesc,
    send,
    isAdd,
    isFull,
    addPresetAtributes,
    loading:
      loadingAddBuild ||
      loadingAddConfig ||
      loadingEditBuild ||
      loadingEditConfig,

    configuration,
    isError: isError(),
    error: error(),
  };
}
