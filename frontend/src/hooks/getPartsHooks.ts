import { useQuery } from 'react-query';
import { ApiError } from '../types/Error';
import { SellablePart } from '../types/SellableParts';

export function useGetParts<T extends SellablePart>(
  query: string[],
  getParts: () => Promise<T[]>
) {
  const { isLoading, isError, error, data } = useQuery<T[], ApiError>(
    query,
    () => getParts()
  );
  return { isLoading, isError, error, data };
}
