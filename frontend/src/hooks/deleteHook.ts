import { MutationFunction, useMutation, useQueryClient } from 'react-query';
import { ApiError } from '../types/Error';

export function useDelete(
  query: string[],
  deleteFn: MutationFunction<void, string>,
  additionalQueryies: string[][] = []
) {
  const queryClient = useQueryClient();
  const { mutate, isLoading, isError, error } = useMutation<
    void,
    ApiError,
    string
  >([`${query}del`], (id) => deleteFn(id), {
    onSuccess: () => {
      queryClient.invalidateQueries(query);
      additionalQueryies.map((oneQuery) =>
        queryClient.invalidateQueries(oneQuery)
      );
    },
  });

  return {
    deleteFn: mutate,
    deleteLoading: isLoading,
    errorDelete: error,
    isErrorDelete: isError,
  };
}
