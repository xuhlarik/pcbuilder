import schema, {
  Memory,
  Motherboard,
  Processor,
  VideoCard,
} from '@pcbuilder/common/api';
import { z } from 'zod';

export interface ConfigurationType {
  processor: Processor | null;
  motherboard: Motherboard | null;
  memory: Memory | null;
  videoCard: VideoCard | null;
  preset: PresetConfig;
  id?: string;
}

export interface PresetConfig {
  id?: string;
  categoryId?: string;
  description?: string;
  name?: string;
}

export interface DeleteType {
  id: string;
}

export type AddBuildType = z.infer<typeof schema.build.create>;
export type AddPresetType = z.infer<typeof schema.preset.create>;
export type EditConfigType = {
  data: z.infer<typeof schema.preset.update>;
  id: string;
};
export type EditBuildType = {
  data: z.infer<typeof schema.build.update>;
  id: string;
};
