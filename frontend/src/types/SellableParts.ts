import {
  Memory,
  Motherboard,
  Processor,
  VideoCard,
} from '@pcbuilder/common/api';

export type SellablePartName =
  | 'Processor'
  | 'Motherboard'
  | 'Memory'
  | 'Video Card';
export type SellablePart = Memory | Motherboard | VideoCard | Processor;
