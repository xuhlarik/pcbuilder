import { ThemeProvider } from '@mui/material/styles';
import { Route, Routes } from 'react-router-dom';
import { Header } from './components/Header/Header.tsx';
import { Footer } from './components/Footer/Footer.tsx';
import { Home } from './pages/Home.tsx';
import { OnePreset } from './pages/OnePreset.tsx';
import { Presets } from './pages/Presets/Presets.tsx';
import { Configuration } from './pages/Configuration/Configuration.tsx';
import { OneCategory } from './pages/Presets/OneCategory.tsx';
import { Login } from './pages/Login/Login.tsx';
import { AddPart } from './pages/AddPart/AddPart.tsx';
import { AddConfig } from './pages/AddConfig/AddConfig.tsx';
import { theme } from './themes/muiTheme';
import { Parts } from './pages/Parts/Parts.tsx';
import { ProcessorParts } from './pages/Parts/ProcessorParts.tsx';
import { MotherBoardParts } from './pages/Parts/MotherboardParts.tsx';
import { VideoCardParts } from './pages/Parts/VideoCardParts.tsx';
import { MemoryParts } from './pages/Parts/MemoryParts.tsx';
import { NotFoundPage } from './pages/NotFoundPage.tsx';
import { Summary } from './pages/Summary/Summary.tsx';
import { AdminPage } from './pages/AdminPage.tsx';
import { DeletePart } from './pages/DeletePart.tsx';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <div className="main">
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="config" element={<Configuration />} />
          <Route path="presets" element={<Presets />} />
          <Route path="category/:categoryId" element={<OneCategory />} />
          <Route path="presets/:presetId" element={<OnePreset />} />
          <Route path="admin" element={<AdminPage />}>
            <Route path="add/config" element={<AddConfig />} />
            <Route path="add/part" element={<AddPart />} />
            <Route path="delete/part" element={<DeletePart />} />
          </Route>
          <Route path="parts" element={<Parts />} />
          <Route path="parts/processor" element={<ProcessorParts />} />
          <Route path="parts/motherboard" element={<MotherBoardParts />} />
          <Route path="parts/videocard" element={<VideoCardParts />} />
          <Route path="parts/memory" element={<MemoryParts />} />
          <Route path="summary" element={<Summary />} />
          <Route path="login" element={<Login />} />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
        <Footer />
      </div>
    </ThemeProvider>
  );
}

export default App;
