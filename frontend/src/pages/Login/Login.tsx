import { useForm } from 'react-hook-form';
import {
  Button,
  IconButton,
  InputAdornment,
  OutlinedInput,
} from '@mui/material';
import { useMutation } from 'react-query';
import './Login.css';
import { useNavigate } from 'react-router-dom';
import { useSetRecoilState } from 'recoil';
import { useState } from 'react';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { zodResolver } from '@hookform/resolvers/zod';
import { z } from 'zod';

import schema, { Account } from '@pcbuilder/common/api';
import { auth } from '../../state/atoms';
import { loginApi } from '../../services/authApi';
import { ApiError } from '../../types/Error';
import { ErrorView } from '../../components/ErrorView.tsx';

export type User = z.infer<typeof schema.auth.login>;
export function Login() {
  const navigate = useNavigate();
  const [showPassword, setShowPassword] = useState(false);

  const setAuth = useSetRecoilState(auth);
  const { register, handleSubmit, reset } = useForm<User>({
    resolver: zodResolver(schema.auth.login),
    defaultValues: { login: '', password: '' },
  });
  const {
    mutate,
    isLoading,
    error: responseError,
    isError,
  } = useMutation<Account, ApiError, User>(
    'login',
    (user: User) => loginApi(user),
    {
      onSuccess: () => {
        setAuth(true);
        navigate('/');
      },
    }
  );
  function login(user: User) {
    reset({
      login: user.login,
      password: '',
    });
    mutate(user);
  }

  return (
    <>
      {isError === true && <ErrorView dataType="User" error={responseError} />}
      <form onSubmit={handleSubmit(login)} className="login__form">
        <h1 className="form__title">Log In</h1>
        <label className="form__name">Username:</label>
        <OutlinedInput
          required
          type="text"
          {...register('login')}
          className="form__name--input"
        />
        <label className="form__password">Password:</label>
        <OutlinedInput
          required
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={() => setShowPassword(!showPassword)}
                edge="end"
              >
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          }
          type={showPassword ? 'text' : 'password'}
          {...register('password')}
          className="form__password--input"
        />
        <Button
          disabled={isLoading}
          type="submit"
          color="info"
          variant="contained"
          className="form__button"
        >
          Log In
        </Button>
      </form>
    </>
  );
}
