import { useRecoilValue } from 'recoil';
import { useNavigate } from 'react-router-dom';
import { Button } from '@mui/material';
import { SummaryItem } from '../../components/SummaryItem.tsx';
import { useConfiguration } from '../../hooks/configurationHook';
import { priceToString, calculatePrice } from '../../services/helperFunctions';
import './Summary.css';
import { auth } from '../../state/atoms';
import { LinkButton } from '../../components/LinkButton.tsx';
import { ErrorView } from '../../components/ErrorView.tsx';

export function Summary() {
  const { configuration, isAdd, isFull, send, loading, isError, error } =
    useConfiguration();
  const admin = useRecoilValue(auth);
  const navigate = useNavigate();
  return (
    <div className="summary">
      {isError && error !== undefined ? (
        <>
          {' '}
          <ErrorView dataType="build" error={error} />
          {console.log(error)}
        </>
      ) : (
        ''
      )}
      <h1 className="summary__title">Here is your build</h1>
      <div className="summary__build">
        {configuration.processor !== null && (
          <SummaryItem name="Processor" part={configuration.processor} />
        )}
        {configuration.motherboard !== null && (
          <SummaryItem name="Motherboard" part={configuration.motherboard} />
        )}
        {configuration.memory !== null && (
          <SummaryItem name="Memory" part={configuration.memory} />
        )}
        {configuration.videoCard !== null && (
          <SummaryItem name="Video Card" part={configuration.videoCard} />
        )}
        <span className="build__price">{`Total price: ${priceToString(
          calculatePrice(configuration)
        )}`}</span>
      </div>
      {admin === true && (
        <>
          <LinkButton name="Change Info" url="/admin/add/config" />
          <LinkButton name="Change Parts" url="/config" />
        </>
      )}
      {admin === true && (!isAdd() || isFull()) && (
        <Button
          onClick={() => {
            send();
            navigate('/');
          }}
          disabled={loading}
          variant="contained"
        >
          Commit {isAdd() ? 'Add' : 'Edit'}
        </Button>
      )}
    </div>
  );
}
