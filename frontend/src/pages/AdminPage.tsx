import { Navigate, Outlet } from 'react-router-dom';
import { useRecoilValue } from 'recoil';
import { auth } from '../state/atoms';

export function AdminPage() {
  const authVal = useRecoilValue(auth);
  if (!authVal) {
    return <Navigate to="/login"></Navigate>;
  }
  return (
    <>
      <Outlet />
    </>
  );
}
