import { useRecoilValue } from 'recoil';
import { LinkButton } from '../../components/LinkButton.tsx';
import './Configuration.css';
import { ConfigurationItem } from '../../components/ConfigurationItem.tsx';
import { useConfiguration } from '../../hooks/configurationHook';
import { calculatePrice, priceToString } from '../../services/helperFunctions';

import { auth } from '../../state/atoms';

export function Configuration() {
  const {
    removeMemory,
    removeMotherBoard,
    removeProcesor,
    removeVideoCard,
    configuration,
  } = useConfiguration();
  const admin = useRecoilValue(auth);
  return (
    <div className="config">
      <h1 className="config__title">Choose your parts</h1>
      <ConfigurationItem
        name="Processor"
        part={configuration.processor}
        removePart={removeProcesor}
      />
      <ConfigurationItem
        name="Motherboard"
        part={configuration.motherboard}
        removePart={removeMotherBoard}
      />
      <ConfigurationItem
        name="Memory"
        part={configuration.memory}
        removePart={removeMemory}
      />
      <ConfigurationItem
        name="Video Card"
        part={configuration.videoCard}
        removePart={removeVideoCard}
      />
      <span className="config__price">{`Total price: ${priceToString(
        calculatePrice(configuration)
      )}`}</span>
      <LinkButton name={admin === true ? 'Proceed' : 'Finish'} url="/summary" />
    </div>
  );
}
