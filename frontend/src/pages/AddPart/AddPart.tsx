import * as React from 'react';

import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { AddMotherboard } from '../../components/AddPart/AddMotherboard.tsx';
import './AddPart.css';
import { AddProcessor } from '../../components/AddPart/AddProcessor.tsx';
import { AddMemory } from '../../components/AddPart/AddMemory.tsx';
import { AddVideoCard } from '../../components/AddPart/AddVideoCard.tsx';

export function AddPart() {
  const [type, setType] = React.useState('processor');

  const [form, setForm] = React.useState(<AddProcessor />);

  const handleChange = (event: SelectChangeEvent) => {
    setType(event.target.value as string);
    switch (event.target.value) {
      case 'processor': {
        setForm(<AddProcessor />);
        break;
      }
      case 'motherboard': {
        setForm(<AddMotherboard />);
        break;
      }
      case 'memory': {
        setForm(<AddMemory />);
        break;
      }
      default: {
        setForm(<AddVideoCard />);
        break;
      }
    }
  };

  return (
    <div className="addPart">
      <h1 className="addPart__title">Add Part</h1>
      <FormControl>
        <Select value={type} autoWidth onChange={handleChange}>
          <MenuItem value={'processor'}>Processor</MenuItem>
          <MenuItem value={'motherboard'}>Motherboard</MenuItem>
          <MenuItem value={'memory'}>Memory</MenuItem>
          <MenuItem value={'videoCard'}>Video Card</MenuItem>
        </Select>
      </FormControl>

      {form}
    </div>
  );
}
