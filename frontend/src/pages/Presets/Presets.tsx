import { Preset } from '@pcbuilder/common/api';
import { RotatingLines } from 'react-loader-spinner';
import { useQuery } from 'react-query';
import { PresetView } from '../../components/PresetView.tsx';
import './Presets.css';

import presetApi from '../../services/presetApi';

export function Presets() {
  const { isLoading, data: presets } = useQuery<Preset[]>({
    queryKey: ['presets'],
    queryFn: () => presetApi.readMany(),
  });
  return (
    <div className="presets">
      <h1 className="presets__title">All Presets</h1>
      {presets === undefined || isLoading ? (
        <RotatingLines />
      ) : (
        <div className="presets__list">
          {presets.map((pr) => (
            <PresetView key={pr.id} {...pr}></PresetView>
          ))}
        </div>
      )}
    </div>
  );
}
