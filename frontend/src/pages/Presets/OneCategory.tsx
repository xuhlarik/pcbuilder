import { useParams } from 'react-router-dom';
import { useQuery } from 'react-query';
import { RotatingLines } from 'react-loader-spinner';

import { PresetView } from '../../components/PresetView.tsx';
import './Presets.css';

import categoryApi, { OneCategoryType } from '../../services/categoryApi';

export function OneCategory() {
  const { categoryId } = useParams();
  const { isLoading, data: category } = useQuery<OneCategoryType>({
    queryKey: [`category${categoryId}`],
    queryFn: () => categoryApi.readOne(categoryId ?? '-1'),
  });

  if (categoryId === undefined) {
    return <h1 className="presets__title">Unknown Category</h1>;
  }
  return (
    <div className="presets">
      {category === undefined || isLoading ? (
        <RotatingLines />
      ) : (
        <>
          <h1 className="presets__title">{category.name}</h1>
          <div className="presets__list">
            {category.presets.map((preset) => (
              <PresetView key={preset.id} {...preset} />
            ))}
          </div>
        </>
      )}
    </div>
  );
}
