import { useQuery } from 'react-query';
import { VideoCard } from '@pcbuilder/common/api';
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { PartTable } from '../../components/PartTable/PartTable.tsx';
import { useConfiguration } from '../../hooks/configurationHook';
import {
  readManyVideoCards,
  deleteVideoCards,
} from '../../services/videoCardApi';
import { videoCardFiltersAtom, videoCardsAtom } from '../../state/atoms';
import { filteredVideoCardsSelector } from '../../state/selectors';
import { useDelete } from '../../hooks/deleteHook';
import { ApiError } from '../../types/Error';

export function VideoCardParts() {
  const { addVideoCard } = useConfiguration();
  const setVideoCards = useSetRecoilState(videoCardsAtom);
  const setVideoCardFilters = useSetRecoilState(videoCardFiltersAtom);
  const query = ['videoCards'];
  const deleteHook = useDelete(query, deleteVideoCards);
  const queryHook = useQuery<VideoCard[], ApiError>({
    queryKey: query,
    queryFn: async () => readManyVideoCards({}),
    onSuccess: (data) => {
      setVideoCards(data);
      const [minPrice, maxPrice] = data.reduce<[number, number]>(
        ([min, max], curr) => [
          Math.floor(Math.min(min, curr.price)),
          Math.round(Math.max(max, curr.price)),
        ],
        [Infinity, 0]
      );
      const [minSlots, maxSlots] = data.reduce<[number, number]>(
        ([min, max], curr) => [
          Math.floor(Math.min(min, curr.slotSize)),
          Math.round(Math.max(max, curr.slotSize)),
        ],
        [Infinity, 0]
      );
      setVideoCardFilters((prev) => ({
        ...prev,
        slotSizeRange: [minSlots, maxSlots],
        priceRange: [minPrice, maxPrice],
      }));
    },
  });

  const filteredVideoCards = useRecoilValue(filteredVideoCardsSelector);

  return (
    <PartTable<VideoCard>
      name="Video Card"
      {...queryHook}
      data={filteredVideoCards ?? []}
      addPart={addVideoCard}
      {...deleteHook}
    />
  );
}
