import { useQuery } from 'react-query';
import { Processor } from '@pcbuilder/common/api';
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { PartTable } from '../../components/PartTable/PartTable.tsx';
import { useConfiguration } from '../../hooks/configurationHook';
import {
  readManyProcessors,
  deleteProcessor,
} from '../../services/processorApi';
import { processorFiltersAtom, processorsAtom } from '../../state/atoms';
import { filteredProcessorsSelector } from '../../state/selectors';
import { useDelete } from '../../hooks/deleteHook';
import { ApiError } from '../../types/Error';

export function ProcessorParts() {
  const { addProcesor, configuration } = useConfiguration();
  const setProcessors = useSetRecoilState(processorsAtom);
  const setProcessorFilters = useSetRecoilState(processorFiltersAtom);
  const query = ['processors'];
  const deleteHook = useDelete(query, deleteProcessor);
  const queryHook = useQuery<Processor[], ApiError>({
    queryKey: query,
    queryFn: async () =>
      readManyProcessors({ socketId: configuration.motherboard?.socket.id }),
    onSuccess: (data) => {
      setProcessors(data);
      const [minPrice, maxPrice] = data.reduce<[number, number]>(
        ([min, max], curr) => [
          Math.floor(Math.min(min, curr.price)),
          Math.round(Math.max(max, curr.price)),
        ],
        [Infinity, 0]
      );
      setProcessorFilters((prev) => ({
        ...prev,
        priceRange: [minPrice, maxPrice],
      }));
    },
  });

  const filteredProcessors = useRecoilValue(filteredProcessorsSelector);

  return (
    <PartTable<Processor>
      name="Processor"
      {...queryHook}
      data={filteredProcessors ?? []}
      addPart={addProcesor}
      {...deleteHook}
    />
  );
}
