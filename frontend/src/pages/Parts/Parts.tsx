import { Link } from 'react-router-dom';
import { makePartPath } from '../../services/helperFunctions';
import { SellablePartName } from '../../types/SellableParts';

export function Parts() {
  const parts: SellablePartName[] = [
    'Processor',
    'Motherboard',
    'Memory',
    'Video Card',
  ];
  return (
    <>
      <h1>Choose Part</h1>
      <ul>
        {parts.map((name) => (
          <Link key={name} to={makePartPath(name)}>
            <li>{name}</li>
          </Link>
        ))}
      </ul>
    </>
  );
}
