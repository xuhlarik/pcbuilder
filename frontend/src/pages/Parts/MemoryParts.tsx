import { useQuery } from 'react-query';
import { Memory } from '@pcbuilder/common/api';
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { PartTable } from '../../components/PartTable/PartTable.tsx';

import { useConfiguration } from '../../hooks/configurationHook';
import {
  readManyMemories,
  deleteMemory as deleteMemoryApi,
} from '../../services/memoryApi';
import { memoriesAtom, memoryFiltersAtom } from '../../state/atoms';
import { filteredMemoriesSelector } from '../../state/selectors';
import { useDelete } from '../../hooks/deleteHook';
import { ApiError } from '../../types/Error';

export function MemoryParts() {
  const { addMemory, configuration } = useConfiguration();
  const setMemories = useSetRecoilState(memoriesAtom);
  const setMemoryFilters = useSetRecoilState(memoryFiltersAtom);
  const mem = ['memoriesfil'];
  const deleteHook = useDelete(mem, deleteMemoryApi);
  const queryHook = useQuery<Memory[], ApiError>({
    queryKey: mem,
    queryFn: async () =>
      readManyMemories({
        memoryGenerationId:
          configuration.motherboard?.chipset.memoryGeneration.id,
      }),
    onSuccess: (data) => {
      setMemories(data);
      const [minPrice, maxPrice] = data.reduce<[number, number]>(
        ([min, max], curr) => [
          Math.floor(Math.min(min, curr.price)),
          Math.round(Math.max(max, curr.price)),
        ],
        [Infinity, 0]
      );
      setMemoryFilters((prev) => ({
        ...prev,
        priceRange: [minPrice, maxPrice],
      }));
    },
  });

  const filteredMemories = useRecoilValue(filteredMemoriesSelector);

  return (
    <PartTable<Memory>
      name="Memory"
      {...queryHook}
      data={filteredMemories ?? []}
      addPart={addMemory}
      {...deleteHook}
    />
  );
}
