import { useQuery } from 'react-query';
import { Motherboard } from '@pcbuilder/common/api';
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { PartTable } from '../../components/PartTable/PartTable.tsx';
import { useConfiguration } from '../../hooks/configurationHook';
import {
  readManyMotherboards,
  deleteMotherboard,
} from '../../services/motherboardApi';
import { motherboardFiltersAtom, motherboardsAtom } from '../../state/atoms';
import { filteredMotherboardsSelector } from '../../state/selectors';

import { useDelete } from '../../hooks/deleteHook';
import { ApiError } from '../../types/Error';

export function MotherBoardParts() {
  const { addMotherboard, configuration } = useConfiguration();
  const setMotherboards = useSetRecoilState(motherboardsAtom);
  const setMotherboardFilters = useSetRecoilState(motherboardFiltersAtom);
  const query = ['motherboards'];
  const deleteHook = useDelete(query, deleteMotherboard);

  const queryHook = useQuery<Motherboard[], ApiError>({
    queryKey: query,
    queryFn: async () =>
      readManyMotherboards({
        socketId: configuration.processor?.socket.id,
        memoryGenerationId: configuration.memory?.memoryGeneration.id,
      }),
    onSuccess: (data) => {
      setMotherboards(data);
      const [minPrice, maxPrice] = data.reduce<[number, number]>(
        ([min, max], curr) => [
          Math.floor(Math.min(min, curr.price)),
          Math.round(Math.max(max, curr.price)),
        ],
        [Infinity, 0]
      );
      const [minSlots, maxSlots] = data.reduce<[number, number]>(
        ([min, max], curr) => [
          Math.floor(Math.min(min, curr.memorySlotCount)),
          Math.round(Math.max(max, curr.memorySlotCount)),
        ],
        [Infinity, 0]
      );
      setMotherboardFilters((prev) => ({
        ...prev,
        memorySlotsRange: [minSlots, maxSlots],
        priceRange: [minPrice, maxPrice],
      }));
    },
  });

  const filteredMotherboards = useRecoilValue(filteredMotherboardsSelector);

  return (
    <PartTable<Motherboard>
      name="Motherboard"
      {...queryHook}
      data={filteredMotherboards ?? []}
      addPart={addMotherboard}
      {...deleteHook}
    />
  );
}
