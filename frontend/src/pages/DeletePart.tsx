import * as React from 'react';

import {
  FormControl,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import {
  Memory,
  Motherboard,
  Processor,
  VideoCard,
} from '@pcbuilder/common/api/index';
// import './addPart/AddPart.css';
import { DeleteTable } from '../components/Deletetable.tsx';
import { deleteProcessor, readManyProcessors } from '../services/processorApi';
import {
  deleteMotherboard,
  readManyMotherboards,
} from '../services/motherboardApi';
import { deleteMemory, readManyMemories } from '../services/memoryApi';
import { deleteVideoCards, readManyVideoCards } from '../services/videoCardApi';

export function DeletePart() {
  const [type, setType] = React.useState('processor');
  const processorTable = (
    <DeleteTable<Processor>
      name="Processor"
      getFn={readManyProcessors}
      deleteFn={deleteProcessor}
    />
  );
  const [table, setTable] = React.useState(processorTable);

  const handleChange = (event: SelectChangeEvent) => {
    setType(event.target.value as string);
    switch (event.target.value) {
      case 'processor': {
        setTable(processorTable);
        break;
      }
      case 'motherboard': {
        setTable(
          <DeleteTable<Motherboard>
            name="Motherboard"
            getFn={readManyMotherboards}
            deleteFn={deleteMotherboard}
          />
        );
        break;
      }
      case 'memory': {
        setTable(
          <DeleteTable<Memory>
            name="Memory"
            getFn={readManyMemories}
            deleteFn={deleteMemory}
          />
        );
        break;
      }
      default: {
        setTable(
          <DeleteTable<VideoCard>
            name="Video Card"
            getFn={readManyVideoCards}
            deleteFn={deleteVideoCards}
          />
        );
        break;
      }
    }
  };

  return (
    <div className="addPart">
      <h1 className="addPart__title">Delete Part</h1>
      <FormControl>
        <Select value={type} autoWidth onChange={handleChange}>
          <MenuItem value={'processor'}>Processor</MenuItem>
          <MenuItem value={'motherboard'}>Motherboard</MenuItem>
          <MenuItem value={'memory'}>Memory</MenuItem>
          <MenuItem value={'videoCard'}>Video Card</MenuItem>
        </Select>
      </FormControl>

      {table}
    </div>
  );
}
