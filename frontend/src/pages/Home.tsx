import { useRecoilValue } from 'recoil';
import { CategoryList } from '../components/CategoryList.tsx';
import { AdminComponent } from '../components/AdminComponent/AdminComonent.tsx';
import { auth } from '../state/atoms';
import { LinkButton } from '../components/LinkButton.tsx';

export function Home() {
  const admin = useRecoilValue(auth);
  const adminPriviliges = [
    { image: 'parts.png', name: 'Part' },
    { image: 'configuration.jpg', name: 'Configuration' },
  ];

  return (
    <>
      <CategoryList view={'view'} />
      <h1 className="main__title">Or</h1>
      <div className="main__scratch">
        <h2 className="scratch__title">Start from scratch</h2>
        <img
          src="../assets/scratch.png"
          alt="scratch"
          className="scratch__img"
        />
        <LinkButton name="Build" url="/config" />
      </div>

      {admin && (
        <>
          <h1 className="main__title">Manage</h1>
          <div className="main__add">
            {adminPriviliges.map((addFunction) => (
              <AdminComponent key={addFunction.name} {...addFunction} />
            ))}
          </div>
        </>
      )}
    </>
  );
}
