import { Button, OutlinedInput } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import { Preset } from '@pcbuilder/common/api';
import { useForm } from 'react-hook-form';
import { useConfiguration } from '../../hooks/configurationHook';
import { CategoryList } from '../../components/CategoryList.tsx';
import './AddConfig.css';

export function AddConfig() {
  const { addNameDesc, configuration } = useConfiguration();
  const { handleSubmit, register } = useForm<Preset>({
    defaultValues: {
      name: configuration.name,
      description: configuration.description,
    },
  });

  const navigate = useNavigate();

  function addConfig(preset: Preset) {
    addNameDesc(preset);
    navigate('/summary');
  }

  return (
    <div className="addConfig">
      <h1 className="addConfig__title">Add Configuration</h1>
      <CategoryList view={'add'} />
      <form onSubmit={handleSubmit(addConfig)} className="addConfig__form">
        <label className="form__name">Name:</label>
        <OutlinedInput
          required
          type="text"
          {...register('name')}
          placeholder=""
          className="form__name--input"
        ></OutlinedInput>
        <label className="form__description">Description:</label>
        <OutlinedInput
          required
          type="text"
          {...register('description')}
          className="form__description--input"
        ></OutlinedInput>
        <Button type="submit" className="form__button">
          Proceed
        </Button>
      </form>
    </div>
  );
}
