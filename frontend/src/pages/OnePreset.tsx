import { useQuery } from 'react-query';
import { useNavigate, useParams } from 'react-router-dom';

import { Build } from '@pcbuilder/common/api';
import { RotatingLines } from 'react-loader-spinner';
import { Button } from '@mui/material';
import { ErrorView } from '../components/ErrorView.tsx';
import buildApi from '../services/buildApi';
import { useConfiguration } from '../hooks/configurationHook';
import { ApiError } from '../types/Error';

export function OnePreset() {
  const { presetId } = useParams();
  const navigate = useNavigate();
  const { addBuild } = useConfiguration();
  const { isLoading, isError, error } = useQuery<Build, ApiError>({
    queryFn: () => buildApi.readOne(presetId ?? '-1'),
    queryKey: ['build'],
    onSuccess: (data: Build) => {
      addBuild(data);
      navigate('/config');
    },
  });

  if (presetId === undefined || presetId === 'undefined') {
    return <h2>Unknown preset</h2>;
  }

  return (
    <div>
      {isLoading === true && <RotatingLines />}
      {isError === true && (
        <>
          <ErrorView dataType="Build" error={error} />
          <Button variant="contained" onClick={() => navigate('/presets')}>
            All Presets
          </Button>
        </>
      )}
    </div>
  );
}
