const base = require('@pcbuilder/common/.eslintrc.cjs');

module.exports = {
  ...base,
  env: {
    ...(base.env ?? {}),
    browser: true,
    es2020: true,
  },
  extends: [
    ...(base.extends ?? []),
    'plugin:@typescript-eslint/recommended',
    'plugin:react-hooks/recommended',
  ],
  parserOptions: {
    ...(base.parserOptions ?? {}),
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: [...(base.plugins ?? []), 'react-refresh'],
  rules: {
    ...(base.rules ?? {}),
    'react-refresh/only-export-components': 'warn',
  },
};
