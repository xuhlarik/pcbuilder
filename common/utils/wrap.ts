/**
 * Wraps function `fn`, passing it's return value through `wrapper`.
 * ```
 * (...args) => wrapper(fn(...args))
 * ```
 */
export const wrap = <Args extends Array<any>, RetOld, RetNew>(
  fn: (...args: Args) => RetOld,
  wrapper: (arg: RetOld) => RetNew
  // eslint-disable-next-line arrow-body-style
) => {
  return (...args: Args) => {
    const ret = fn(...args);
    return wrapper(ret);
  };
};

/**
 * Wraps async function `fn`, passing it's return value through `wrapper`.
 * ```
 * async (...args) => wrapper(await fn(...args))
 * ```
 */
export const wrapAsync = <Args extends Array<any>, RetOld, RetNew>(
  fn: (...args: Args) => Promise<RetOld>,
  wrapper: (arg: RetOld) => RetNew
  // eslint-disable-next-line arrow-body-style
) => {
  return async (...args: Args) => {
    const ret = await fn(...args);
    return wrapper(ret);
  };
};
