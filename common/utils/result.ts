import { Result } from '@badrap/result';

/**
 * Wraps function `fn` to return `Result` instead of throwing errors.
 */
export const withResult = <Args extends Array<any>, Ret>(
  fn: (...args: Args) => Ret
  // eslint-disable-next-line arrow-body-style
) => {
  return (...args: Args) => {
    try {
      const ret = fn(...args);
      return Result.ok(ret);
    } catch (err) {
      return Result.err(err as Error);
    }
  };
};

/**
 * Wraps async function `fn` to return `Result` instead of throwing errors.
 */
export const withResultAsync = <Args extends Array<any>, Ret>(
  fn: (...args: Args) => Promise<Ret>
  // eslint-disable-next-line arrow-body-style
) => {
  return async (...args: Args) => {
    try {
      const ret = await fn(...args);
      return Result.ok(ret);
    } catch (err: any) {
      return Result.err(err);
    }
  };
};
