/**
 * Removes fields **not** specified by `keys` from object `obj`.
 */
export const pick = <T extends object, K extends keyof T>(
  obj: T,
  ...keys: K[]
): Pick<T, K> =>
  Object.assign({}, ...keys.map((prop) => ({ [prop]: obj[prop] })));

export default pick;
