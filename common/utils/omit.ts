// https://stackoverflow.com/a/67434028

/**
 * Removes fields specified by `keys` from object `obj`.
 */
export const omit = <T extends object, K extends keyof T>(
  obj: T,
  ...keys: K[]
): Omit<T, K> => {
  const ret = { ...obj };
  keys.forEach((key) => delete ret[key]);
  return ret;
};

export default omit;
