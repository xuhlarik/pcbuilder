const baseStyle = require('eslint-config-airbnb-base/rules/style');

module.exports = {
  extends: ['airbnb-base', 'airbnb-typescript/base', 'prettier'],
  parserOptions: {
    project: './tsconfig.json',
  },
  rules: {
    'import/prefer-default-export': 'off',
    'max-classes-per-file': 'off',
    'no-console': 'off',
    'no-restricted-syntax': [
      'error',
      ...baseStyle.rules['no-restricted-syntax']
        .slice(1) // skip "error"
        .filter(
          ({ selector }) =>
            !['ForInStatement', 'ForOfStatement'].includes(selector)
        ),
    ],
  },
};
