export enum AccessRole {
  ADMIN = 'ADMIN',
}

/**
 * Account information (excluding secrets).
 */
export class Account {
  login: string;

  // Should be Set<AccessRole>, but that that requires custom (de)serialization.
  roles: AccessRole[];

  /**
   * Construct new `Account` by picking **only required** fields from `obj`.
   */
  constructor(obj: Account) {
    this.login = obj.login;
    this.roles = obj.roles;
  }
}
