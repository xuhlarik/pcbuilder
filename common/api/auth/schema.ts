import { z } from 'zod';

const credentials = z.object({
  login: z.string().trim().nonempty(),
  password: z.string().nonempty(),
});

export default {
  register: credentials,
  login: credentials,
  logout: z.object({}),
  whoami: z.object({}),
};
