import { z } from 'zod';
import { sort } from '../common';

const data = z.object({
  name: z.string().trim().nonempty(),
  description: z.string().trim().nonempty(),
  buildId: z.string().nonempty(),
  categoryId: z.string().nonempty(),
});

export default {
  create: data,
  delete: z.object({}),
  readOne: z.object({}),
  readMany: z
    .object({
      // Name contains given substring.
      name: z.string().trim(),
      // Matches one of given IDs (comma separated).
      categoryId: z.string().transform((ids) => ids.split(',')),
      buildId: z.string().transform((ids) => ids.split(',')),
      sort,
    })
    .partial(),
  update: data.partial(),
};
