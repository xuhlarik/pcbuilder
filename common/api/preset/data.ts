export class Preset {
  id: string;

  name: string;

  description: string;

  categoryId: string;

  buildId: string;

  /**
   * Construct new `Preset` by picking **only required** fields from `obj`.
   */
  constructor(obj: Preset) {
    this.id = obj.id;
    this.name = obj.name;
    this.description = obj.description;
    this.categoryId = obj.categoryId;
    this.buildId = obj.buildId;
  }
}
