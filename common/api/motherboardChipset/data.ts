import { Manufacturer } from '../manufacturer/data';
import { MemoryGeneration } from '../memoryGeneration/data';

export class MotherboardChipset {
  id: string;

  name: string;

  manufacturer: Manufacturer;

  memoryGeneration: MemoryGeneration;

  /**
   * Construct new `MotherboardChipset` by picking **only required** fields from `obj`.
   */
  constructor(obj: MotherboardChipset) {
    this.id = obj.id;
    this.name = obj.name;
    this.manufacturer = new Manufacturer(obj.manufacturer);
    this.memoryGeneration = new MemoryGeneration(obj.memoryGeneration);
  }
}
