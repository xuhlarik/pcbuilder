import { Manufacturer } from '../manufacturer/data';

export class VideoChipset {
  id: string;

  name: string;

  manufacturer: Manufacturer;

  /**
   * Construct new `VideoChipset` by picking **only required** fields from `obj`.
   */
  constructor(obj: VideoChipset) {
    this.id = obj.id;
    this.name = obj.name;
    this.manufacturer = new Manufacturer(obj.manufacturer);
  }
}
