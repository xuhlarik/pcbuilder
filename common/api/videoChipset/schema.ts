import { z } from 'zod';
import { sort } from '../common';

const data = z.object({
  name: z.string().trim().nonempty(),
  manufacturerId: z.string().nonempty(),
});

export default {
  create: data,
  delete: z.object({}),
  readOne: z.object({}),
  readMany: z
    .object({
      // Name contains given substring.
      name: z.string().trim(),
      // Manufacturer matches one of given IDs (comma separated).
      manufacturerId: z.string().transform((ids) => ids.split(',')),
      sort,
    })
    .partial(),
  update: data.partial(),
};
