import { Preset } from '../preset/data';

export class Category {
  id: string;

  name: string;

  description: string;

  /**
   * Construct new `Category` by picking **only required** fields from `obj`.
   */
  constructor(obj: Category) {
    this.id = obj.id;
    this.name = obj.name;
    this.description = obj.description;
  }
}

export class CategoryWithPresets extends Category {
  presets: Preset[];

  /**
   * Construct new `CategoryWithPresets` by picking **only required** fields from `obj`.
   */
  constructor(obj: CategoryWithPresets) {
    super(obj);
    this.presets = obj.presets.map((data) => new Preset(data));
  }
}
