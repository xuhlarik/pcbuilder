import { IntegratedGraphics } from '../integratedGraphics/data';
import { Manufacturer } from '../manufacturer/data';
import { Socket } from '../socket/data';

export class Processor {
  id: string;

  name: string;

  manufacturer: Manufacturer;

  socket: Socket;

  price: number;

  integratedGraphics: IntegratedGraphics | null;

  image: string;

  /**
   * Construct new `Processor` by picking **only required** fields from `obj`.
   */
  constructor(obj: Processor) {
    this.id = obj.id;
    this.name = obj.name;
    this.manufacturer = new Manufacturer(obj.manufacturer);
    this.socket = new Socket(obj.socket);
    this.price = obj.price;
    this.integratedGraphics =
      obj.integratedGraphics === null
        ? null
        : new IntegratedGraphics(obj.integratedGraphics);
    this.image = obj.image;
  }
}
