import { z } from 'zod';
import { sort } from '../common';

const data = z.object({
  name: z.string().trim().nonempty(),
  price: z.coerce.number(),
  manufacturerId: z.string().nonempty(),
  socketId: z.string().nonempty(),
  integratedGraphicsId: z.string().nonempty().nullable(),
  image: z.string().nonempty(),
});

export default {
  create: data,
  delete: z.object({}),
  readOne: z.object({}),
  readMany: z
    .object({
      // Name contains given substring.
      name: z.string().trim(),
      // Matches one of given IDs (comma separated).
      manufacturerId: z.string().transform((ids) => ids.split(',')),
      socketId: z.string().transform((ids) => ids.split(',')),
      // Must (not) have integrated graphics.
      integratedGraphics: z.coerce.boolean(),
      sort,
    })
    .partial(),
  update: data.partial(),
};
