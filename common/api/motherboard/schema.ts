import { z } from 'zod';
import { sort } from '../common';

const data = z.object({
  name: z.string().trim().nonempty(),
  price: z.coerce.number(),
  manufacturerId: z.string().nonempty(),
  socketId: z.string().nonempty(),
  memorySlotCount: z.coerce.number(),
  formFactorId: z.string().nonempty(),
  chipsetId: z.string().nonempty(),
  pcieSlots: z
    .object({
      pcieGenerationId: z.string().nonempty(),
      size: z.coerce.number(),
      count: z.coerce.number(),
    })
    .array(),
  image: z.string().nonempty(),
});

export default {
  create: data,
  delete: z.object({}),
  readOne: z.object({}),
  readMany: z
    .object({
      // Name contains given substring.
      name: z.string().trim(),
      // Matches one of given IDs (comma separated).
      manufacturerId: z.string().transform((ids) => ids.split(',')),
      memoryGenerationId: z.string().transform((ids) => ids.split(',')),
      socketId: z.string().transform((ids) => ids.split(',')),
      chipsetId: z.string().transform((ids) => ids.split(',')),
      formFactorId: z.string().transform((ids) => ids.split(',')),
      sort,
    })
    .partial(),
  update: data.partial(),
};
