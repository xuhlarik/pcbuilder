import { FormFactor } from '../formFactor/data';
import { Manufacturer } from '../manufacturer/data';
import { MotherboardChipset } from '../motherboardChipset/data';
import { MotherboardPcieSlot } from '../motherboardPcieSlot/data';
import { Socket } from '../socket/data';

export class Motherboard {
  id: string;

  name: string;

  manufacturer: Manufacturer;

  socket: Socket;

  price: number;

  memorySlotCount: number;

  formFactor: FormFactor;

  chipset: MotherboardChipset;

  pcieSlots: MotherboardPcieSlot[];

  image: string;

  /**
   * Construct new `Build` by picking **only required** fields from `obj`.
   */
  constructor(obj: Motherboard) {
    this.id = obj.id;
    this.name = obj.name;
    this.manufacturer = new Manufacturer(obj.manufacturer);
    this.socket = new Socket(obj.socket);
    this.price = obj.price;
    this.memorySlotCount = obj.memorySlotCount;
    this.formFactor = new FormFactor(obj.formFactor);
    this.chipset = new MotherboardChipset(obj.chipset);
    this.pcieSlots = obj.pcieSlots.map((e) => new MotherboardPcieSlot(e));
    this.image = obj.image;
  }
}
