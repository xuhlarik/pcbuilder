import { z } from 'zod';

export const sort = z.enum(['asc', 'desc']);
