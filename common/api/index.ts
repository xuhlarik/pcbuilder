import auth from './auth/schema';
import build from './build/schema';
import category from './category/schema';
import formFactor from './formFactor/schema';
import integratedGraphics from './integratedGraphics/schema';
import manufacturer from './manufacturer/schema';
import memory from './memory/schema';
import memoryGeneration from './memoryGeneration/schema';
import motherboardChipset from './motherboardChipset/schema';
import motherboard from './motherboard/schema';
import processor from './processor/schema';
import pcieGeneration from './pcieGeneration/schema';
import preset from './preset/schema';
import socket from './socket/schema';
import videoCard from './videoCard/schema';
import videoChipset from './videoChipset/schema';

export { Account, AccessRole } from './auth/data';
export { Build } from './build/data';
export { Category, CategoryWithPresets } from './category/data';
export { FormFactor } from './formFactor/data';
export { IntegratedGraphics } from './integratedGraphics/data';
export { Manufacturer } from './manufacturer/data';
export { Memory } from './memory/data';
export { MemoryGeneration } from './memoryGeneration/data';
export { Motherboard } from './motherboard/data';
export { MotherboardChipset } from './motherboardChipset/data';
export { MotherboardPcieSlot } from './motherboardPcieSlot/data';
export { PcieGeneration } from './pcieGeneration/data';
export { Preset } from './preset/data';
export { Processor } from './processor/data';
export { Socket } from './socket/data';
export { VideoCard } from './videoCard/data';
export { VideoChipset } from './videoChipset/data';

export const schema = {
  auth,
  build,
  category,
  formFactor,
  integratedGraphics,
  manufacturer,
  memory,
  memoryGeneration,
  motherboard,
  motherboardChipset,
  processor,
  pcieGeneration,
  preset,
  socket,
  videoCard,
  videoChipset,
};

export default schema;
