import { Manufacturer } from '../manufacturer/data';
import { MemoryGeneration } from '../memoryGeneration/data';

export class Memory {
  id: string;

  name: string;

  manufacturer: Manufacturer;

  memoryGeneration: MemoryGeneration;

  price: number;

  image: string;

  /**
   * Construct new `Memory` by picking **only required** fields from `obj`.
   */
  constructor(obj: Memory) {
    this.id = obj.id;
    this.name = obj.name;
    this.manufacturer = new Manufacturer(obj.manufacturer);
    this.memoryGeneration = new MemoryGeneration(obj.memoryGeneration);
    this.price = obj.price;
    this.image = obj.image;
  }
}
