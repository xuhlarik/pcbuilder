import { z } from 'zod';
import { sort } from '../common';

export default {
  create: z.object({
    name: z.string().trim().nonempty(),
  }),
  delete: z.object({}),
  readOne: z.object({}),
  readMany: z.object({
    name: z.string().trim().optional(),
    sort: sort.optional(),
  }),
  update: z.object({
    name: z.string().trim().optional(),
  }),
};
