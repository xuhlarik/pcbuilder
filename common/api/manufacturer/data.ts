export class Manufacturer {
  id: string;

  name: string;

  /**
   * Construct new `Manufacturer` by picking **only required** fields from `obj`.
   */
  constructor(obj: Manufacturer) {
    this.id = obj.id;
    this.name = obj.name;
  }
}
