export class PcieGeneration {
  id: string;

  name: string;

  /**
   * Construct new `PcieGeneration` by picking **only required** fields from `obj`.
   */
  constructor(obj: PcieGeneration) {
    this.id = obj.id;
    this.name = obj.name;
  }
}
