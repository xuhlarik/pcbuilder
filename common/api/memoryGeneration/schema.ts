import { z } from 'zod';
import { sort } from '../common';

const data = z.object({
  name: z.string().trim().nonempty(),
});

export default {
  create: data,
  delete: z.object({}),
  readOne: z.object({}),
  readMany: z.object({
    name: z.string().trim().optional(),
    sort: sort.optional(),
  }),
  update: data.partial(),
};
