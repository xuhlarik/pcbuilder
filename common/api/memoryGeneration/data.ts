export class MemoryGeneration {
  id: string;

  name: string;

  /**
   * Construct new `MemoryGeneration` by picking **only required** fields from `obj`.
   */
  constructor(obj: MemoryGeneration) {
    this.id = obj.id;
    this.name = obj.name;
  }
}
