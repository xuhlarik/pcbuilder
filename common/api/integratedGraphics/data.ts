export class IntegratedGraphics {
  id: string;

  name: string;

  /**
   * Construct new `IntegratedGraphics` by picking **only required** fields from `obj`.
   */
  constructor(obj: IntegratedGraphics) {
    this.id = obj.id;
    this.name = obj.name;
  }
}
