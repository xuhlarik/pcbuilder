export class FormFactor {
  id: string;

  name: string;

  /**
   * Construct new `FormFactor` by picking **only required** fields from `obj`.
   */
  constructor(obj: FormFactor) {
    this.id = obj.id;
    this.name = obj.name;
  }
}
