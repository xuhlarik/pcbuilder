import { Memory } from '../memory/data';
import { Motherboard } from '../motherboard/data';
import { Processor } from '../processor/data';
import { VideoCard } from '../videoCard/data';

export class Build {
  id: string;

  processor: Processor | null;

  motherboard: Motherboard | null;

  memory: Memory | null;

  videoCard: VideoCard | null;

  /**
   * Construct new `Build` by picking **only required** fields from `obj`.
   */
  constructor(obj: Build) {
    this.id = obj.id;
    this.memory = obj.memory ? new Memory(obj.memory) : null;
    this.motherboard = obj.motherboard
      ? new Motherboard(obj.motherboard)
      : null;
    this.videoCard = obj.videoCard ? new VideoCard(obj.videoCard) : null;
    this.processor = obj.processor ? new Processor(obj.processor) : null;
  }
}
