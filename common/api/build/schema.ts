import { z } from 'zod';

const data = z.object({
  processorId: z.string().nonempty().nullable(),
  motherboardId: z.string().nonempty().nullable(),
  memoryId: z.string().nonempty().nullable(),
  videoCardId: z.string().nonempty().nullable(),
});

export default {
  create: data,
  delete: z.object({}),
  readOne: z.object({}),
  readMany: z.object({}).partial(),
  update: data.partial(),
};
