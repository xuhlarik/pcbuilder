import { Manufacturer } from '../manufacturer/data';
import { PcieGeneration } from '../pcieGeneration/data';
import { VideoChipset } from '../videoChipset/data';

export class VideoCard {
  id: string;

  name: string;

  manufacturer: Manufacturer;

  price: number;

  pcieGeneration: PcieGeneration;

  chipset: VideoChipset;

  slotSize: number;

  image: string;

  /**
   * Construct new `VideoCard` by picking **only required** fields from `obj`.
   */
  constructor(obj: VideoCard) {
    this.id = obj.id;
    this.name = obj.name;
    this.manufacturer = new Manufacturer(obj.manufacturer);
    this.price = obj.price;
    this.pcieGeneration = new PcieGeneration(obj.pcieGeneration);
    this.chipset = new VideoChipset(obj.chipset);
    this.slotSize = obj.slotSize;
    this.image = obj.image;
  }
}
