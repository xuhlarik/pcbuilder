import { PcieGeneration } from '../pcieGeneration/data';

export class MotherboardPcieSlot {
  id: string;

  pcieGeneration: PcieGeneration;

  size: number;

  count: number;

  /**
   * Construct new `MotherboardPcieSlot` by picking **only required** fields from `obj`.
   */
  constructor(obj: MotherboardPcieSlot) {
    this.id = obj.id;
    this.pcieGeneration = new PcieGeneration(obj.pcieGeneration);
    this.size = obj.size;
    this.count = obj.count;
  }
}
