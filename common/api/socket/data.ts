export class Socket {
  id: string;

  name: string;

  /**
   * Construct new `Socket` by picking **only required** fields from `obj`.
   */
  constructor(obj: Socket) {
    this.id = obj.id;
    this.name = obj.name;
  }
}
