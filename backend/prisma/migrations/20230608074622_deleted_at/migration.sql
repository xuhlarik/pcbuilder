-- AlterTable
ALTER TABLE "Build" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "Category" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "FormFactor" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "IntegratedGraphics" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "Manufacturer" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "Memory" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "MemoryGeneration" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "Motherboard" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "MotherboardChipset" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "MotherboardPcieSlot" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "PcieGeneration" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "Preset" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "Processor" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "Socket" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "VideoCard" ADD COLUMN "deletedAt" DATETIME;

-- AlterTable
ALTER TABLE "VideoChipset" ADD COLUMN "deletedAt" DATETIME;
