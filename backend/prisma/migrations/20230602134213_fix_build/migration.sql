/*
  Warnings:

  - You are about to drop the column `description` on the `Build` table. All the data in the column will be lost.
  - The primary key for the `MotherboardPcieSlot` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The required column `id` was added to the `MotherboardPcieSlot` table with a prisma-level default value. This is not possible if the table is not empty. Please add this column as optional, then populate it before making it required.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Build" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "processorId" TEXT,
    "motherboardId" TEXT,
    "memoryId" TEXT,
    "videoCardId" TEXT,
    CONSTRAINT "Build_processorId_fkey" FOREIGN KEY ("processorId") REFERENCES "Processor" ("id") ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT "Build_motherboardId_fkey" FOREIGN KEY ("motherboardId") REFERENCES "Motherboard" ("id") ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT "Build_memoryId_fkey" FOREIGN KEY ("memoryId") REFERENCES "Memory" ("id") ON DELETE SET NULL ON UPDATE CASCADE,
    CONSTRAINT "Build_videoCardId_fkey" FOREIGN KEY ("videoCardId") REFERENCES "VideoCard" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_Build" ("id", "motherboardId", "processorId", "videoCardId") SELECT "id", "motherboardId", "processorId", "videoCardId" FROM "Build";
DROP TABLE "Build";
ALTER TABLE "new_Build" RENAME TO "Build";
CREATE TABLE "new_MotherboardPcieSlot" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "motherboardId" TEXT NOT NULL,
    "pcieGenerationId" TEXT NOT NULL,
    "size" INTEGER NOT NULL,
    "count" INTEGER NOT NULL,
    CONSTRAINT "MotherboardPcieSlot_motherboardId_fkey" FOREIGN KEY ("motherboardId") REFERENCES "Motherboard" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "MotherboardPcieSlot_pcieGenerationId_fkey" FOREIGN KEY ("pcieGenerationId") REFERENCES "PcieGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_MotherboardPcieSlot" ("count", "motherboardId", "pcieGenerationId", "size") SELECT "count", "motherboardId", "pcieGenerationId", "size" FROM "MotherboardPcieSlot";
DROP TABLE "MotherboardPcieSlot";
ALTER TABLE "new_MotherboardPcieSlot" RENAME TO "MotherboardPcieSlot";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
