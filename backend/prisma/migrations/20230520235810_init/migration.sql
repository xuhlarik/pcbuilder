-- CreateTable
CREATE TABLE "Preset" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "categoryId" TEXT NOT NULL,
    "buildId" TEXT NOT NULL,
    CONSTRAINT "Preset_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Preset_buildId_fkey" FOREIGN KEY ("buildId") REFERENCES "Build" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Category" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "Build" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "cpuId" TEXT NOT NULL,
    "motherboardId" TEXT NOT NULL,
    "videoCardId" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    CONSTRAINT "Build_cpuId_fkey" FOREIGN KEY ("cpuId") REFERENCES "CPU" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Build_motherboardId_fkey" FOREIGN KEY ("motherboardId") REFERENCES "MotherBoard" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Build_videoCardId_fkey" FOREIGN KEY ("videoCardId") REFERENCES "VideoCard" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "CPU" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "socketId" TEXT NOT NULL,
    "price" INTEGER NOT NULL,
    "integratedGraphicsId" TEXT NOT NULL,
    CONSTRAINT "CPU_socketId_fkey" FOREIGN KEY ("socketId") REFERENCES "Socket" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "CPU_integratedGraphicsId_fkey" FOREIGN KEY ("integratedGraphicsId") REFERENCES "IntegratedGraphics" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "IntegratedGraphics" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "price" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "Socket" (
    "id" TEXT NOT NULL PRIMARY KEY
);

-- CreateTable
CREATE TABLE "MotherBoard" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "socketId" TEXT NOT NULL,
    "price" INTEGER NOT NULL,
    "memoryslots" INTEGER NOT NULL,
    "formFactorId" TEXT NOT NULL,
    "motherboardChipSetId" TEXT NOT NULL,
    CONSTRAINT "MotherBoard_socketId_fkey" FOREIGN KEY ("socketId") REFERENCES "Socket" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "MotherBoard_formFactorId_fkey" FOREIGN KEY ("formFactorId") REFERENCES "FormFactor" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "MotherBoard_motherboardChipSetId_fkey" FOREIGN KEY ("motherboardChipSetId") REFERENCES "MotherboardChipSet" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "FormFactor" (
    "id" TEXT NOT NULL PRIMARY KEY
);

-- CreateTable
CREATE TABLE "VideoCard" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "price" INTEGER NOT NULL,
    "pCIeGenerationId" TEXT NOT NULL,
    "videoChipsetId" TEXT NOT NULL,
    "slotSize" INTEGER NOT NULL,
    CONSTRAINT "VideoCard_pCIeGenerationId_fkey" FOREIGN KEY ("pCIeGenerationId") REFERENCES "PCIeGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "VideoCard_videoChipsetId_fkey" FOREIGN KEY ("videoChipsetId") REFERENCES "VideoChipset" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Manufacturer" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "Memory" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "memoryGenerationId" TEXT NOT NULL,
    "price" INTEGER NOT NULL,
    CONSTRAINT "Memory_memoryGenerationId_fkey" FOREIGN KEY ("memoryGenerationId") REFERENCES "MemoryGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "MemoryGeneration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "MotherboardChipSet" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "memoryGenerationId" TEXT NOT NULL,
    CONSTRAINT "MotherboardChipSet_memoryGenerationId_fkey" FOREIGN KEY ("memoryGenerationId") REFERENCES "MemoryGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "PCIeGeneration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "MemoryPCIeSlot" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "size" INTEGER NOT NULL,
    "pCIeGenerationId" TEXT NOT NULL,
    CONSTRAINT "MemoryPCIeSlot_pCIeGenerationId_fkey" FOREIGN KEY ("pCIeGenerationId") REFERENCES "PCIeGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "VideoChipset" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL
);
