/*
  Warnings:

  - You are about to drop the `CPU` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `MemoryPCIeSlot` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `MotherBoard` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `MotherboardChipSet` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `PCIeGeneration` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the column `cpuId` on the `Build` table. All the data in the column will be lost.
  - You are about to drop the column `price` on the `IntegratedGraphics` table. All the data in the column will be lost.
  - You are about to alter the column `price` on the `Memory` table. The data in that column could be lost. The data in that column will be cast from `Int` to `Decimal`.
  - You are about to drop the column `pCIeGenerationId` on the `VideoCard` table. All the data in the column will be lost.
  - You are about to drop the column `videoChipsetId` on the `VideoCard` table. All the data in the column will be lost.
  - You are about to alter the column `price` on the `VideoCard` table. The data in that column could be lost. The data in that column will be cast from `Int` to `Decimal`.
  - Added the required column `name` to the `FormFactor` table without a default value. This is not possible if the table is not empty.
  - Added the required column `processorId` to the `Build` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `Socket` table without a default value. This is not possible if the table is not empty.
  - Added the required column `manufacturerId` to the `Memory` table without a default value. This is not possible if the table is not empty.
  - Added the required column `chipsetId` to the `VideoCard` table without a default value. This is not possible if the table is not empty.
  - Added the required column `manufacturerId` to the `VideoCard` table without a default value. This is not possible if the table is not empty.
  - Added the required column `name` to the `VideoCard` table without a default value. This is not possible if the table is not empty.
  - Added the required column `pcieGenerationId` to the `VideoCard` table without a default value. This is not possible if the table is not empty.
  - Added the required column `manufacturerId` to the `VideoChipset` table without a default value. This is not possible if the table is not empty.

*/
-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "CPU";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "MemoryPCIeSlot";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "MotherBoard";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "MotherboardChipSet";
PRAGMA foreign_keys=on;

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "PCIeGeneration";
PRAGMA foreign_keys=on;

-- CreateTable
CREATE TABLE "Processor" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    "socketId" TEXT NOT NULL,
    "price" DECIMAL NOT NULL,
    "integratedGraphicsId" TEXT,
    CONSTRAINT "Processor_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Processor_socketId_fkey" FOREIGN KEY ("socketId") REFERENCES "Socket" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Processor_integratedGraphicsId_fkey" FOREIGN KEY ("integratedGraphicsId") REFERENCES "IntegratedGraphics" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "Motherboard" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    "socketId" TEXT NOT NULL,
    "price" DECIMAL NOT NULL,
    "memorySlotCount" INTEGER NOT NULL,
    "formFactorId" TEXT NOT NULL,
    "chipsetId" TEXT NOT NULL,
    CONSTRAINT "Motherboard_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Motherboard_socketId_fkey" FOREIGN KEY ("socketId") REFERENCES "Socket" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Motherboard_formFactorId_fkey" FOREIGN KEY ("formFactorId") REFERENCES "FormFactor" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Motherboard_chipsetId_fkey" FOREIGN KEY ("chipsetId") REFERENCES "MotherboardChipset" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "MotherboardChipset" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    "memoryGenerationId" TEXT NOT NULL,
    CONSTRAINT "MotherboardChipset_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "MotherboardChipset_memoryGenerationId_fkey" FOREIGN KEY ("memoryGenerationId") REFERENCES "MemoryGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- CreateTable
CREATE TABLE "PcieGeneration" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "MotherboardPcieSlot" (
    "motherboardId" TEXT NOT NULL,
    "pcieGenerationId" TEXT NOT NULL,
    "size" INTEGER NOT NULL,
    "count" INTEGER NOT NULL,

    PRIMARY KEY ("motherboardId", "pcieGenerationId", "size"),
    CONSTRAINT "MotherboardPcieSlot_motherboardId_fkey" FOREIGN KEY ("motherboardId") REFERENCES "Motherboard" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "MotherboardPcieSlot_pcieGenerationId_fkey" FOREIGN KEY ("pcieGenerationId") REFERENCES "PcieGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_FormFactor" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL
);
INSERT INTO "new_FormFactor" ("id") SELECT "id" FROM "FormFactor";
DROP TABLE "FormFactor";
ALTER TABLE "new_FormFactor" RENAME TO "FormFactor";
CREATE TABLE "new_Build" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "processorId" TEXT NOT NULL,
    "motherboardId" TEXT NOT NULL,
    "videoCardId" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    CONSTRAINT "Build_processorId_fkey" FOREIGN KEY ("processorId") REFERENCES "Processor" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Build_motherboardId_fkey" FOREIGN KEY ("motherboardId") REFERENCES "Motherboard" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Build_videoCardId_fkey" FOREIGN KEY ("videoCardId") REFERENCES "VideoCard" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Build" ("description", "id", "motherboardId", "videoCardId") SELECT "description", "id", "motherboardId", "videoCardId" FROM "Build";
DROP TABLE "Build";
ALTER TABLE "new_Build" RENAME TO "Build";
CREATE TABLE "new_Socket" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL
);
INSERT INTO "new_Socket" ("id") SELECT "id" FROM "Socket";
DROP TABLE "Socket";
ALTER TABLE "new_Socket" RENAME TO "Socket";
CREATE TABLE "new_IntegratedGraphics" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL
);
INSERT INTO "new_IntegratedGraphics" ("id", "name") SELECT "id", "name" FROM "IntegratedGraphics";
DROP TABLE "IntegratedGraphics";
ALTER TABLE "new_IntegratedGraphics" RENAME TO "IntegratedGraphics";
CREATE TABLE "new_Memory" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    "memoryGenerationId" TEXT NOT NULL,
    "price" DECIMAL NOT NULL,
    CONSTRAINT "Memory_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Memory_memoryGenerationId_fkey" FOREIGN KEY ("memoryGenerationId") REFERENCES "MemoryGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Memory" ("id", "memoryGenerationId", "name", "price") SELECT "id", "memoryGenerationId", "name", "price" FROM "Memory";
DROP TABLE "Memory";
ALTER TABLE "new_Memory" RENAME TO "Memory";
CREATE TABLE "new_VideoCard" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    "price" DECIMAL NOT NULL,
    "pcieGenerationId" TEXT NOT NULL,
    "chipsetId" TEXT NOT NULL,
    "slotSize" INTEGER NOT NULL,
    CONSTRAINT "VideoCard_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "VideoCard_pcieGenerationId_fkey" FOREIGN KEY ("pcieGenerationId") REFERENCES "PcieGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "VideoCard_chipsetId_fkey" FOREIGN KEY ("chipsetId") REFERENCES "VideoChipset" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_VideoCard" ("id", "price", "slotSize") SELECT "id", "price", "slotSize" FROM "VideoCard";
DROP TABLE "VideoCard";
ALTER TABLE "new_VideoCard" RENAME TO "VideoCard";
CREATE TABLE "new_VideoChipset" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    CONSTRAINT "VideoChipset_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_VideoChipset" ("id", "name") SELECT "id", "name" FROM "VideoChipset";
DROP TABLE "VideoChipset";
ALTER TABLE "new_VideoChipset" RENAME TO "VideoChipset";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
