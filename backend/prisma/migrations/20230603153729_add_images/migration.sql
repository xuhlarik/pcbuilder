/*
  Warnings:

  - Added the required column `image` to the `Motherboard` table without a default value. This is not possible if the table is not empty.
  - Added the required column `image` to the `Memory` table without a default value. This is not possible if the table is not empty.
  - Added the required column `image` to the `Processor` table without a default value. This is not possible if the table is not empty.
  - Added the required column `image` to the `VideoCard` table without a default value. This is not possible if the table is not empty.

*/
-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Motherboard" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    "socketId" TEXT NOT NULL,
    "price" DECIMAL NOT NULL,
    "memorySlotCount" INTEGER NOT NULL,
    "formFactorId" TEXT NOT NULL,
    "chipsetId" TEXT NOT NULL,
    "image" TEXT NOT NULL,
    CONSTRAINT "Motherboard_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Motherboard_socketId_fkey" FOREIGN KEY ("socketId") REFERENCES "Socket" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Motherboard_formFactorId_fkey" FOREIGN KEY ("formFactorId") REFERENCES "FormFactor" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Motherboard_chipsetId_fkey" FOREIGN KEY ("chipsetId") REFERENCES "MotherboardChipset" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Motherboard" ("chipsetId", "formFactorId", "id", "manufacturerId", "memorySlotCount", "name", "price", "socketId") SELECT "chipsetId", "formFactorId", "id", "manufacturerId", "memorySlotCount", "name", "price", "socketId" FROM "Motherboard";
DROP TABLE "Motherboard";
ALTER TABLE "new_Motherboard" RENAME TO "Motherboard";
CREATE TABLE "new_Memory" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    "memoryGenerationId" TEXT NOT NULL,
    "price" DECIMAL NOT NULL,
    "image" TEXT NOT NULL,
    CONSTRAINT "Memory_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Memory_memoryGenerationId_fkey" FOREIGN KEY ("memoryGenerationId") REFERENCES "MemoryGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Memory" ("id", "manufacturerId", "memoryGenerationId", "name", "price") SELECT "id", "manufacturerId", "memoryGenerationId", "name", "price" FROM "Memory";
DROP TABLE "Memory";
ALTER TABLE "new_Memory" RENAME TO "Memory";
CREATE TABLE "new_Processor" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    "socketId" TEXT NOT NULL,
    "price" DECIMAL NOT NULL,
    "integratedGraphicsId" TEXT,
    "image" TEXT NOT NULL,
    CONSTRAINT "Processor_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Processor_socketId_fkey" FOREIGN KEY ("socketId") REFERENCES "Socket" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Processor_integratedGraphicsId_fkey" FOREIGN KEY ("integratedGraphicsId") REFERENCES "IntegratedGraphics" ("id") ON DELETE SET NULL ON UPDATE CASCADE
);
INSERT INTO "new_Processor" ("id", "integratedGraphicsId", "manufacturerId", "name", "price", "socketId") SELECT "id", "integratedGraphicsId", "manufacturerId", "name", "price", "socketId" FROM "Processor";
DROP TABLE "Processor";
ALTER TABLE "new_Processor" RENAME TO "Processor";
CREATE TABLE "new_VideoCard" (
    "id" TEXT NOT NULL PRIMARY KEY,
    "name" TEXT NOT NULL,
    "manufacturerId" TEXT NOT NULL,
    "price" DECIMAL NOT NULL,
    "pcieGenerationId" TEXT NOT NULL,
    "chipsetId" TEXT NOT NULL,
    "slotSize" INTEGER NOT NULL,
    "image" TEXT NOT NULL,
    CONSTRAINT "VideoCard_manufacturerId_fkey" FOREIGN KEY ("manufacturerId") REFERENCES "Manufacturer" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "VideoCard_pcieGenerationId_fkey" FOREIGN KEY ("pcieGenerationId") REFERENCES "PcieGeneration" ("id") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "VideoCard_chipsetId_fkey" FOREIGN KEY ("chipsetId") REFERENCES "VideoChipset" ("id") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_VideoCard" ("chipsetId", "id", "manufacturerId", "name", "pcieGenerationId", "price", "slotSize") SELECT "chipsetId", "id", "manufacturerId", "name", "pcieGenerationId", "price", "slotSize" FROM "VideoCard";
DROP TABLE "VideoCard";
ALTER TABLE "new_VideoCard" RENAME TO "VideoCard";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
