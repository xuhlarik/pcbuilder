import express from 'express';
import cors from 'cors';
import env from './env';
import session from './middleware/session';
import router from './routes';
import errorHandler from './middleware/errors';

const app = express();

app.use(express.json());
app.use(session);
app.use(
  cors({
    origin: `http://${env('FRONTEND_HOST', 'localhost')}:${env(
      'FRONTEND_PORT',
      '5173'
    )}`,
    credentials: true,
  })
);
app.use(router);

// Must be last!
app.use(errorHandler);

const host = env('BACKEND_HOST', 'localhost');
const port = Number.parseInt(env('BACKEND_PORT', '3000'), 10);

app.listen(port, host, () => {
  console.log(
    `[${new Date().toISOString()}] RESTful API listening on http://${host}:${port}`
  );
});
