import { Router } from 'express';
import controller from '../controllers/auth';

const router = Router();

router
  .route('/')
  .get(controller.whoami)
  .post(controller.login)
  .delete(controller.logout);

export default router;
