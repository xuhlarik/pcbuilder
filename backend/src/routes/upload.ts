import { AccessRole } from '@pcbuilder/common/api';
import { pick } from '@pcbuilder/common/utils';
import express, { Router } from 'express';
import multer from 'multer';
import path from 'path';
import * as uuid from 'uuid';
import { BadRequest } from '../errors';
import env from '../env';
import { handler } from '../controllers/handler';

const directories = env('UPLOAD_DIR').split(':');

const router = Router();

const upload = multer({
  storage: multer.diskStorage({
    destination: directories[0], // first one is write directory
    filename: (_req, file, cb) => {
      const filename = `${uuid.v4()}${path.extname(file.originalname)}`;
      cb(null, filename);
    },
  }),
});

router.post(
  '/',
  upload.array('file'),
  handler({
    allow: [AccessRole.ADMIN],
    fn: async (req) => {
      if (!Array.isArray(req.files)) {
        throw new BadRequest();
      }

      return req.files.map((file) => pick(file, 'filename', 'originalname'));
    },
  })
);

console.log(`Uploads will be saved to directory: ${directories[0]}`);

for (const directory of directories) {
  console.log(`Serving static uploads directory: ${directory}`);
  router.use(
    express.static(directory, {
      index: false,
    })
  );
}

export default router;
