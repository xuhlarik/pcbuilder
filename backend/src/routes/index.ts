import { Router } from 'express';
import auth from './auth';
import upload from './upload';
import controllers from '../controllers';
import type { Controller } from '../controllers/types';

const router = Router();

router.use('/auth', auth);
router.use('/upload', upload);

const routes: { [key: string]: Controller } = {
  '/build': controllers.build,
  '/category': controllers.category,
  '/form-factor': controllers.formFactor,
  '/integrated-graphics': controllers.integratedGraphics,
  '/manufacturer': controllers.manufacturer,
  '/memory': controllers.memory,
  '/memory-generation': controllers.memoryGeneration,
  '/motherboard': controllers.motherboard,
  '/motherboard-chipset': controllers.motherboardChipset,
  '/pcie-generation': controllers.pcieGeneration,
  '/preset': controllers.preset,
  '/processor': controllers.processor,
  '/socket': controllers.socket,
  '/video-card': controllers.videoCard,
  '/video-chipset': controllers.videoChipset,
};

for (const [path, controller] of Object.entries(routes)) {
  const route = Router();

  route.route('/').post(controller.create).get(controller.readMany);
  route
    .route('/:id')
    .get(controller.readOne)
    .delete(controller.delete)
    .patch(controller.update);

  router.use(path, route);
}

export default router;
