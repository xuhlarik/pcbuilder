import { withResultAsync, wrapAsync } from '@pcbuilder/common/utils';
import { Prisma } from '@prisma/client';

class SerializableError extends Error {
  // JSON.stringify should serialize properties.
  toJSON() {
    return {
      name: this.name,
      message: this.message,
    };
  }
}

export class NotFoundError extends SerializableError {
  override name = 'NotFoundError';
}

export class ConflictError extends SerializableError {
  override name = 'ConflictError';
}

export class ForeignKeyError extends SerializableError {
  override name = 'ForeignKeyError';
}

export const translateError = (err: Error): Error => {
  if (err instanceof Prisma.PrismaClientKnownRequestError) {
    // https://www.prisma.io/docs/reference/api-reference/error-reference#error-codes
    switch (err.code) {
      case 'P2002':
        return new ConflictError();
      case 'P2003':
        return new ForeignKeyError();
      case 'P2025':
        return new NotFoundError();
      default:
        break;
    }
  }

  return err;
};

export const withTranslatedErrors = <Args extends Array<any>, Ret>(
  fn: (...args: Args) => Promise<Ret>
  // eslint-disable-next-line arrow-body-style
) => {
  return wrapAsync(withResultAsync(fn), (result) => {
    if (result.isOk) {
      return result.value;
    }

    throw translateError(result.error);
  });
};
