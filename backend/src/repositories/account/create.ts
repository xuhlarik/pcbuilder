import type { Account } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type AccountCreateData = Account;

export const createAccount = withTranslatedErrors(
  async (data: AccountCreateData) => prisma.account.create({ data })
);
