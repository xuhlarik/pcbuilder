import { omit, pick } from '@pcbuilder/common/utils';
import type { Account } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type AccountUpdateData = Pick<Account, 'login'> & Partial<Account>;

export const updateAccount = withTranslatedErrors(
  async (data: AccountUpdateData) =>
    prisma.account.update({
      data: omit(data, 'login'),
      where: pick(data, 'login'),
    })
);
