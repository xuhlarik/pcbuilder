import { createAccount } from './create';
import { deleteAccount } from './delete';
import { readOneAccount } from './read';
import { updateAccount } from './update';

export type { AccountCreateData } from './create';
export type { AccountDeleteData } from './delete';
export type { AccountReadOneData } from './read';
export type { AccountUpdateData } from './update';

export default {
  create: createAccount,
  delete: deleteAccount,
  readOne: readOneAccount,
  update: updateAccount,
};
