import type { Account } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type AccountReadOneData = Pick<Account, 'login'>;

export const readOneAccount = withTranslatedErrors(
  async (data: AccountReadOneData) =>
    prisma.account.findUniqueOrThrow({ where: data })
);
