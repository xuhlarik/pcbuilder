import type { Account } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type AccountDeleteData = Pick<Account, 'login'>;

export const deleteAccount = withTranslatedErrors(
  async (data: AccountDeleteData) =>
    prisma.account.delete({
      where: data,
    })
);
