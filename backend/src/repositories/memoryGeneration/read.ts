import type { Prisma, MemoryGeneration } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type MemoryGenerationReadOneData = Pick<MemoryGeneration, 'id'>;

export const readOneMemoryGeneration = withTranslatedErrors(
  async (data: MemoryGenerationReadOneData) => {
    const result = await prisma.memoryGeneration.findUniqueOrThrow({
      where: { id: data.id },
    });

    return result;
  }
);

export type MemoryGenerationReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<MemoryGeneration, 'name'>>;

export const readManyMemoryGeneration = withTranslatedErrors(
  async (data: MemoryGenerationReadManyData) => {
    const result = await prisma.memoryGeneration.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
    });

    return result;
  }
);
