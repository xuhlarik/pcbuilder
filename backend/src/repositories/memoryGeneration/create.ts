import type { MemoryGeneration } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { CreateData } from '../common';

export type MemoryGenerationCreateData = CreateData<MemoryGeneration>;

export const createMemoryGeneration = withTranslatedErrors(
  async (data: MemoryGenerationCreateData) => {
    const result = await prisma.memoryGeneration.create({ data });

    return result;
  }
);
