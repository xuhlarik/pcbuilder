import { createMemoryGeneration } from './create';
import { deleteMemoryGeneration } from './delete';
import { readManyMemoryGeneration, readOneMemoryGeneration } from './read';
import { updateMemoryGeneration } from './update';

export type { MemoryGenerationCreateData } from './create';
export type { MemoryGenerationDeleteData } from './delete';
export type {
  MemoryGenerationReadManyData,
  MemoryGenerationReadOneData,
} from './read';
export type { MemoryGenerationUpdateData } from './update';

export default {
  create: createMemoryGeneration,
  delete: deleteMemoryGeneration,
  read: {
    one: readOneMemoryGeneration,
    many: readManyMemoryGeneration,
  },
  update: updateMemoryGeneration,
};
