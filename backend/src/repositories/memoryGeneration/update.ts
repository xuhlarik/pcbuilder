import { omit } from '@pcbuilder/common/utils';
import type { MemoryGeneration } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { UpdateData } from '../common';

export type MemoryGenerationUpdateData = UpdateData<MemoryGeneration>;

export const updateMemoryGeneration = withTranslatedErrors(
  async (data: MemoryGenerationUpdateData) => {
    const result = await prisma.memoryGeneration.update({
      data: omit(data, 'id'),
      where: { id: data.id },
    });

    return result;
  }
);
