import type { MemoryGeneration } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type MemoryGenerationDeleteData = Pick<MemoryGeneration, 'id'>;

export const deleteMemoryGeneration = withTranslatedErrors(
  async (data: MemoryGenerationDeleteData) => {
    const result = await prisma.memoryGeneration.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
