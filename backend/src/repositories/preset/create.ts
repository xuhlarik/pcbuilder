import type { Preset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { CreateData } from '../common';

export type PresetCreateData = CreateData<Preset>;

export const createPreset = withTranslatedErrors(
  async (data: PresetCreateData) => {
    const result = await prisma.preset.create({
      data,
      include,
    });
    return result;
  }
);
