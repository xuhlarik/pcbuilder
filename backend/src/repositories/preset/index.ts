import { createPreset } from './create';
import { deletePreset } from './delete';
import { readManyPreset, readOnePreset } from './read';
import { updatePreset } from './update';

export type { PresetCreateData } from './create';
export type { PresetDeleteData } from './delete';
export type { PresetReadManyData, PresetReadOneData } from './read';
export type { PresetUpdateData } from './update';

export default {
  create: createPreset,
  delete: deletePreset,
  read: {
    one: readOnePreset,
    many: readManyPreset,
  },
  update: updatePreset,
};
