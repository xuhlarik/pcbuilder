import type { Prisma, Preset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';

export type PresetReadOneData = Pick<Preset, 'id'>;

export const readOnePreset = withTranslatedErrors(
  async (data: PresetReadOneData) => {
    const result = await prisma.preset.findUniqueOrThrow({
      where: { id: data.id },
      include,
    });

    return result;
  }
);

export type PresetReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<Preset, 'name'>> & {
    categoryId?: string[];
    buildId?: string[];
  };

export const readManyPreset = withTranslatedErrors(
  async (data: PresetReadManyData) => {
    const result = await prisma.preset.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
        categoryId: { in: data.categoryId },
        buildId: { in: data.buildId },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
      include,
    });

    return result;
  }
);
