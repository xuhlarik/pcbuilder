import { omit } from '@pcbuilder/common/utils';
import type { Preset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { UpdateData } from '../common';

export type PresetUpdateData = UpdateData<Preset>;

export const updatePreset = withTranslatedErrors(
  async (data: PresetUpdateData) => {
    const result = await prisma.preset.update({
      data: omit(data, 'id'),
      where: { id: data.id },
      include,
    });

    return result;
  }
);
