import type { Preset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type PresetDeleteData = Pick<Preset, 'id'>;

export const deletePreset = withTranslatedErrors(
  async (data: PresetDeleteData) => {
    const result = await prisma.preset.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
