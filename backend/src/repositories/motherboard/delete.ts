import type { Motherboard } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type MotherboardDeleteData = Pick<Motherboard, 'id'>;

export const deleteMotherboard = withTranslatedErrors(
  async (data: MotherboardDeleteData) => {
    const result = await prisma.motherboard.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
