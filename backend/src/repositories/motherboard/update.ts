import { omit } from '@pcbuilder/common/utils';
import type { Motherboard } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { UpdateData } from '../common';

export type MotherboardUpdateData = UpdateData<Motherboard>;

export const updateMotherboard = withTranslatedErrors(
  async (data: MotherboardUpdateData) => {
    const result = await prisma.motherboard.update({
      data: omit(data, 'id'),
      where: { id: data.id },
      include,
    });

    return result;
  }
);
