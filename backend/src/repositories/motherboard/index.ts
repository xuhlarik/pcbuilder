import { createMotherboard } from './create';
import { deleteMotherboard } from './delete';
import { readManyMotherboard, readOneMotherboard } from './read';
import { updateMotherboard } from './update';

export type { MotherboardCreateData } from './create';
export type { MotherboardDeleteData } from './delete';
export type { MotherboardReadManyData, MotherboardReadOneData } from './read';
export type { MotherboardUpdateData } from './update';

export default {
  create: createMotherboard,
  delete: deleteMotherboard,
  read: {
    one: readOneMotherboard,
    many: readManyMotherboard,
  },
  update: updateMotherboard,
};
