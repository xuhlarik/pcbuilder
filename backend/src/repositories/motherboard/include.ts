import chipset from '../motherboardChipset/include';

export default {
  manufacturer: true,
  chipset: { include: chipset },
  formFactor: true,
  socket: true,
  pcieSlots: {
    where: { deletedAt: null },
    include: { pcieGeneration: true },
  },
};
