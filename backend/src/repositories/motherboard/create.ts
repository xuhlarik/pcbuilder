import type { Motherboard, MotherboardPcieSlot } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { CreateData } from '../common';

export type MotherboardCreateData = CreateData<Motherboard> & {
  pcieSlots: Omit<CreateData<MotherboardPcieSlot>, 'motherboardId'>[];
};

export const createMotherboard = withTranslatedErrors(
  async ({ pcieSlots, ...data }: MotherboardCreateData) => {
    const result = await prisma.motherboard.create({
      data: {
        ...data,
        pcieSlots: {
          create: pcieSlots.filter(({ count }) => count !== 0),
        },
      },
      include,
    });

    return result;
  }
);
