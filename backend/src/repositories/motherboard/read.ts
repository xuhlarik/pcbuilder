import type { Prisma, Motherboard } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';

export type MotherboardReadOneData = Pick<Motherboard, 'id'>;

export const readOneMotherboard = withTranslatedErrors(
  async (data: MotherboardReadOneData) => {
    const result = await prisma.motherboard.findUniqueOrThrow({
      where: { id: data.id },
      include,
    });

    return result;
  }
);

export type MotherboardReadManyData = {
  sort?: Prisma.SortOrder;
  manufacturerId?: string[];
  memoryGenerationId?: string[];
  socketId?: string[];
  chipsetId?: string[];
  formFactorId?: string[];
} & Partial<Pick<Motherboard, 'name'>>;

export const readManyMotherboard = withTranslatedErrors(
  async (data: MotherboardReadManyData) => {
    const result = await prisma.motherboard.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
        manufacturerId: { in: data.manufacturerId },
        socketId: { in: data.socketId },
        chipsetId: { in: data.chipsetId },
        formFactorId: { in: data.formFactorId },
        chipset: {
          memoryGenerationId: { in: data.memoryGenerationId },
        },
      },
      orderBy: {
        price: data.sort ?? 'asc',
      },
      include,
    });

    return result;
  }
);
