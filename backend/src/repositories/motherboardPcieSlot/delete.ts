import type { MotherboardPcieSlot } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type MotherboardPcieSlotDeleteData = Pick<MotherboardPcieSlot, 'id'>;

export const deleteMotherboardPcieSlot = withTranslatedErrors(
  async (data: MotherboardPcieSlotDeleteData) => {
    const result = await prisma.motherboardPcieSlot.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
