import { omit } from '@pcbuilder/common/utils';
import type { MotherboardPcieSlot } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { UpdateData } from '../common';

export type MotherboardPcieSlotUpdateData = UpdateData<MotherboardPcieSlot>;

export const updateMotherboardPcieSlot = withTranslatedErrors(
  async (data: MotherboardPcieSlotUpdateData) => {
    const result = await prisma.motherboardPcieSlot.update({
      data: omit(data, 'id'),
      where: { id: data.id },
    });

    return result;
  }
);
