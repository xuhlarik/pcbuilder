import { createMotherboardPcieSlot } from './create';
import { deleteMotherboardPcieSlot } from './delete';
import {
  readManyMotherboardPcieSlot,
  readOneMotherboardPcieSlot,
} from './read';
import { updateMotherboardPcieSlot } from './update';

export type { MotherboardPcieSlotCreateData } from './create';
export type { MotherboardPcieSlotDeleteData } from './delete';
export type { MotherboardPcieSlotReadOneData } from './read';
export type { MotherboardPcieSlotUpdateData } from './update';

export default {
  create: createMotherboardPcieSlot,
  delete: deleteMotherboardPcieSlot,
  read: {
    one: readOneMotherboardPcieSlot,
    many: readManyMotherboardPcieSlot,
  },
  update: updateMotherboardPcieSlot,
};
