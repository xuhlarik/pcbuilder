import type { MotherboardPcieSlot } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type MotherboardPcieSlotReadOneData = Pick<MotherboardPcieSlot, 'id'>;

export const readOneMotherboardPcieSlot = withTranslatedErrors(
  async (data: MotherboardPcieSlotReadOneData) => {
    const result = await prisma.motherboardPcieSlot.findUniqueOrThrow({
      where: { id: data.id },
    });

    return result;
  }
);

export type MotherboardPcieSlotReadManyData = {
  motherboardId?: string[];
  pcieGenerationId?: string[];
  size?: number[];
  count?: number[];
};

export const readManyMotherboardPcieSlot = withTranslatedErrors(
  async (data: MotherboardPcieSlotReadManyData) => {
    const result = await prisma.motherboardPcieSlot.findMany({
      where: {
        deletedAt: null,
        motherboardId: { in: data.motherboardId },
        pcieGenerationId: { in: data.pcieGenerationId },
        size: { in: data.size },
        count: { in: data.count },
      },
    });

    return result;
  }
);
