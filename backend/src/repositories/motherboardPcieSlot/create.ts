import type { MotherboardPcieSlot } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { CreateData } from '../common';

export type MotherboardPcieSlotCreateData = CreateData<MotherboardPcieSlot>;

export const createMotherboardPcieSlot = withTranslatedErrors(
  async (data: MotherboardPcieSlotCreateData) => {
    const result = await prisma.motherboardPcieSlot.create({ data });

    return result;
  }
);
