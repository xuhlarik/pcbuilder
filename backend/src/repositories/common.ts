export type CreateData<T> = Omit<T, 'id' | 'deletedAt'>;

export type UpdateData<T extends { id: string }> = Pick<T, 'id'> &
  Partial<CreateData<T>>;
