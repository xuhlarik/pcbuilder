import type { FormFactor } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type FormFactorDeleteData = Pick<FormFactor, 'id'>;

export const deleteFormFactor = withTranslatedErrors(
  async (data: FormFactorDeleteData) => {
    const result = await prisma.formFactor.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
