import type { Prisma, FormFactor } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type FormFactorReadOneData = Pick<FormFactor, 'id'>;

export const readOneFormFactor = withTranslatedErrors(
  async (data: FormFactorReadOneData) => {
    const result = await prisma.formFactor.findUniqueOrThrow({
      where: { id: data.id },
    });

    return result;
  }
);

export type FormFactorReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<FormFactor, 'name'>>;

export const readManyFormFactor = withTranslatedErrors(
  async (data: FormFactorReadManyData) => {
    const result = await prisma.formFactor.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
    });

    return result;
  }
);
