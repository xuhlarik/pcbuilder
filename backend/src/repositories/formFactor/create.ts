import type { FormFactor } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { CreateData } from '../common';

export type FormFactorCreateData = CreateData<FormFactor>;

export const createFormFactor = withTranslatedErrors(
  async (data: FormFactorCreateData) => {
    const result = await prisma.formFactor.create({ data });

    return result;
  }
);
