import { createFormFactor } from './create';
import { deleteFormFactor } from './delete';
import { readManyFormFactor, readOneFormFactor } from './read';
import { updateFormFactor } from './update';

export type { FormFactorCreateData } from './create';
export type { FormFactorDeleteData } from './delete';
export type { FormFactorReadManyData, FormFactorReadOneData } from './read';
export type { FormFactorUpdateData } from './update';

export default {
  create: createFormFactor,
  delete: deleteFormFactor,
  read: {
    one: readOneFormFactor,
    many: readManyFormFactor,
  },
  update: updateFormFactor,
};
