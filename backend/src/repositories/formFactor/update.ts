import { omit } from '@pcbuilder/common/utils';
import type { FormFactor } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { UpdateData } from '../common';

export type FormFactorUpdateData = UpdateData<FormFactor>;

export const updateFormFactor = withTranslatedErrors(
  async (data: FormFactorUpdateData) => {
    const result = await prisma.formFactor.update({
      data: omit(data, 'id'),
      where: { id: data.id },
    });

    return result;
  }
);
