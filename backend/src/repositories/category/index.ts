import { createCategory } from './create';
import { deleteCategory } from './delete';
import { readManyCategory, readOneCategory } from './read';
import { updateCategory } from './update';

export type { CategoryCreateData } from './create';
export type { CategoryDeleteData } from './delete';
export type { CategoryReadManyData, CategoryReadOneData } from './read';
export type { CategoryUpdateData } from './update';

export default {
  create: createCategory,
  delete: deleteCategory,
  read: {
    one: readOneCategory,
    many: readManyCategory,
  },
  update: updateCategory,
};
