import type { Prisma, Category } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type CategoryReadOneData = Pick<Category, 'id'>;

export const readOneCategory = withTranslatedErrors(
  async (data: CategoryReadOneData) => {
    const result = await prisma.category.findUniqueOrThrow({
      where: { id: data.id },
      include: {
        preset: {
          where: { deletedAt: null },
          include: {
            category: true,
          },
        },
      },
    });

    return result;
  }
);

export type CategoryReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<Category, 'name'>>;

export const readManyCategory = withTranslatedErrors(
  async (data: CategoryReadManyData) => {
    const result = await prisma.category.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
    });

    return result;
  }
);
