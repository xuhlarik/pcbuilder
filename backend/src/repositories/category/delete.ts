import type { Category } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type CategoryDeleteData = Pick<Category, 'id'>;

export const deleteCategory = withTranslatedErrors(
  async (data: CategoryDeleteData) => {
    const result = await prisma.category.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
