import { omit } from '@pcbuilder/common/utils';
import type { Category } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { UpdateData } from '../common';

export type CategoryUpdateData = UpdateData<Category>;

export const updateCategory = withTranslatedErrors(
  async (data: CategoryUpdateData) => {
    const result = await prisma.category.update({
      data: omit(data, 'id'),
      where: { id: data.id },
    });

    return result;
  }
);
