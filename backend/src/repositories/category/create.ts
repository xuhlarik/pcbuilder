import type { Category } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { CreateData } from '../common';

export type CategoryCreateData = CreateData<Category>;

export const createCategory = withTranslatedErrors(
  async (data: CategoryCreateData) => {
    const result = await prisma.category.create({ data });

    return result;
  }
);
