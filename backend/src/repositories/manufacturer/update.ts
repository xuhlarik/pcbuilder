import { omit } from '@pcbuilder/common/utils';
import type { Manufacturer } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { UpdateData } from '../common';

export type ManufacturerUpdateData = UpdateData<Manufacturer>;

export const updateManufacturer = withTranslatedErrors(
  async (data: ManufacturerUpdateData) =>
    prisma.manufacturer.update({
      data: omit(data, 'id'),
      where: { id: data.id },
    })
);
