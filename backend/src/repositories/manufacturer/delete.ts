import type { Manufacturer } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type ManufacturerDeleteData = Pick<Manufacturer, 'id'>;

export const deleteManufacturer = withTranslatedErrors(
  async (data: ManufacturerDeleteData) =>
    prisma.manufacturer.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    })
);
