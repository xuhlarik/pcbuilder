import type { Prisma, Manufacturer } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type ManufacturerReadOneData = Pick<Manufacturer, 'id'>;

export const readOneManufacturer = withTranslatedErrors(
  async (data: ManufacturerReadOneData) => {
    const record = await prisma.manufacturer.findUniqueOrThrow({
      where: { id: data.id },
    });

    return record;
  }
);

export type ManufacturerReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<Manufacturer, 'name'>>;

export const readManyManufacturer = withTranslatedErrors(
  async (data: ManufacturerReadManyData) =>
    prisma.manufacturer.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
    })
);
