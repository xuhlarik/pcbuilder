import type { Manufacturer } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { CreateData } from '../common';

export type ManufacturerCreateData = CreateData<Manufacturer>;

export const createManufacturer = withTranslatedErrors(
  async (data: ManufacturerCreateData) => prisma.manufacturer.create({ data })
);
