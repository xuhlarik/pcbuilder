import { createManufacturer } from './create';
import { deleteManufacturer } from './delete';
import { readManyManufacturer, readOneManufacturer } from './read';
import { updateManufacturer } from './update';

export type { ManufacturerCreateData } from './create';
export type { ManufacturerDeleteData } from './delete';
export type { ManufacturerReadManyData, ManufacturerReadOneData } from './read';
export type { ManufacturerUpdateData } from './update';

export default {
  create: createManufacturer,
  delete: deleteManufacturer,
  read: {
    one: readOneManufacturer,
    many: readManyManufacturer,
  },
  update: updateManufacturer,
};
