import type { VideoChipset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type VideoChipsetDeleteData = Pick<VideoChipset, 'id'>;

export const deleteVideoChipset = withTranslatedErrors(
  async (data: VideoChipsetDeleteData) => {
    const result = await prisma.videoChipset.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
