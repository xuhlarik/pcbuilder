import type { Prisma, VideoChipset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';

export type VideoChipsetReadOneData = Pick<VideoChipset, 'id'>;

export const readOneVideoChipset = withTranslatedErrors(
  async (data: VideoChipsetReadOneData) => {
    const result = await prisma.videoChipset.findUniqueOrThrow({
      where: { id: data.id },
      include,
    });

    return result;
  }
);

export type VideoChipsetReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<VideoChipset, 'name'>> & {
    manufacturerId?: string[];
  };

export const readManyVideoChipset = withTranslatedErrors(
  async (data: VideoChipsetReadManyData) => {
    const result = await prisma.videoChipset.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
        manufacturerId: { in: data.manufacturerId },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
      include,
    });

    return result;
  }
);
