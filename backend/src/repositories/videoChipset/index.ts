import { createVideoChipset } from './create';
import { deleteVideoChipset } from './delete';
import { readManyVideoChipset, readOneVideoChipset } from './read';
import { updateVideoChipset } from './update';

export type { VideoChipsetCreateData } from './create';
export type { VideoChipsetDeleteData } from './delete';
export type { VideoChipsetReadManyData, VideoChipsetReadOneData } from './read';
export type { VideoChipsetUpdateData } from './update';

export default {
  create: createVideoChipset,
  delete: deleteVideoChipset,
  read: {
    one: readOneVideoChipset,
    many: readManyVideoChipset,
  },
  update: updateVideoChipset,
};
