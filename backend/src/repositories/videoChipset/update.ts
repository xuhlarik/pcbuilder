import { omit } from '@pcbuilder/common/utils';
import type { VideoChipset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { UpdateData } from '../common';

export type VideoChipsetUpdateData = UpdateData<VideoChipset>;

export const updateVideoChipset = withTranslatedErrors(
  async (data: VideoChipsetUpdateData) => {
    const result = await prisma.videoChipset.update({
      data: omit(data, 'id'),
      where: { id: data.id },
      include,
    });

    return result;
  }
);
