import type { VideoChipset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { CreateData } from '../common';

export type VideoChipsetCreateData = CreateData<VideoChipset>;

export const createVideoChipset = withTranslatedErrors(
  async (data: VideoChipsetCreateData) => {
    const result = await prisma.videoChipset.create({
      data,
      include,
    });

    return result;
  }
);
