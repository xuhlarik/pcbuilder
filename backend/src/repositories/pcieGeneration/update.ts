import { omit } from '@pcbuilder/common/utils';
import type { PcieGeneration } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { UpdateData } from '../common';

export type PcieGenerationUpdateData = UpdateData<PcieGeneration>;

export const updatePcieGeneration = withTranslatedErrors(
  async (data: PcieGenerationUpdateData) => {
    const result = await prisma.pcieGeneration.update({
      data: omit(data, 'id'),
      where: { id: data.id },
    });

    return result;
  }
);
