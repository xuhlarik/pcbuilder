import type { Prisma, PcieGeneration } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type PcieGenerationReadOneData = Pick<PcieGeneration, 'id'>;

export const readOnePcieGeneration = withTranslatedErrors(
  async (data: PcieGenerationReadOneData) => {
    const result = await prisma.pcieGeneration.findUniqueOrThrow({
      where: { id: data.id },
    });

    return result;
  }
);

export type PcieGenerationReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<PcieGeneration, 'name'>>;

export const readManyPcieGeneration = withTranslatedErrors(
  async (data: PcieGenerationReadManyData) => {
    const result = await prisma.pcieGeneration.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
    });

    return result;
  }
);
