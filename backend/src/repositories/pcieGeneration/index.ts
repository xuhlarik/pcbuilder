import { createPcieGeneration } from './create';
import { deletePcieGeneration } from './delete';
import { readManyPcieGeneration, readOnePcieGeneration } from './read';
import { updatePcieGeneration } from './update';

export type { PcieGenerationCreateData } from './create';
export type { PcieGenerationDeleteData } from './delete';
export type {
  PcieGenerationReadManyData,
  PcieGenerationReadOneData,
} from './read';
export type { PcieGenerationUpdateData } from './update';

export default {
  create: createPcieGeneration,
  delete: deletePcieGeneration,
  read: {
    one: readOnePcieGeneration,
    many: readManyPcieGeneration,
  },
  update: updatePcieGeneration,
};
