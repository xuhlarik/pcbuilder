import type { PcieGeneration } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type PcieGenerationDeleteData = Pick<PcieGeneration, 'id'>;

export const deletePcieGeneration = withTranslatedErrors(
  async (data: PcieGenerationDeleteData) => {
    const result = await prisma.pcieGeneration.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
