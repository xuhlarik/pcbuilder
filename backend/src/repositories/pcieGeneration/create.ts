import type { PcieGeneration } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { CreateData } from '../common';

export type PcieGenerationCreateData = CreateData<PcieGeneration>;

export const createPcieGeneration = withTranslatedErrors(
  async (data: PcieGenerationCreateData) => {
    const result = await prisma.pcieGeneration.create({ data });

    return result;
  }
);
