import type { Prisma, Memory } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';

export type MemoryReadOneData = Pick<Memory, 'id'>;

export const readOneMemory = withTranslatedErrors(
  async (data: MemoryReadOneData) => {
    const record = await prisma.memory.findUniqueOrThrow({
      where: { id: data.id },
      include,
    });

    return record;
  }
);

export type MemoryReadManyData = {
  sort?: Prisma.SortOrder;
  manufacturerId?: string[];
  memoryGenerationId?: string[];
} & Partial<Pick<Memory, 'name'>>;

export const readManyMemory = withTranslatedErrors(
  async (data: MemoryReadManyData) =>
    prisma.memory.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
        manufacturerId: { in: data.manufacturerId },
        memoryGenerationId: { in: data.memoryGenerationId },
      },
      orderBy: {
        price: data.sort ?? 'asc',
      },
      include,
    })
);
