import type { Memory } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type MemoryDeleteData = Pick<Memory, 'id'>;

export const deleteMemory = withTranslatedErrors(
  async (data: MemoryDeleteData) =>
    prisma.memory.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    })
);
