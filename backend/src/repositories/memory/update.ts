import { omit } from '@pcbuilder/common/utils';
import type { Memory } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { UpdateData } from '../common';

export type MemoryUpdateData = UpdateData<Memory>;

export const updateMemory = withTranslatedErrors(
  async (data: MemoryUpdateData) =>
    prisma.memory.update({
      data: omit(data, 'id'),
      where: { id: data.id },
      include,
    })
);
