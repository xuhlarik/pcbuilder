import { createMemory } from './create';
import { deleteMemory } from './delete';
import { readManyMemory, readOneMemory } from './read';
import { updateMemory } from './update';

export type { MemoryCreateData } from './create';
export type { MemoryDeleteData } from './delete';
export type { MemoryReadManyData, MemoryReadOneData } from './read';
export type { MemoryUpdateData } from './update';

export default {
  create: createMemory,
  delete: deleteMemory,
  read: {
    one: readOneMemory,
    many: readManyMemory,
  },
  update: updateMemory,
};
