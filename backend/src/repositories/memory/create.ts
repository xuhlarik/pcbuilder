import type { Memory } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { CreateData } from '../common';

export type MemoryCreateData = CreateData<Memory>;

export const createMemory = withTranslatedErrors(
  async (data: MemoryCreateData) =>
    prisma.memory.create({
      data,
      include,
    })
);
