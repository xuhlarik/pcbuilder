import { createMotherboardChipset } from './create';
import { deleteMotherboardChipset } from './delete';
import { readManyMotherboardChipset, readOneMotherboardChipset } from './read';
import { updateMotherboardChipset } from './update';

export type { MotherboardChipsetCreateData } from './create';
export type { MotherboardChipsetDeleteData } from './delete';
export type {
  MotherboardChipsetReadManyData,
  MotherboardChipsetReadOneData,
} from './read';
export type { MotherboardChipsetUpdateData } from './update';

export default {
  create: createMotherboardChipset,
  delete: deleteMotherboardChipset,
  read: {
    one: readOneMotherboardChipset,
    many: readManyMotherboardChipset,
  },
  update: updateMotherboardChipset,
};
