import type { Prisma, MotherboardChipset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';

export type MotherboardChipsetReadOneData = Pick<MotherboardChipset, 'id'>;

export const readOneMotherboardChipset = withTranslatedErrors(
  async (data: MotherboardChipsetReadOneData) => {
    const result = await prisma.motherboardChipset.findUniqueOrThrow({
      where: { id: data.id },
      include,
    });

    return result;
  }
);

export type MotherboardChipsetReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<MotherboardChipset, 'name'>> & {
    manufacturerId?: string[];
    memoryGenerationId?: string[];
  };

export const readManyMotherboardChipset = withTranslatedErrors(
  async (data: MotherboardChipsetReadManyData) => {
    const result = await prisma.motherboardChipset.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
        manufacturerId: { in: data.manufacturerId },
        memoryGenerationId: { in: data.memoryGenerationId },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
      include,
    });

    return result;
  }
);
