import type { MotherboardChipset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { CreateData } from '../common';

export type MotherboardChipsetCreateData = CreateData<MotherboardChipset>;

export const createMotherboardChipset = withTranslatedErrors(
  async (data: MotherboardChipsetCreateData) => {
    const result = await prisma.motherboardChipset.create({
      data,
      include,
    });

    return result;
  }
);
