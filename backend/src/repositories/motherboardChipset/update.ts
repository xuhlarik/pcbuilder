import { omit } from '@pcbuilder/common/utils';
import type { MotherboardChipset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { UpdateData } from '../common';

export type MotherboardChipsetUpdateData = UpdateData<MotherboardChipset>;

export const updateMotherboardChipset = withTranslatedErrors(
  async (data: MotherboardChipsetUpdateData) => {
    const result = await prisma.motherboardChipset.update({
      data: omit(data, 'id'),
      where: { id: data.id },
      include,
    });

    return result;
  }
);
