import type { MotherboardChipset } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type MotherboardChipsetDeleteData = Pick<MotherboardChipset, 'id'>;

export const deleteMotherboardChipset = withTranslatedErrors(
  async (data: MotherboardChipsetDeleteData) => {
    const result = await prisma.motherboardChipset.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
