import { omit } from '@pcbuilder/common/utils';
import type { VideoCard } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { UpdateData } from '../common';

export type VideoCardUpdateData = UpdateData<VideoCard>;

export const updateVideoCard = withTranslatedErrors(
  async (data: VideoCardUpdateData) => {
    const result = await prisma.videoCard.update({
      data: omit(data, 'id'),
      where: { id: data.id },
      include,
    });

    return result;
  }
);
