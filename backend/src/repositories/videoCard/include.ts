import chipset from '../videoChipset/include';

export default {
  manufacturer: true,
  pcieGeneration: true,
  chipset: { include: chipset },
};
