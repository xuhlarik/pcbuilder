import type { VideoCard } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { CreateData } from '../common';

export type VideoCardCreateData = CreateData<VideoCard>;

export const createVideoCard = withTranslatedErrors(
  async (data: VideoCardCreateData) => {
    const result = await prisma.videoCard.create({
      data,
      include,
    });

    return result;
  }
);
