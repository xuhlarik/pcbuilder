import { createVideoCard } from './create';
import { deleteVideoCard } from './delete';
import { readManyVideoCard, readOneVideoCard } from './read';
import { updateVideoCard } from './update';

export type { VideoCardCreateData } from './create';
export type { VideoCardDeleteData } from './delete';
export type { VideoCardReadManyData, VideoCardReadOneData } from './read';
export type { VideoCardUpdateData } from './update';

export default {
  create: createVideoCard,
  delete: deleteVideoCard,
  read: {
    one: readOneVideoCard,
    many: readManyVideoCard,
  },
  update: updateVideoCard,
};
