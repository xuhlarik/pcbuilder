import type { VideoCard } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type VideoCardDeleteData = Pick<VideoCard, 'id'>;

export const deleteVideoCard = withTranslatedErrors(
  async (data: VideoCardDeleteData) => {
    const result = await prisma.videoCard.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
