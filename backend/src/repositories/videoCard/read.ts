import type { Prisma, VideoCard } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';

export type VideoCardReadOneData = Pick<VideoCard, 'id'>;

export const readOneVideoCard = withTranslatedErrors(
  async (data: VideoCardReadOneData) => {
    const result = await prisma.videoCard.findUniqueOrThrow({
      where: { id: data.id },
      include,
    });

    return result;
  }
);

export type VideoCardReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<VideoCard, 'name'>> & {
    manufacturerId?: string[];
    pcieGenerationId?: string[];
    chipsetId?: string[];
    slotSize?: number[];
  };

export const readManyVideoCard = withTranslatedErrors(
  async (data: VideoCardReadManyData) => {
    const result = await prisma.videoCard.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
        manufacturerId: { in: data.manufacturerId },
        pcieGenerationId: { in: data.pcieGenerationId },
        chipsetId: { in: data.chipsetId },
        slotSize: { in: data.slotSize },
      },
      orderBy: {
        price: data.sort ?? 'asc',
      },
      include,
    });

    return result;
  }
);
