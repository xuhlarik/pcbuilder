import type { IntegratedGraphics } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { CreateData } from '../common';

export type IntegratedGraphicsCreateData = CreateData<IntegratedGraphics>;

export const createIntegratedGraphics = withTranslatedErrors(
  async (data: IntegratedGraphicsCreateData) => {
    const result = await prisma.integratedGraphics.create({ data });

    return result;
  }
);
