import { omit } from '@pcbuilder/common/utils';
import type { IntegratedGraphics } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { UpdateData } from '../common';

export type IntegratedGraphicsUpdateData = UpdateData<IntegratedGraphics>;

export const updateIntegratedGraphics = withTranslatedErrors(
  async (data: IntegratedGraphicsUpdateData) => {
    const result = await prisma.integratedGraphics.update({
      data: omit(data, 'id'),
      where: { id: data.id },
    });

    return result;
  }
);
