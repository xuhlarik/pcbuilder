import type { Prisma, IntegratedGraphics } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type IntegratedGraphicsReadOneData = Pick<IntegratedGraphics, 'id'>;

export const readOneIntegratedGraphics = withTranslatedErrors(
  async (data: IntegratedGraphicsReadOneData) => {
    const result = await prisma.integratedGraphics.findUniqueOrThrow({
      where: { id: data.id },
    });

    return result;
  }
);

export type IntegratedGraphicsReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<IntegratedGraphics, 'name'>>;

export const readManyIntegratedGraphics = withTranslatedErrors(
  async (data: IntegratedGraphicsReadManyData) => {
    const result = await prisma.integratedGraphics.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
    });

    return result;
  }
);
