import { createIntegratedGraphics } from './create';
import { deleteIntegratedGraphics } from './delete';
import { readManyIntegratedGraphics, readOneIntegratedGraphics } from './read';
import { updateIntegratedGraphics } from './update';

export type { IntegratedGraphicsCreateData } from './create';
export type { IntegratedGraphicsDeleteData } from './delete';
export type {
  IntegratedGraphicsReadManyData,
  IntegratedGraphicsReadOneData,
} from './read';
export type { IntegratedGraphicsUpdateData } from './update';

export default {
  create: createIntegratedGraphics,
  delete: deleteIntegratedGraphics,
  read: {
    one: readOneIntegratedGraphics,
    many: readManyIntegratedGraphics,
  },
  update: updateIntegratedGraphics,
};
