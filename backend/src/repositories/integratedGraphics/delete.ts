import type { IntegratedGraphics } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type IntegratedGraphicsDeleteData = Pick<IntegratedGraphics, 'id'>;

export const deleteIntegratedGraphics = withTranslatedErrors(
  async (data: IntegratedGraphicsDeleteData) => {
    const result = await prisma.integratedGraphics.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
