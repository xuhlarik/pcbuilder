import { createBuild } from './create';
import { deleteBuild } from './delete';
import { readManyBuild, readOneBuild } from './read';
import { updateBuild } from './update';

export type { BuildCreateData } from './create';
export type { BuildDeleteData } from './delete';
export type { BuildReadOneData } from './read';
export type { BuildUpdateData } from './update';

export default {
  create: createBuild,
  delete: deleteBuild,
  read: {
    one: readOneBuild,
    many: readManyBuild,
  },
  update: updateBuild,
};
