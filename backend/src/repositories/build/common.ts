import type prisma from '../client';
import { ConflictError } from '../errors';
import type include from './include';

const eq = <T>(x: T | undefined, y: T | undefined) => !x || !y || x === y;

type Build = Awaited<
  ReturnType<typeof prisma.build.findFirstOrThrow<{ include: typeof include }>>
>;

export const validateBuild = (build: Build) => {
  const { processor, motherboard, memory, videoCard } = build;

  if (!eq(processor?.socketId, motherboard?.socketId)) {
    throw new ConflictError('Socket');
  }

  if (
    !eq(memory?.memoryGenerationId, motherboard?.chipset.memoryGenerationId)
  ) {
    throw new ConflictError('MemoryGeneration');
  }

  if (motherboard !== null && videoCard !== null) {
    if (
      !motherboard.pcieSlots.some(
        (pcieSlot) => pcieSlot.size === videoCard.slotSize
      )
    ) {
      throw new ConflictError('PcieSlotSize');
    }
  }
};
