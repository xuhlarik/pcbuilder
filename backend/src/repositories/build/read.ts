import type { Build } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';

export type BuildReadOneData = Pick<Build, 'id'>;

export const readOneBuild = withTranslatedErrors(
  async (data: BuildReadOneData) => {
    const result = await prisma.build.findUniqueOrThrow({
      where: { id: data.id },
      include,
    });

    return result;
  }
);

export type BuildReadManyData = {};

export const readManyBuild = withTranslatedErrors(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async (_data: BuildReadManyData) => {
    const result = await prisma.build.findMany({
      where: { deletedAt: null },
      include,
    });
    return result;
  }
);
