import { omit } from '@pcbuilder/common/utils';
import type { Build } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import { validateBuild } from './common';
import include from './include';
import type { UpdateData } from '../common';

export type BuildUpdateData = UpdateData<Build>;

export const updateBuild = withTranslatedErrors(
  async (data: BuildUpdateData) => {
    const result = await prisma.$transaction(async (tx) => {
      const build = await tx.build.update({
        data: omit(data, 'id'),
        where: { id: data.id },
        include,
      });

      validateBuild(build);

      return build;
    });

    return result;
  }
);
