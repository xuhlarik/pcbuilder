import memory from '../memory/include';
import motherboard from '../motherboard/include';
import processor from '../processor/include';
import videoCard from '../videoCard/include';

export default {
  processor: { include: processor },
  motherboard: { include: motherboard },
  memory: { include: memory },
  videoCard: { include: videoCard },
};
