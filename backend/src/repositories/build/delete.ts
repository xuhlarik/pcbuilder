import type { Build } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type BuildDeleteData = Pick<Build, 'id'>;

export const deleteBuild = withTranslatedErrors(
  async (data: BuildDeleteData) => {
    const result = await prisma.build.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
