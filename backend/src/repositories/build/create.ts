import type { Build } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import { validateBuild } from './common';
import include from './include';
import type { CreateData } from '../common';

export type BuildCreateData = CreateData<Build>;

export const createBuild = withTranslatedErrors(
  async (data: BuildCreateData) => {
    const result = await prisma.$transaction(async (tx) => {
      const build = await tx.build.create({
        data,
        include,
      });

      validateBuild(build);

      return build;
    });

    return result;
  }
);
