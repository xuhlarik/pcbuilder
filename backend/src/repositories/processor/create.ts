import type { Processor } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { CreateData } from '../common';

export type ProcessorCreateData = CreateData<Processor>;

export const createProcessor = withTranslatedErrors(
  async (data: ProcessorCreateData) => {
    const result = await prisma.processor.create({
      data,
      include,
    });

    return result;
  }
);
