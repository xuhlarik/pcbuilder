import { createProcessor } from './create';
import { deleteProcessor } from './delete';
import { readManyProcessor, readOneProcessor } from './read';
import { updateProcessor } from './update';

export type { ProcessorCreateData } from './create';
export type { ProcessorDeleteData } from './delete';
export type { ProcessorReadManyData, ProcessorReadOneData } from './read';
export type { ProcessorUpdateData } from './update';

export default {
  create: createProcessor,
  delete: deleteProcessor,
  read: {
    one: readOneProcessor,
    many: readManyProcessor,
  },
  update: updateProcessor,
};
