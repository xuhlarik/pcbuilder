import type { Processor } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type ProcessorDeleteData = Pick<Processor, 'id'>;

export const deleteProcessor = withTranslatedErrors(
  async (data: ProcessorDeleteData) => {
    const result = await prisma.processor.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
