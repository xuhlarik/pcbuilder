import { omit } from '@pcbuilder/common/utils';
import type { Processor } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';
import type { UpdateData } from '../common';

export type ProcessorUpdateData = UpdateData<Processor>;

export const updateProcessor = withTranslatedErrors(
  async (data: ProcessorUpdateData) => {
    const result = await prisma.processor.update({
      data: omit(data, 'id'),
      where: { id: data.id },
      include,
    });

    return result;
  }
);
