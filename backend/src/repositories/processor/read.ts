import type { Prisma, Processor } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import include from './include';

export type ProcessorReadOneData = Pick<Processor, 'id'>;

export const readOneProcessor = withTranslatedErrors(
  async (data: ProcessorReadOneData) => {
    const result = await prisma.processor.findUniqueOrThrow({
      where: { id: data.id },
      include,
    });

    return result;
  }
);

export type ProcessorReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<Processor, 'name'>> & {
    manufacturerId?: string[];
    socketId?: string[];
    integratedGraphics?: boolean;
  };

const integratedGraphicsFilter = (value?: boolean) => {
  switch (value) {
    case true:
      return { not: null };
    case false:
      return null;
    default:
      return undefined;
  }
};

export const readManyProcessor = withTranslatedErrors(
  async (data: ProcessorReadManyData) => {
    const result = await prisma.processor.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
        manufacturerId: { in: data.manufacturerId },
        socketId: { in: data.socketId },
        integratedGraphicsId: integratedGraphicsFilter(data.integratedGraphics),
      },
      orderBy: {
        price: data.sort ?? 'asc',
      },
      include,
    });

    return result;
  }
);
