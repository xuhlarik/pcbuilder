import { createSocket } from './create';
import { deleteSocket } from './delete';
import { readManySocket, readOneSocket } from './read';
import { updateSocket } from './update';

export type { SocketCreateData } from './create';
export type { SocketDeleteData } from './delete';
export type { SocketReadManyData, SocketReadOneData } from './read';
export type { SocketUpdateData } from './update';

export default {
  create: createSocket,
  delete: deleteSocket,
  read: {
    one: readOneSocket,
    many: readManySocket,
  },
  update: updateSocket,
};
