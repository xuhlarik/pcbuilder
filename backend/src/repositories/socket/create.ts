import type { Socket } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { CreateData } from '../common';

export type SocketCreateData = CreateData<Socket>;

export const createSocket = withTranslatedErrors(
  async (data: SocketCreateData) => {
    const result = await prisma.socket.create({ data });

    return result;
  }
);
