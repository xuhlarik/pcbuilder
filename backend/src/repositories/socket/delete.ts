import type { Socket } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type SocketDeleteData = Pick<Socket, 'id'>;

export const deleteSocket = withTranslatedErrors(
  async (data: SocketDeleteData) => {
    const result = await prisma.socket.update({
      data: { deletedAt: new Date() },
      where: { id: data.id },
    });

    return result;
  }
);
