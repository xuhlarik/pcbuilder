import { omit } from '@pcbuilder/common/utils';
import type { Socket } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';
import type { UpdateData } from '../common';

export type SocketUpdateData = UpdateData<Socket>;

export const updateSocket = withTranslatedErrors(
  async (data: SocketUpdateData) => {
    const result = await prisma.socket.update({
      data: omit(data, 'id'),
      where: { id: data.id },
    });

    return result;
  }
);
