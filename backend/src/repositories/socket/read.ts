import type { Prisma, Socket } from '@prisma/client';
import prisma from '../client';
import { withTranslatedErrors } from '../errors';

export type SocketReadOneData = Pick<Socket, 'id'>;

export const readOneSocket = withTranslatedErrors(
  async (data: SocketReadOneData) => {
    const result = await prisma.socket.findUniqueOrThrow({
      where: { id: data.id },
    });

    return result;
  }
);

export type SocketReadManyData = {
  sort?: Prisma.SortOrder;
} & Partial<Pick<Socket, 'name'>>;

export const readManySocket = withTranslatedErrors(
  async (data: SocketReadManyData) => {
    const result = await prisma.socket.findMany({
      where: {
        deletedAt: null,
        name: { contains: data.name ?? '' },
      },
      orderBy: {
        name: data.sort ?? 'asc',
      },
    });

    return result;
  }
);
