import { AccessRole, Socket, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createSocket = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.socket.create.parseAsync(req.body);
    const record = await db.socket.create(data);
    return new Socket(record);
  },
  status: StatusCodes.CREATED,
});
