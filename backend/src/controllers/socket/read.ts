import { Socket, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOneSocket = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.socket.readOne.parseAsync(req.query);
    const record = await db.socket.read.one({ ...req.params, ...data });
    return new Socket(record);
  },
});

export const readManySockets = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.socket.readMany.parseAsync(req.query);
    const records = await db.socket.read.many({ ...req.params, ...data });
    return records.map((record) => new Socket(record));
  },
});
