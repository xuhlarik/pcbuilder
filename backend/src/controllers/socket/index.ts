import { createSocket } from './create';
import { deleteSocket } from './delete';
import { readOneSocket, readManySockets } from './read';
import { updateSocket } from './update';

export default {
  create: createSocket,
  delete: deleteSocket,
  readOne: readOneSocket,
  readMany: readManySockets,
  update: updateSocket,
};
