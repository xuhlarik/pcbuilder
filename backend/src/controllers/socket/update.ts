import { AccessRole, Socket, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updateSocket = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.socket.update.parseAsync(req.body);
    const record = await db.socket.update({ ...req.params, ...data });
    return new Socket(record);
  },
});
