import { AccessRole, VideoChipset, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createVideoChipset = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.videoChipset.create.parseAsync(req.body);
    const record = await db.videoChipset.create(data);
    return new VideoChipset(record);
  },
  status: StatusCodes.CREATED,
});
