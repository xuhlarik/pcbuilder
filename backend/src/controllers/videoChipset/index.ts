import { createVideoChipset } from './create';
import { deleteVideoChipset } from './delete';
import { readOneVideoChipset, readManyVideoChipsets } from './read';
import { updateVideoChipset } from './update';

export default {
  create: createVideoChipset,
  delete: deleteVideoChipset,
  readOne: readOneVideoChipset,
  readMany: readManyVideoChipsets,
  update: updateVideoChipset,
};
