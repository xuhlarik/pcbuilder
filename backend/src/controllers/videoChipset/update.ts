import { AccessRole, VideoChipset, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updateVideoChipset = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.videoChipset.update.parseAsync(req.body);
    const record = await db.videoChipset.update({ ...req.params, ...data });
    return new VideoChipset(record);
  },
});
