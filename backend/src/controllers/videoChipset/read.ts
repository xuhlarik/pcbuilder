import { VideoChipset, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOneVideoChipset = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.videoChipset.readOne.parseAsync(req.query);
    const record = await db.videoChipset.read.one({ ...req.params, ...data });
    return new VideoChipset(record);
  },
});

export const readManyVideoChipsets = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.videoChipset.readMany.parseAsync(req.query);
    const records = await db.videoChipset.read.many({ ...req.params, ...data });
    return records.map((record) => new VideoChipset(record));
  },
});
