import { createCategory } from './create';
import { deleteCategory } from './delete';
import { readOneCategory, readManyCategories } from './read';
import { updateCategory } from './update';

export default {
  create: createCategory,
  delete: deleteCategory,
  readOne: readOneCategory,
  readMany: readManyCategories,
  update: updateCategory,
};
