import { AccessRole, Category, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createCategory = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.category.create.parseAsync(req.body);
    const record = await db.category.create(data);
    return new Category(record);
  },
  status: StatusCodes.CREATED,
});
