import { Category, CategoryWithPresets, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOneCategory = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.category.readOne.parseAsync(req.query);
    const { preset, ...record } = await db.category.read.one({
      ...req.params,
      ...data,
    });
    return new CategoryWithPresets({ ...record, presets: preset });
  },
});

export const readManyCategories = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.category.readMany.parseAsync(req.query);
    const records = await db.category.read.many({ ...req.params, ...data });
    return records.map((record) => new Category(record));
  },
});
