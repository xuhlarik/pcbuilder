import { AccessRole, Category, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updateCategory = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.category.update.parseAsync(req.body);
    const record = await db.category.update({ ...req.params, ...data });
    return new Category(record);
  },
});
