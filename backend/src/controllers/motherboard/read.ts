import { Motherboard, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler, withNumericPrice } from '../common';

export const readOneMotherboard = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.motherboard.readOne.parseAsync(req.query);
    const record = await db.motherboard.read.one({ ...req.params, ...data });
    return new Motherboard(withNumericPrice(record));
  },
});

export const readManyMotherboards = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.motherboard.readMany.parseAsync(req.query);
    const records = await db.motherboard.read.many({ ...req.params, ...data });
    return records.map((record) => new Motherboard(withNumericPrice(record)));
  },
});
