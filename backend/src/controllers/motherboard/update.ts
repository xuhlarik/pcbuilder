import { AccessRole, Motherboard, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import {
  assertImageExists,
  handler,
  withNumericPrice,
  withOptionalDecimalPrice,
} from '../common';

export const updateMotherboard = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.motherboard.update.parseAsync(req.body);
    await assertImageExists(data);
    const record = await db.motherboard.update({
      ...req.params,
      ...withOptionalDecimalPrice(data),
    });
    return new Motherboard(withNumericPrice(record));
  },
});
