import { createMotherboard } from './create';
import { deleteMotherboard } from './delete';
import { readOneMotherboard, readManyMotherboards } from './read';
import { updateMotherboard } from './update';

export default {
  create: createMotherboard,
  delete: deleteMotherboard,
  readOne: readOneMotherboard,
  readMany: readManyMotherboards,
  update: updateMotherboard,
};
