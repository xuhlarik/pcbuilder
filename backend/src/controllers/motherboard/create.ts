import { AccessRole, Motherboard, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import {
  assertImageExists,
  handler,
  withDecimalPrice,
  withNumericPrice,
} from '../common';

export const createMotherboard = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.motherboard.create.parseAsync(req.body);
    await assertImageExists(data);
    const record = await db.motherboard.create(withDecimalPrice(data));
    return new Motherboard(withNumericPrice(record));
  },
  status: StatusCodes.CREATED,
});
