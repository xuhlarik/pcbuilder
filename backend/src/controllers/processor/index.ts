import { createProcessor } from './create';
import { deleteProcessor } from './delete';
import { readOneProcessor, readManyProcessors } from './read';
import { updateProcessor } from './update';

export default {
  create: createProcessor,
  delete: deleteProcessor,
  readOne: readOneProcessor,
  readMany: readManyProcessors,
  update: updateProcessor,
};
