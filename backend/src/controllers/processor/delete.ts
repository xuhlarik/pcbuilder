import { AccessRole } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const deleteProcessor = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    await db.processor.delete(req.params);
  },
});
