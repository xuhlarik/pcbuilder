import { AccessRole, Processor, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import {
  assertImageExists,
  handler,
  withNumericPrice,
  withOptionalDecimalPrice,
} from '../common';

export const updateProcessor = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.processor.update.parseAsync(req.body);
    await assertImageExists(data);
    const record = await db.processor.update({
      ...req.params,
      ...withOptionalDecimalPrice(data),
    });
    return new Processor(withNumericPrice(record));
  },
});
