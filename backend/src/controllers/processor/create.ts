import { AccessRole, Processor, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import {
  assertImageExists,
  handler,
  withDecimalPrice,
  withNumericPrice,
} from '../common';

export const createProcessor = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.processor.create.parseAsync(req.body);
    await assertImageExists(data);
    const record = await db.processor.create(withDecimalPrice(data));
    return new Processor(withNumericPrice(record));
  },
  status: StatusCodes.CREATED,
});
