import { Processor, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler, withNumericPrice } from '../common';

export const readOneProcessor = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.processor.readOne.parseAsync(req.query);
    const record = await db.processor.read.one({ ...req.params, ...data });
    return new Processor(withNumericPrice(record));
  },
});

export const readManyProcessors = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.processor.readMany.parseAsync(req.query);
    const records = await db.processor.read.many({ ...req.params, ...data });
    return records.map((record) => new Processor(withNumericPrice(record)));
  },
});
