import { createManufacturer } from './create';
import { deleteManufacturer } from './delete';
import { readOneManufacturer, readManyManufacturers } from './read';
import { updateManufacturer } from './update';

export default {
  create: createManufacturer,
  delete: deleteManufacturer,
  readOne: readOneManufacturer,
  readMany: readManyManufacturers,
  update: updateManufacturer,
};
