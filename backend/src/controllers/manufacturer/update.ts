import { AccessRole, Manufacturer, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updateManufacturer = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.manufacturer.update.parseAsync(req.body);
    const record = await db.manufacturer.update({ ...req.params, ...data });
    return new Manufacturer(record);
  },
});
