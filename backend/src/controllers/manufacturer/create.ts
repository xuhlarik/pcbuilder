import { AccessRole, Manufacturer, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createManufacturer = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.manufacturer.create.parseAsync(req.body);
    const record = await db.manufacturer.create(data);
    return new Manufacturer(record);
  },
  status: StatusCodes.CREATED,
});
