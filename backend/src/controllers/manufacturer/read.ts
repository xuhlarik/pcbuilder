import { Manufacturer, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOneManufacturer = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.manufacturer.readOne.parseAsync(req.query);
    const record = await db.manufacturer.read.one({ ...req.params, ...data });
    return new Manufacturer(record);
  },
});

export const readManyManufacturers = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.manufacturer.readMany.parseAsync(req.query);
    const records = await db.manufacturer.read.many({ ...req.params, ...data });
    return records.map((record) => new Manufacturer(record));
  },
});
