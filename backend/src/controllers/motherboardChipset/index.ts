import { createMotherboardChipset } from './create';
import { deleteMotherboardChipset } from './delete';
import { readOneMotherboardChipset, readManyMotherboardChipsets } from './read';
import { updateMotherboardChipset } from './update';

export default {
  create: createMotherboardChipset,
  delete: deleteMotherboardChipset,
  readOne: readOneMotherboardChipset,
  readMany: readManyMotherboardChipsets,
  update: updateMotherboardChipset,
};
