import { AccessRole, MotherboardChipset, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updateMotherboardChipset = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.motherboardChipset.update.parseAsync(req.body);
    const record = await db.motherboardChipset.update({
      ...req.params,
      ...data,
    });
    return new MotherboardChipset(record);
  },
});
