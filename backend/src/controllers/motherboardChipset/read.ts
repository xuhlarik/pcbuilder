import { MotherboardChipset, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOneMotherboardChipset = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.motherboardChipset.readOne.parseAsync(req.query);
    const record = await db.motherboardChipset.read.one({
      ...req.params,
      ...data,
    });
    return new MotherboardChipset(record);
  },
});

export const readManyMotherboardChipsets = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.motherboardChipset.readMany.parseAsync(req.query);
    const records = await db.motherboardChipset.read.many({
      ...req.params,
      ...data,
    });
    return records.map((record) => new MotherboardChipset(record));
  },
});
