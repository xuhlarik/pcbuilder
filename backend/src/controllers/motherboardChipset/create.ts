import { AccessRole, MotherboardChipset, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createMotherboardChipset = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.motherboardChipset.create.parseAsync(req.body);
    const record = await db.motherboardChipset.create(data);
    return new MotherboardChipset(record);
  },
  status: StatusCodes.CREATED,
});
