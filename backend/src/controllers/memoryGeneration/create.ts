import { AccessRole, MemoryGeneration, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createMemoryGeneration = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.memoryGeneration.create.parseAsync(req.body);
    const record = await db.memoryGeneration.create(data);
    return new MemoryGeneration(record);
  },
  status: StatusCodes.CREATED,
});
