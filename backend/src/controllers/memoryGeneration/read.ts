import { MemoryGeneration, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOneMemoryGeneration = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.memoryGeneration.readOne.parseAsync(req.query);
    const record = await db.memoryGeneration.read.one({
      ...req.params,
      ...data,
    });
    return new MemoryGeneration(record);
  },
});

export const readManyMemoryGenerations = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.memoryGeneration.readMany.parseAsync(req.query);
    const records = await db.memoryGeneration.read.many({
      ...req.params,
      ...data,
    });
    return records.map((record) => new MemoryGeneration(record));
  },
});
