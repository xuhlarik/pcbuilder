import { AccessRole, MemoryGeneration, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updateMemoryGeneration = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.memoryGeneration.update.parseAsync(req.body);
    const record = await db.memoryGeneration.update({ ...req.params, ...data });
    return new MemoryGeneration(record);
  },
});
