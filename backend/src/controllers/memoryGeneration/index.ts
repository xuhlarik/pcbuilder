import { createMemoryGeneration } from './create';
import { deleteMemoryGeneration } from './delete';
import { readOneMemoryGeneration, readManyMemoryGenerations } from './read';
import { updateMemoryGeneration } from './update';

export default {
  create: createMemoryGeneration,
  delete: deleteMemoryGeneration,
  readOne: readOneMemoryGeneration,
  readMany: readManyMemoryGenerations,
  update: updateMemoryGeneration,
};
