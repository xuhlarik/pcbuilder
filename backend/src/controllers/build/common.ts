import * as common from '../common';

const opt =
  <Arg extends any, Ret>(fn: (args: Arg) => Ret) =>
  (arg: Arg | null) =>
    arg !== null ? fn(arg) : null;

export const withNumericPrice = opt(common.withNumericPrice);

export const withDecimalPrice = opt(common.withDecimalPrice);

export const withOptionalNumericPrice = opt(common.withOptionalDecimalPrice);

export const withOptionalDecimalPrice = opt(common.withOptionalNumericPrice);
