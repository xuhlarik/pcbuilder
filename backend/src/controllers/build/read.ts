import { Build, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';
import { withNumericPrice } from './common';

export const readOneBuild = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.build.readOne.parseAsync(req.query);
    const record = await db.build.read.one({ ...req.params, ...data });
    return new Build({
      ...record,
      processor: withNumericPrice(record.processor),
      motherboard: withNumericPrice(record.motherboard),
      memory: withNumericPrice(record.memory),
      videoCard: withNumericPrice(record.videoCard),
    });
  },
});

export const readManyBuilds = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.build.readMany.parseAsync(req.query);
    const records = await db.build.read.many({ ...req.params, ...data });
    return records.map(
      (record) =>
        new Build({
          ...record,
          processor: withNumericPrice(record.processor),
          motherboard: withNumericPrice(record.motherboard),
          memory: withNumericPrice(record.memory),
          videoCard: withNumericPrice(record.videoCard),
        })
    );
  },
});
