import { Build, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';
import { withNumericPrice } from './common';

export const createBuild = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.build.create.parseAsync(req.body);
    const record = await db.build.create(data);
    return new Build({
      ...record,
      processor: withNumericPrice(record.processor),
      motherboard: withNumericPrice(record.motherboard),
      memory: withNumericPrice(record.memory),
      videoCard: withNumericPrice(record.videoCard),
    });
  },
  status: StatusCodes.CREATED,
});
