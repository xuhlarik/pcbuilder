import { Build, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';
import { withNumericPrice } from './common';

export const updateBuild = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.build.update.parseAsync(req.body);
    const record = await db.build.update({ ...req.params, ...data });
    return new Build({
      ...record,
      processor: withNumericPrice(record.processor),
      motherboard: withNumericPrice(record.motherboard),
      memory: withNumericPrice(record.memory),
      videoCard: withNumericPrice(record.videoCard),
    });
  },
});
