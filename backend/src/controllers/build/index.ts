import { createBuild } from './create';
import { deleteBuild } from './delete';
import { readOneBuild, readManyBuilds } from './read';
import { updateBuild } from './update';

export default {
  create: createBuild,
  delete: deleteBuild,
  readOne: readOneBuild,
  readMany: readManyBuilds,
  update: updateBuild,
};
