import type { RequestHandler } from 'express';

export type Controller = {
  create: RequestHandler<{}>;
  delete: RequestHandler<{ id: string }>;
  readOne: RequestHandler<{ id: string }>;
  readMany: RequestHandler<{}>;
  update: RequestHandler<{ id: string }>;
};
