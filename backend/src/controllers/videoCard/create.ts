import { AccessRole, VideoCard, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import {
  assertImageExists,
  handler,
  withDecimalPrice,
  withNumericPrice,
} from '../common';

export const createVideoCard = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.videoCard.create.parseAsync(req.body);
    await assertImageExists(data);
    const record = await db.videoCard.create(withDecimalPrice(data));
    return new VideoCard(withNumericPrice(record));
  },
  status: StatusCodes.CREATED,
});
