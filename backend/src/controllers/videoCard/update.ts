import { AccessRole, VideoCard, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import {
  assertImageExists,
  handler,
  withNumericPrice,
  withOptionalDecimalPrice,
} from '../common';

export const updateVideoCard = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.videoCard.update.parseAsync(req.body);
    await assertImageExists(data);
    const record = await db.videoCard.update({
      ...req.params,
      ...withOptionalDecimalPrice(data),
    });
    return new VideoCard(withNumericPrice(record));
  },
});
