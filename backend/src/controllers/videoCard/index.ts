import { createVideoCard } from './create';
import { deleteVideoCard } from './delete';
import { readOneVideoCard, readManyVideoCards } from './read';
import { updateVideoCard } from './update';

export default {
  create: createVideoCard,
  delete: deleteVideoCard,
  readOne: readOneVideoCard,
  readMany: readManyVideoCards,
  update: updateVideoCard,
};
