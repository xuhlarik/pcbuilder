import { VideoCard, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler, withNumericPrice } from '../common';

export const readOneVideoCard = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.videoCard.readOne.parseAsync(req.query);
    const record = await db.videoCard.read.one({ ...req.params, ...data });
    return new VideoCard(withNumericPrice(record));
  },
});

export const readManyVideoCards = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.videoCard.readMany.parseAsync(req.query);
    const records = await db.videoCard.read.many({ ...req.params, ...data });
    return records.map((record) => new VideoCard(withNumericPrice(record)));
  },
});
