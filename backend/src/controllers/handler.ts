import type { AccessRole } from '@pcbuilder/common/api';
import type { NextFunction, Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { Forbidden, Unauthorized } from '../errors';

const assertAccessRights = (given?: AccessRole[], allow?: Set<AccessRole>) => {
  if (allow === undefined) {
    return;
  }

  if (given === undefined) {
    throw new Unauthorized('Login required');
  }

  if (given.find((role) => allow.has(role)) === undefined) {
    throw new Forbidden('You are not permitted to access this resource');
  }
};

export const handler = <
  T extends object | void,
  Req extends Request,
  Res extends Response<T | Error>
>(params: {
  fn: (req: Req) => Promise<T>;
  status?: StatusCodes;
  allow?: AccessRole[];
}) => {
  const allowSet = params.allow ? new Set(params.allow) : undefined;

  return async (req: Req, res: Res, next: NextFunction) => {
    try {
      assertAccessRights(req.session.account?.roles, allowSet);
      const data = await params.fn(req);
      const status = StatusCodes[data === undefined ? 'NO_CONTENT' : 'OK'];
      res.status(params.status ?? status).json(data);
    } catch (err) {
      next(err);
    }
  };
};
