import { IntegratedGraphics, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOneIntegratedGraphics = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.integratedGraphics.readOne.parseAsync(req.query);
    const record = await db.integratedGraphics.read.one({
      ...req.params,
      ...data,
    });
    return new IntegratedGraphics(record);
  },
});

export const readManyIntegratedGraphics = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.integratedGraphics.readMany.parseAsync(req.query);
    const records = await db.integratedGraphics.read.many({
      ...req.params,
      ...data,
    });
    return records.map((record) => new IntegratedGraphics(record));
  },
});
