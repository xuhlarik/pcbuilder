import { createIntegratedGraphics } from './create';
import { deleteIntegratedGraphics } from './delete';
import { readOneIntegratedGraphics, readManyIntegratedGraphics } from './read';
import { updateIntegratedGraphics } from './update';

export default {
  create: createIntegratedGraphics,
  delete: deleteIntegratedGraphics,
  readOne: readOneIntegratedGraphics,
  readMany: readManyIntegratedGraphics,
  update: updateIntegratedGraphics,
};
