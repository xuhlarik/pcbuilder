import { AccessRole, IntegratedGraphics, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updateIntegratedGraphics = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.integratedGraphics.update.parseAsync(req.body);
    const record = await db.integratedGraphics.update({
      ...req.params,
      ...data,
    });
    return new IntegratedGraphics(record);
  },
});
