import { AccessRole, IntegratedGraphics, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createIntegratedGraphics = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.integratedGraphics.create.parseAsync(req.body);
    const record = await db.integratedGraphics.create(data);
    return new IntegratedGraphics(record);
  },
  status: StatusCodes.CREATED,
});
