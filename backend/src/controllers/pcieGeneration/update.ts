import { AccessRole, PcieGeneration, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updatePcieGeneration = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.pcieGeneration.update.parseAsync(req.body);
    const record = await db.pcieGeneration.update({ ...req.params, ...data });
    return new PcieGeneration(record);
  },
});
