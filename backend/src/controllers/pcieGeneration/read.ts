import { PcieGeneration, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOnePcieGeneration = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.pcieGeneration.readOne.parseAsync(req.query);
    const record = await db.pcieGeneration.read.one({ ...req.params, ...data });
    return new PcieGeneration(record);
  },
});

export const readManyPcieGenerations = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.pcieGeneration.readMany.parseAsync(req.query);
    const records = await db.pcieGeneration.read.many({
      ...req.params,
      ...data,
    });
    return records.map((record) => new PcieGeneration(record));
  },
});
