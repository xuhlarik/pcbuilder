import { AccessRole, PcieGeneration, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createPcieGeneration = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.pcieGeneration.create.parseAsync(req.body);
    const record = await db.pcieGeneration.create(data);
    return new PcieGeneration(record);
  },
  status: StatusCodes.CREATED,
});
