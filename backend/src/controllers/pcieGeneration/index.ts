import { createPcieGeneration } from './create';
import { deletePcieGeneration } from './delete';
import { readOnePcieGeneration, readManyPcieGenerations } from './read';
import { updatePcieGeneration } from './update';

export default {
  create: createPcieGeneration,
  delete: deletePcieGeneration,
  readOne: readOnePcieGeneration,
  readMany: readManyPcieGenerations,
  update: updatePcieGeneration,
};
