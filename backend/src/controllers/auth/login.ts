import { AccessRole, Account, schema } from '@pcbuilder/common/api';
import { pick } from '@pcbuilder/common/utils';
import argon2 from 'argon2';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';
import { Unauthorized } from '../../errors';

export const login = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.auth.login.parseAsync(req.body);

    const record = await db.account.readOne(pick(data, 'login'));

    // Current model is that only admins have accounts.
    const account = new Account({ ...record, roles: [AccessRole.ADMIN] });

    if (await argon2.verify(record.password, data.password)) {
      req.session.account = account;
    } else {
      throw new Unauthorized('Wrong password');
    }

    return account;
  },
});
