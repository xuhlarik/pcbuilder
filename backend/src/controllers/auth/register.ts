import { AccessRole, Account, schema } from '@pcbuilder/common/api';
import argon2 from 'argon2';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const register = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.auth.register.parseAsync(req.body);

    const hash = await argon2.hash(data.password);

    const record = await db.account.create({
      ...data,
      password: hash,
    });

    // Current model is that only admins have accounts.
    const account = new Account({ ...record, roles: [AccessRole.ADMIN] });

    // Save session state.
    req.session.account = account;

    return account;
  },
  status: StatusCodes.CREATED,
});
