import type { Request } from 'express';
import { handler } from '../common';

export const logout = handler({
  fn: async (req: Request<{}>) => {
    req.session.destroy(() => {});
  },
});
