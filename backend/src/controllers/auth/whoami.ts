import type { Request } from 'express';
import { handler } from '../common';
import { Unauthorized } from '../../errors';

export const whoami = handler({
  fn: async (req: Request<{}>) => {
    if (!req.session.account) {
      throw new Unauthorized('Login required');
    }
    return req.session.account;
  },
});
