import { login } from './login';
import { logout } from './logout';
import { register } from './register';
import { whoami } from './whoami';

export default {
  login,
  logout,
  register,
  whoami,
};
