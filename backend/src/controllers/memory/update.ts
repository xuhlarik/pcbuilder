import { AccessRole, Memory, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import {
  assertImageExists,
  handler,
  withNumericPrice,
  withOptionalDecimalPrice,
} from '../common';

export const updateMemory = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.memory.update.parseAsync(req.body);
    await assertImageExists(data);
    const record = await db.memory.update({
      ...req.params,
      ...withOptionalDecimalPrice(data),
    });
    return new Memory(withNumericPrice(record));
  },
});
