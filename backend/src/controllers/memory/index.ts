import { createMemory } from './create';
import { deleteMemory } from './delete';
import { readOneMemory, readManyMemories } from './read';
import { updateMemory } from './update';

export default {
  create: createMemory,
  delete: deleteMemory,
  readOne: readOneMemory,
  readMany: readManyMemories,
  update: updateMemory,
};
