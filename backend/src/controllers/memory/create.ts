import { AccessRole, Memory, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import {
  assertImageExists,
  handler,
  withDecimalPrice,
  withNumericPrice,
} from '../common';

export const createMemory = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.memory.create.parseAsync(req.body);
    await assertImageExists(data);
    const record = await db.memory.create(withDecimalPrice(data));
    return new Memory(withNumericPrice(record));
  },
  status: StatusCodes.CREATED,
});
