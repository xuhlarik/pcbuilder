import { Memory, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler, withNumericPrice } from '../common';

export const readOneMemory = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.memory.readOne.parseAsync(req.query);
    const record = await db.memory.read.one({ ...req.params, ...data });
    return new Memory(withNumericPrice(record));
  },
});

export const readManyMemories = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.memory.readMany.parseAsync(req.query);
    const records = await db.memory.read.many({ ...req.params, ...data });
    return records.map((record) => new Memory(withNumericPrice(record)));
  },
});
