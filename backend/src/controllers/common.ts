import { Prisma } from '@prisma/client';
import * as fs from 'fs/promises';
import * as path from 'path';
import env from '../env';
import { BadRequest } from '../errors';

export { handler } from './handler';

export const withNumericPrice = <T extends { price: Prisma.Decimal }>({
  price,
  ...rest
}: T) => ({
  price: price.toNumber(),
  ...rest,
});

export const withDecimalPrice = <T extends { price: number }>({
  price,
  ...rest
}: T) => ({
  price: new Prisma.Decimal(price),
  ...rest,
});

export const withOptionalNumericPrice = <T extends { price?: Prisma.Decimal }>({
  price,
  ...rest
}: T) => ({
  price: price?.toNumber(),
  ...rest,
});

export const withOptionalDecimalPrice = <T extends { price?: number }>({
  price,
  ...rest
}: T) => ({
  price: price ? new Prisma.Decimal(price) : undefined,
  ...rest,
});

export const assertImageExists = async (data: { image?: string }) => {
  if (data.image === undefined) {
    return;
  }

  const filepath = path.join(env('UPLOAD_DIR').split(':')[0]!, data.image);

  try {
    await fs.access(filepath);
  } catch (err) {
    throw new BadRequest('Image does not exist');
  }
};
