import auth from './auth';
import build from './build';
import category from './category';
import formFactor from './formFactor';
import integratedGraphics from './integratedGraphics';
import manufacturer from './manufacturer';
import memory from './memory';
import memoryGeneration from './memoryGeneration';
import motherboard from './motherboard';
import motherboardChipset from './motherboardChipset';
import pcieGeneration from './pcieGeneration';
import preset from './preset';
import processor from './processor';
import socket from './socket';
import videoCard from './videoCard';
import videoChipset from './videoChipset';

export default {
  auth,
  build,
  category,
  formFactor,
  integratedGraphics,
  memory,
  memoryGeneration,
  manufacturer,
  motherboard,
  motherboardChipset,
  pcieGeneration,
  preset,
  processor,
  socket,
  videoCard,
  videoChipset,
};
