import { AccessRole, FormFactor, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updateFormFactor = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.formFactor.update.parseAsync(req.body);
    const record = await db.formFactor.update({ ...req.params, ...data });
    return new FormFactor(record);
  },
});
