import { AccessRole, FormFactor, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createFormFactor = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.formFactor.create.parseAsync(req.body);
    const record = await db.formFactor.create(data);
    return new FormFactor(record);
  },
  status: StatusCodes.CREATED,
});
