import { FormFactor, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOneFormFactor = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.formFactor.readOne.parseAsync(req.query);
    const record = await db.formFactor.read.one({ ...req.params, ...data });
    return new FormFactor(record);
  },
});

export const readManyFormFactors = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.formFactor.readMany.parseAsync(req.query);
    const records = await db.formFactor.read.many({ ...req.params, ...data });
    return records.map((record) => new FormFactor(record));
  },
});
