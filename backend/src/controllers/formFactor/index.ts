import { createFormFactor } from './create';
import { deleteFormFactor } from './delete';
import { readOneFormFactor, readManyFormFactors } from './read';
import { updateFormFactor } from './update';

export default {
  create: createFormFactor,
  delete: deleteFormFactor,
  readOne: readOneFormFactor,
  readMany: readManyFormFactors,
  update: updateFormFactor,
};
