import { createPreset } from './create';
import { deletePreset } from './delete';
import { readOnePreset, readManyPresets } from './read';
import { updatePreset } from './update';

export default {
  create: createPreset,
  delete: deletePreset,
  readOne: readOnePreset,
  readMany: readManyPresets,
  update: updatePreset,
};
