import { Preset, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const readOnePreset = handler({
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.preset.readOne.parseAsync(req.query);
    const record = await db.preset.read.one({ ...req.params, ...data });
    return new Preset(record);
  },
});

export const readManyPresets = handler({
  fn: async (req: Request<{}>) => {
    const data = await schema.preset.readMany.parseAsync(req.query);
    const records = await db.preset.read.many({ ...req.params, ...data });
    return records.map((record) => new Preset(record));
  },
});
