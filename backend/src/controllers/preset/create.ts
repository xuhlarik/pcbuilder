import { AccessRole, Preset, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import db from '../../repositories';
import { handler } from '../common';

export const createPreset = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{}>) => {
    const data = await schema.preset.create.parseAsync(req.body);
    const record = await db.preset.create(data);
    return new Preset(record);
  },
  status: StatusCodes.CREATED,
});
