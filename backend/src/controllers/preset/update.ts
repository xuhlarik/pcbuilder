import { AccessRole, Preset, schema } from '@pcbuilder/common/api';
import type { Request } from 'express';
import db from '../../repositories';
import { handler } from '../common';

export const updatePreset = handler({
  allow: [AccessRole.ADMIN],
  fn: async (req: Request<{ id: string }>) => {
    const data = await schema.preset.update.parseAsync(req.body);
    const record = await db.preset.update({ ...req.params, ...data });
    return new Preset(record);
  },
});
