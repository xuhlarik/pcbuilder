import category from './category.json';
import memory from './memory.json';
import motherboard from './motherboard.json';
import processor from './processor.json';
import videoCard from './videoCard.json';

export default {
  category,
  memory,
  motherboard,
  processor,
  videoCard,
};
