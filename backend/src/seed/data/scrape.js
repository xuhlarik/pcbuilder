// https://pcpartpicker.com/product/*/*

Array.from(document.querySelectorAll('.group--spec')).reduce(
  (specs, node) => {
    const key = node.querySelector('.group__title').textContent.trim();
    const value = node.querySelector('.group__content').textContent.trim();
    return {
      ...specs,
      [key]: value,
    };
  },
  {
    $title: document.querySelector('.pageTitle').textContent,
    $price: Number.parseFloat(
      document
        .querySelector('#prices tbody .td__finalPrice')
        .textContent.trim()
        .replace('$', '')
    ),
  }
);
