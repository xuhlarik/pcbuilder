/* eslint-disable @typescript-eslint/dot-notation */

import { Prisma } from '@prisma/client';
import argon2 from 'argon2';
import accounts from './accounts';
import dataset from './data';
import prisma from '../repositories/client';

const createRecords = async <D, R extends { name: string; id: string }>(
  fn: (args: { data: D }) => Promise<R>,
  ...ds: D[]
) => {
  const records = await Promise.all(ds.map((data) => fn({ data })));
  return new Map(records.map(({ id, name }) => [name, id]));
};

const createNames = (...names: string[][]) => {
  const unique = new Set(names.flat());
  return [...unique].map((name) => ({ name }));
};

const extractNames = <T extends object, K extends keyof T>(
  key: K,
  ...datasets: Pick<T, K>[][]
) => datasets.flatMap((ds) => ds.flatMap((data) => data[key]));

const guessVideoChipsetManufacturer = (chipset: string) => {
  switch (chipset.split(' ', 2)[0]?.toLowerCase()) {
    case 'radeon':
      return 'AMD';
    case 'geforce':
      return 'NVIDIA';
    case 'titan':
      return 'NVIDIA';
    default:
      throw Error(`Cannot guess video chipset manufacturer for '${chipset}'`);
  }
};

const seedComponents = async (tx: Prisma.TransactionClient) => {
  // Order matters - referenced records (foreign keys) need to be created first!

  const manufacturer = await createRecords(
    tx.manufacturer.create,
    ...createNames(
      extractNames(
        'Manufacturer',
        dataset.memory,
        dataset.motherboard,
        dataset.processor,
        dataset.videoCard
      )
    )
  );

  console.log(`Created ${manufacturer.size} manufacturers.`);

  const socket = await createRecords(
    tx.socket.create,
    ...createNames(
      extractNames('Socket / CPU', dataset.motherboard),
      extractNames('Socket', dataset.processor)
    )
  );

  console.log(`Created ${socket.size} sockets.`);

  const integratedGraphics: Map<string, string | null> = await createRecords(
    tx.integratedGraphics.create,
    ...createNames(
      extractNames('Integrated Graphics', dataset.processor).filter(
        (name) => name !== 'None'
      )
    )
  );

  console.log(`Created ${integratedGraphics.size} integrated graphics.`);

  // Saves us from having to check this when reading processor data.
  integratedGraphics.set('None', null);

  const processor = await createRecords(
    tx.processor.create,
    ...dataset.processor.map((obj) => ({
      name: obj['$title'],
      price: new Prisma.Decimal(obj['$price']),
      socketId: socket.get(obj['Socket'])!,
      manufacturerId: manufacturer.get(obj['Manufacturer'])!,
      integratedGraphicsId: integratedGraphics.get(obj['Integrated Graphics'])!,
      image: obj['image'],
    }))
  );

  console.log(`Created ${processor.size} processors.`);

  const memoryGeneration = await createRecords(
    tx.memoryGeneration.create,
    ...createNames(extractNames('Memory Type', dataset.motherboard))
  );

  console.log(`Created ${memoryGeneration.size} memory generations.`);

  const memory = await createRecords(
    tx.memory.create,
    ...dataset.memory.map((data) => ({
      name: data['$title'],
      price: new Prisma.Decimal(data['$price']),
      manufacturerId: manufacturer.get(data['Manufacturer'])!,
      memoryGenerationId: memoryGeneration.get(data['Speed'].split('-')[0]!)!,
      image: data['image'],
    }))
  );

  console.log(`Created ${memory.size} memory kits.`);

  const formFactor = await createRecords(
    tx.formFactor.create,
    ...createNames(extractNames('Form Factor', dataset.motherboard))
  );

  console.log(`Created ${formFactor.size} form factors.`);

  const pcieGeneration = await createRecords(
    tx.pcieGeneration.create,
    ...createNames(['PCIe 6.0', 'PCIe 5.0', 'PCIe 4.0', 'PCIe 3.0'])
  );

  console.log(`Created ${pcieGeneration.size} PCIe generations.`);

  const videoChipset = await createRecords(
    tx.videoChipset.create,
    ...extractNames('Chipset', dataset.videoCard).map((chipset) => ({
      name: chipset,
      manufacturerId: manufacturer.get(guessVideoChipsetManufacturer(chipset))!,
    }))
  );

  console.log(`Created ${videoChipset.size} video chipsets.`);

  const videoCard = await createRecords(
    tx.videoCard.create,
    ...dataset.videoCard.map((data) => ({
      name: data['$title'],
      price: new Prisma.Decimal(data['$price']),
      manufacturerId: manufacturer.get(data['Manufacturer'])!,
      chipsetId: videoChipset.get(data['Chipset'])!,
      pcieGenerationId: pcieGeneration.get('PCIe 5.0')!, // don't have the data
      slotSize: Number.parseInt(data['Interface'].replace('PCIe x', ''), 10),
      image: data['image'],
    }))
  );

  console.log(`Created ${videoCard.size} video cards.`);

  const motherboardChipset = await createRecords(
    tx.motherboardChipset.create,
    ...dataset.motherboard.map((data) => {
      const [chipsetManufacturer, chipsetName] = data['Chipset'].split(' ', 2);
      return {
        name: chipsetName!,
        manufacturerId: manufacturer.get(chipsetManufacturer!)!,
        memoryGenerationId: memoryGeneration.get(data['Memory Type'])!,
      };
    })
  );

  console.log(`Created ${motherboardChipset.size} motherboard chipsets.`);

  const motherboard = await createRecords(
    tx.motherboard.create,
    ...dataset.motherboard.map((data) => ({
      name: data['$title'],
      price: new Prisma.Decimal(data['$price']),
      manufacturerId: manufacturer.get(data['Manufacturer'])!,
      chipsetId: motherboardChipset.get(data['Chipset'].split(' ', 2)[1]!)!,
      formFactorId: formFactor.get(data['Form Factor'])!,
      socketId: socket.get(data['Socket / CPU'])!,
      memorySlotCount: Number.parseInt(data['Memory Slots'], 10),
      image: data['image'],
      pcieSlots: {
        create: [
          {
            // We don't have data about PCIe generation :/
            pcieGenerationId: pcieGeneration.get('PCIe 5.0')!,
            size: 16,
            count: Number.parseInt(data['PCIe x16 Slots'], 10),
          },
          {
            pcieGenerationId: pcieGeneration.get('PCIe 5.0')!,
            size: 8,
            count: Number.parseInt(data['PCIe x8 Slots'], 10),
          },
          {
            pcieGenerationId: pcieGeneration.get('PCIe 5.0')!,
            size: 4,
            count: Number.parseInt(data['PCIe x4 Slots'], 10),
          },
          {
            pcieGenerationId: pcieGeneration.get('PCIe 5.0')!,
            size: 1,
            count: Number.parseInt(data['PCIe x1 Slots'], 10),
          },
        ].filter(({ count }) => count !== 0),
      },
    }))
  );

  console.log(`Created ${motherboard.size} motherboards.`);

  const category = await createRecords(
    tx.category.create,
    ...dataset.category.map(({ presets, ...data }) => ({
      ...data,
      preset: {
        create: presets.map((preset) => ({
          name: preset.name,
          description: preset.description,
          build: {
            create: {
              processorId: preset.processor
                ? processor.get(preset.processor)!
                : null,
              motherboardId: preset.motherboard
                ? motherboard.get(preset.motherboard)!
                : null,
              memoryId: preset.memory ? memory.get(preset.memory)! : null,
              videoCardId: preset.videoCard
                ? videoCard.get(preset.videoCard)!
                : null,
            },
          },
        })),
      },
    }))
  );

  console.log(`Created ${category.size} categories (with presets).`);
};

const seedAccounts = async (tx: Prisma.TransactionClient) => {
  await Promise.all(
    accounts.map(async (data) =>
      tx.account.create({
        data: {
          ...data,
          password: await argon2.hash(data.password),
        },
      })
    )
  );
};

const seed = async () => {
  console.log(`[${new Date().toISOString()}] Seed started`);
  await prisma.$transaction(async (tx) =>
    Promise.all([seedAccounts(tx), seedComponents(tx)])
  );
};

seed()
  .then(() => {
    console.log(`[${new Date().toISOString()}] Seed succeeded`);
  })
  .catch((e) => {
    console.log(`[${new Date().toISOString()}] Seed failed`);
    console.log(e);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
