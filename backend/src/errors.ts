import { StatusCodes, getReasonPhrase } from 'http-status-codes';

export abstract class ResponseError extends Error {
  status!: StatusCodes;

  // JSON.stringify should serialize properties.
  toJSON() {
    return {
      status: this.status,
      name: this.name,
      message: this.message,
    };
  }
}

const responseError = (status: StatusCodes) =>
  class extends ResponseError {
    override status = status;

    override name = getReasonPhrase(status); // close enough
  };

export class Conflict extends responseError(StatusCodes.CONFLICT) {}

export class Forbidden extends responseError(StatusCodes.FORBIDDEN) {}

export class NotFound extends responseError(StatusCodes.NOT_FOUND) {}

export class Unauthorized extends responseError(StatusCodes.UNAUTHORIZED) {}

export class InternalServerError extends responseError(
  StatusCodes.INTERNAL_SERVER_ERROR
) {}

export class BadRequest extends responseError(StatusCodes.BAD_REQUEST) {}
