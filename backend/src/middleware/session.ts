import type { Account } from '@pcbuilder/common/api';
import expressSession from 'express-session';
import env from '../env';

const secret = env('SESSION_SECRET');

declare module 'express-session' {
  interface SessionData {
    account: Account;
  }
}

export default expressSession({
  secret: secret.split(':'),
  resave: false,
  saveUninitialized: false,
  cookie: {
    httpOnly: true,
    secure: false,
  },
});
