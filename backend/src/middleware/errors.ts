import type { NextFunction, Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { MulterError } from 'multer';
import { ZodError } from 'zod';
import * as db from '../repositories';
import { InternalServerError, ResponseError } from '../errors';

const statusFromDatabaseError = (res: Response, err: unknown): err is Error => {
  if (err instanceof db.NotFoundError) {
    res.status(StatusCodes.NOT_FOUND);
  } else if (err instanceof db.ConflictError) {
    res.status(StatusCodes.CONFLICT);
  } else if (err instanceof db.ForeignKeyError) {
    res.status(StatusCodes.BAD_REQUEST);
  } else {
    return false;
  }

  return true;
};

const sendError = (res: Response, err: unknown) => {
  if (err instanceof ZodError) {
    res.status(StatusCodes.BAD_REQUEST);
  } else if (err instanceof MulterError) {
    res.status(StatusCodes.BAD_REQUEST);
  } else if (err instanceof ResponseError) {
    res.status(err.status);
  } else if (statusFromDatabaseError(res, err)) {
    // ^ sets response status code while also working as a type guard.
  } else {
    console.error(`[${new Date().toISOString()}]`, err);
    // Don't send the (potentially sensitive) error.
    sendError(res, new InternalServerError('This should not have happened'));
    return;
  }
  res.json(err);
};

const errorHandler = (
  err: unknown,
  _req: Request,
  res: Response,
  _next: NextFunction // eslint-disable-line @typescript-eslint/no-unused-vars
) => {
  sendError(res, err);
};

export default errorHandler;
