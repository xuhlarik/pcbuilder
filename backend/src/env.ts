import * as dotenv from 'dotenv';
import { cwd, env } from 'process';
import * as path from 'path';

// From highest priority to lowest.
const envDirs = ['./', '../'];

// From highest priority to lowest.
const envFiles = ['.env.local', '.env'];

for (const dir of envDirs) {
  for (const file of envFiles) {
    dotenv.config({ path: path.resolve(cwd(), path.join(dir, file)) });
  }
}

export default (name: string, fallback?: string) => {
  const value = env[name];

  if (value !== undefined) {
    return value;
  }

  if (fallback !== undefined) {
    return fallback;
  }

  throw Error(`Environment variable ${name} must be set!`);
};
