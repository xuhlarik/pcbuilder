import baseConfig from '../.lintstagedrc.js';

export default {
  ...baseConfig,
  '*.{ts,tsx}': 'eslint',
  '*.prisma': (files) => files.map((file) => `prisma validate '${file}'`),
};
