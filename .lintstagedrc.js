export default {
  // If the file has unsupported format, this prints an error, but still succeeds.
  '*': 'prettier --check',
};
